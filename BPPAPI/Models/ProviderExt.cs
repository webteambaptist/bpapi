﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public partial class Provider
    {
        public List<int> OtherPracticeLocations
        { get; set; }

        public List<int> ProviderFacilities
        { get; set; }

        public string PrintName
        { get; set; }

        public int ApplicationSubStatus
        { get; set; }
    }
}