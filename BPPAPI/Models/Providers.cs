﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class Providers
    {
        public class _ProvidersShort
        {
            //public Provider _Provider { get; set; }
            public LegalEntity _LegalEntity { get; set; }
            public ProviderFull _Provider { get; set; }

            public PracticeShort _Practice { get; set; }

            public List<Location> PracticeLocations { get; set; }

        }

        public class _ProvidersListShort
        {
            public List<_ProvidersShort> _ProvidersShort { get; set; }
        }

        public class _Providers
        {
            public Provider _Provider { get; set; }

            public Practice _Practice { get; set; }

            public List<Location> PracticeLocations { get; set; }        
        }

        public class _ProvidersList
        {
            public List<_Providers> _Providers { get; set; }
        }
    }
}