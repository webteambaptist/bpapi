﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class Facilities
    {
        public int ID { get; set; }
        public string FacilityName { get; set; }
        public bool isSelected { get; set; }
    }
}