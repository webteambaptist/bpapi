﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class Change
    {
        public DateTime? SubmittedDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string SubmitterGroup { get; set; }
        public string ProviderName { get; set; }
        public string ProviderNPI { get; set; }
        public string ChangeReason { get; set; }
        public string NameChange { get; set; }
        //public int? StatusChange { get; set; }
        public string User { get; set; }
        public string SubmitterTIN { get; set; }
        public long Id { get; set; }
        public Guid? Practice { get; set; }
        public int? ProcessedResults { get; set; }        
    }
}