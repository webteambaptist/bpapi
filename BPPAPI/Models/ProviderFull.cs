﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class ProviderFull
    {
        public int ID { get; set; }
        public string TaxID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        //public string AlsoKnownAs { get; set; }
        //public string MaidenName { get; set; }
        public string Suffix { get; set; }
        public string FullNameWithSuffix { get; set; }
        public string Degree { get; set; }
        public string DateOfBirth { get; set; }
        public string PrimarySpecialty { get; set; }
        public string SecondarySpecialty { get; set; }
        public string SpecialtyNotes { get; set; }
        public string NPINumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
      //  public Nullable<bool> isBoardCert { get; set; }
        public Nullable<bool> isBHPrivileged { get; set; }
      // public string ResidencyCompletionDate { get; set; }
      //  public string DateOfBoardExam { get; set; }
        public DateTime? CreatedDT { get; set; }
        public DateTime? ModifiedDT { get; set; }
        //public ProviderApplicationWorkflow ProviderApplicationWorkFlow { get; set; }
        public ProviderPracticeWorkflow ProviderPracticeWorkFlow { get; set; }
        public List<ProviderSpecialtyMatrix> ProviderSpecialties { get; set; }
        public ProviderPracticeMatrix matrix { get; set; }
        public int? ProviderMemberStatus { get; set; }
        public Nullable<System.Guid> GUID { get; set; }
        public string CMS { get; set; }
        public string PrimaryPracticeLocation { get; set; }
        public List<int> OtherPracticeLocations { get; set; }
        //public Nullable<int> CareType { get; set; }
        //public Nullable<int> ProviderType { get; set; }
        //public Nullable<int> Based { get; set; }
        //public string SpecialtyType { get; set; }
        //public string ServicePopulation { get; set; }
        //public string EmploymentStatus { get; set; }
        public ProviderAncillary ProviderAncillary { get; set; }
        public List<int> ProviderFacilities { get; set; }
        public List<Facility> Facilties { get; set; }
        public List<int> ProviderAcceptingPatients { get; set; }
        public List<Contract> Contracts { get; set; }
        public List<ProviderLocation> Locations { get; set; }
        public List<SpecialtyMatrix> Specialties { get; set; }
        public ProviderMemberStatu MemberStatus { get; set; }
        //public ProviderApplicationStatu ApplicationStatus { get; set; }
        public Nullable<int> StatusChangeReasonID { get; set; }
        public string StatusChangeReason { get; set; }
        public string OtherReason { get; set; }
        public Nullable<int> MSOVerificationStatusID { get; set; }
        public Nullable<int> MQRecommendationStatusID { get; set; }
        public Nullable<int> BoardOutcomeStatusID { get; set; }
        public Nullable<int> DenialReasonStatusID { get; set; }
        public Nullable<int> ProviderStatusID { get; set; }
        public Nullable<int> InactiveReasonStatusID { get; set; }
        public DateTime? ProviderInactiveDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string ProvidersMemberStatus { get; set; }
        public string MSOVerification { get; set; }
        public string MQRecommendation { get; set; }
        public string BoardOutcomeStatus { get; set; }
        public string DenialReason { get; set; }
        //public List<InactiveReasonStatu> InactiveReasonStatusDD { get; set; }
        public string InactiveReason { get; set; }
        public bool isNew { get; set; }
    }
}