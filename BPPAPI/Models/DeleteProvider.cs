﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class DeleteProvider
    {
        public string taxID { get; set; }
        public string providerID { get; set; }
        public string isDeleted { get; set; }
    }
}