﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class Utility
    {
        public static double DBParse(string input)
        {
            try
            {
                return double.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        public static bool BoolParse(string input)
        {
            try
            {
                return bool.Parse(input);
            }
            catch
            {
                return false;
            }
        }

        public static int IntParse(string input)
        {
            try
            {
                return int.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        public static DateTime DTParse(string input)
        {
            try
            {
                return DateTime.Parse(input);
            }
            catch
            {
                return DateTime.Now;
            }
        }
    }
}