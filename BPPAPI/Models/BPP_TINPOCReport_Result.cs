//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BPPAPI.Models
{
    using System;
    
    public partial class BPP_TINPOCReport_Result
    {
        public string LE_Name { get; set; }
        public string LE_Address1 { get; set; }
        public string LE_Address2 { get; set; }
        public string LE_City { get; set; }
        public string LE_State { get; set; }
        public string LE_Zip { get; set; }
        public string POCName { get; set; }
        public string POCEmail { get; set; }
        public string POCPhoneNumber { get; set; }
    }
}
