//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BPPAPI.Models
{
    using System;
    
    public partial class BPP_MasterLookupReport2_Result
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string AlsoKnownAs { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Maiden_Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Credentials { get; set; }
        public string NPINumber { get; set; }
        public string Email { get; set; }
        public string ProviderStatus { get; set; }
        public string PrimarySpecialty { get; set; }
        public string SecondarySpecialty { get; set; }
        public string SpecialtyNotes { get; set; }
        public string TIN { get; set; }
        public string GroupName { get; set; }
        public string PracticeName { get; set; }
        public string PrimaryLocationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string LocationCity { get; set; }
        public string LocationState { get; set; }
        public string LocationZip { get; set; }
        public string MedStaffPrivileges { get; set; }
        public string DateJoinderReceived { get; set; }
        public string DateSignedJoinder { get; set; }
        public string DateSignedMemberApp { get; set; }
        public string DatePresentedMQProcess { get; set; }
        public string DatePresentedReady { get; set; }
        public string DateRecommendationSent { get; set; }
        public string BoardConsiderationDate { get; set; }
        public string BoardOutcome { get; set; }
        public string OutcomeReason { get; set; }
        public string DateNotifcationOutcome { get; set; }
        public string BPPEffectiveDate { get; set; }
        public string ProviderInactiveDate { get; set; }
        public string InactiveReason { get; set; }
        public string NotificationDate { get; set; }
        public string EmploymentStatus { get; set; }
        public string ServicePopulation { get; set; }
        public string SpecialtyType { get; set; }
        public string FacilitySetting { get; set; }
        public string NotAcceptingPatients { get; set; }
        public string Notes { get; set; }
        public string LocationPhoneNumber { get; set; }
        public string LocationFax { get; set; }
    }
}
