﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class ManageProvider
    {
        public string taxID { get; set; }
        public string providerID { get; set; }
        public string memberStatus { get; set; }
        public string wfStatus { get; set; }
    }
}