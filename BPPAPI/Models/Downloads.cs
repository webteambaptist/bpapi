﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BPPAPI.Models;

namespace BPPAPI.Models
{
    public class Downloads
    {
        public List<BppDocument> DownloadLinks { get; set; }
    }
}