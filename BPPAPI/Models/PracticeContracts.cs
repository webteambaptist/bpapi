﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class PracticeContracts
    {
        public int ContractID { get; set; }
        public string ContractName { get; set; }
        public bool OptOut { get; set; }
    }
}