﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class NewProviders
    {
        public List<DegreeType> Degrees { get; set; }
        public List<SpecialtyMatrix> Specialities { get; set; }
        public List<Location> Locations { get; set; }
        public List<NewProvider> Providers { get; set; }
        //public BPPPracticeLocation Locations { get; set; }
    }
    public class NewProvider
    {
        public int ID { get; set; }
        public string TaxID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string FullNameWithSuffix { get; set; }
        public string Degree { get; set; }
        public string DateOfBirth { get; set; }
        public string PrimarySpecialty { get; set; }
        public string SecondarySpecialty { get; set; }
        public string NPINumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool? isBoardCert { get; set; }
        public bool? isBHPrivileged { get; set; }
        public string ResidencyCompletionDate { get; set; }
        public string DateOfBoardExam { get; set; }
        public DateTime? CreatedDT { get; set; }
        public DateTime? ModifiedDT { get; set; }
        public Nullable<System.Guid> GUID { get; set; }
        public string CMS { get; set; }
        public int PrimaryPracticeLocation { get; set; }
        public List<int> OtherPracticeLocations { get; set; }
        public Nullable<int> CareType { get; set; }
        public Nullable<int> ProviderType { get; set; }
        public Nullable<int> Based { get; set; }
        public List<int> ProviderFacilities { get; set; }
        public string AlsoKnownAs { get; set; }
    }
}