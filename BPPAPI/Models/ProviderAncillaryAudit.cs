//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BPPAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProviderAncillaryAudit
    {
        public int ID { get; set; }
        public string NPINumber { get; set; }
        public System.DateTime CreatedDT { get; set; }
        public string AKA { get; set; }
        public string Maiden_Name { get; set; }
        public string EmploymentStatus { get; set; }
        public string SpecialtyType { get; set; }
        public string ServicePopulation { get; set; }
        public string FacilitySetting { get; set; }
        public Nullable<System.Guid> PracticeGuid { get; set; }
        public Nullable<int> ProviderPracticeMatrixID { get; set; }
        public Nullable<System.DateTime> ModifiedDt { get; set; }
        public string ModifiedUser { get; set; }
    }
}
