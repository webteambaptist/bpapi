﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class Documents
    {
        public string Name {get;set;}
        public string Title { get; set; }
        public string Provider { get; set; }
        public string Practice { get; set; }
        public string FilePath { get; set; }
        public string FileOwner { get; set; }
        public string TaxID { get; set; }
        public string NPI { get; set; }
        public byte[] Content { get; set; }
        public string InputPath { get; set; }


    }
}