﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public partial class PracticeApplications
    {
        //public int ID { get; set; }
        public string PracticeName { get; set; }
        public string PracticeTaxID { get; set; }
        public string PrimaryPracticeSpecialty { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PracticePhoneNumber { get; set; }
        public string PracticeFaxNumber { get; set; }
        public string EMRVendor { get; set; }
        public string EMRVersion { get; set; }
        public string OfficeManagerName { get; set; }
        public string OfficeManagerPhoneNumber { get; set; }
        public string OfficeManagerEmail { get; set; }
        public string BillingManagerName { get; set; }
        public string BillingManagerPhoneNumber { get; set; }
        public string BillingManagerEmail { get; set; }
        public string PointOfContact { get; set; }
        public string POCPhoneNumber { get; set; }
        public string POCEmail { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        //public string ModifiedBy { get; set; }
    }
}