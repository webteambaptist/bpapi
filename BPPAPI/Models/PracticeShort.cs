﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class PracticeShort
    {
        public int ID { get; set; }
        public string PracticeGUID { get; set; }
        public string LegalEntityID { get; set; }
        public string PracticeName { get; set; }
        public int PracticeApprovalStatus { get; set; }
        //public string MSSPDetails { get; set; }
    }
}
