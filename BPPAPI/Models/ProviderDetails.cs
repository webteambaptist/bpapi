﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class ProviderDetails
    {
        //public int ID { get; set; }
        //public System.DateTime CREATEDT { get; set; }
        public string PRACTICETAXID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string PRIMARYSPECIALTY { get; set; }
        public string SECONDARYSPECIALTY { get; set; }
        public string NPINUMBER { get; set; }
        public string EMAIL { get; set; }
        public string ISBOARDCERT { get; set; }
        public string ISBHPRIVILEGED { get; set; }
        public string PRIMARYPRACTICELOCATION { get; set; }        
    }
}