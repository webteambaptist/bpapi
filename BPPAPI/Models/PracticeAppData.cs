﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PracticeAppData
    {
        public string PracticeID { get; set; }

        public string PracticeName { get; set; }

        public string TaxID { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public int PracticeApprovalStatus { get; set; }

        public string PracticePhoneNumber { get; set; }

        public List<PracticeLocations> PracticeLocations { get; set; }
        public Guid PracticeGuid { get; set; }

        //public List<Provider> _Providers { get; set; } 
        public List<ProviderFull> _Providers { get; set; }
    }
}