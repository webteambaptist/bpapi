﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class NewMemberApp
    {
        public string GUID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int AppStep { get; set; }
    }

    public class ReturningMemberApp
    {
        public string GUID { get; set; }
        public string LE_GUID { get; set; }
        public string TIN { get; set; }
        public string Email { get; set; }
        public int AppStep { get; set; }
    }
}