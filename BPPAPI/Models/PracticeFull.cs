﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    /// <summary>
    /// class for full application form update
    /// </summary>
    public class PracticeFull
    {
        public LegalEntity _Entity { get; set; }
        public List<PracticeExt> _Practices { get; set; }
        //public List<Location> PracticeLocations { get; set; }
        public List<Provider> _Providers { get; set; }
        public List<PracticeContracts> _Contracts { get; set; }
        public List<ProviderPracticeWorkflow> _WorkFlow { get; set; }
        public string LegalEntityStatus { get; set; }
 
    }

}
