﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class Changes
    {
        public string SubmitterName { get; set; }
        public string SubmitterPhysicianGroup { get; set; }
        public string SubmitterTIN { get; set; }
        public string SubmitterEmailAddress { get; set; }
        public string SubmitterPhoneNumber { get; set; }
        public List<Provider> _Providers { get; set; }
        public List<StatusChange> Change { get; set; }
        public List<StatusChangeReason> Reason { get; set; }
        //public List<ProviderMemberStatu> Statuses { get; set; }
    }
    public partial class Provider
    {
        public string ProviderName { get; set; }
        public string ProviderNPI { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int StatusChange { get; set; }
        public int StatusChangeReason { get; set; }
        public int UpdateStatus { get; set; }
        public string UpdateLastName { get; set; }
        public string Other { get; set; }
        public Guid? Practice { get; set; }
    }
}
