﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    /// <summary>
    /// Class to update practice
    /// </summary>
    public class PracticeUpdate
    {
        public int PracticeApprovalStatus { get; set; }
        public List<Practice> Practice { get; set; }
    }
}