﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class BppDocument
    {
        public string Title { get; set; }
        public string Link { get; set; }
    }
}