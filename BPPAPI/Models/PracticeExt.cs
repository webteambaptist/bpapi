﻿using System.Collections.Generic;

namespace BPPAPI.Models
{
    /// <summary>
    /// Extended properties for Practice
    /// </summary>
    public partial class PracticeExt
    {
        public int ID { get; set; }
        public string PracticeName { get; set; }
        public string LegalEntityID { get; set; }
        public string PracticeGUID { get; set; }
        public int? PracticeApprovalStatus { get; set; }
        public bool? PracticeLogoReceived { get; set; }
        public System.DateTime? CreatedDT { get; set; }
        public System.DateTime? ModifiedDT { get; set; }
        public List<PracticeLocations> PracticeLocations { get; set; }
    }
}
