﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class SubmitProviders
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string PersonalPhoneNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string Degree { get; set; }
        public string ResidencyCompletionDate { get; set; }
        public string DateOfBoardExam { get; set; }
        public string NPINumber { get; set; }
        public string PrimarySpecialty { get; set; }
        public string PracticeName { get; set; }
        public string PracticePhoneNumber { get; set; }
        public string PracticePhysicalAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool IsBoardCert { get; set; }
        public bool IsBaptistPrivileges { get; set; }
        public string EmployeeOrCommunityBased { get; set; }
        public string PrimaryCareOrSpecialist {get; set;}
        public DateTime ModifiedDT { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime CreatedDT { get; set; }
    }
}