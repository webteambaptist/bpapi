﻿namespace BPPAPI.Models
{
    public class FileUploadData
    {
        public string Base64DataUrl { get; set; }
        public string TaxidNPI { get; set; }
        public string FileName { get; set; }
        public string FileTitle { get; set; }
    }
}