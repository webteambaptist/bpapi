﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class BPPPracticeLocation
    {
        public int? tmp_loc_id { get; set; }
        public int ID { get; set; }
        public string LocationName { get; set; }
        public Nullable<bool> IsPrimary { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string FullAddress { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string EMRVendor { get; set; }
        public string EMRVersion { get; set; }
        public string OfficeManagerName { get; set; }
        public string OfficeManagerPhoneNumber { get; set; }
        public string OfficeManagerEmail { get; set; }
        public string OfficeManagerExtension { get; set; }
        public Nullable<System.DateTime> CreatedDT { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.Guid> PracticeGUID { get; set; }
        public Nullable<bool> IsPrimaryLocation { get; set; }
        public string LocationFullText { get; set; }

    }
}