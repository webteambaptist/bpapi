﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    /// <summary>
    /// class for PracticeLocations, stored in the Location table
    /// </summary>
    public class PracticeLocations
    {
        public int ID { get; set; }
        public System.Guid PracticeGUID { get; set; }
        public Nullable<System.Guid> LocationGUID { get; set; }
        public string LocationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public Nullable<bool> IsPrimary { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string EMRVendor { get; set; }
        public string EMRVersion { get; set; }
        public string OfficeManagerName { get; set; }
        public string OfficeManagerPhoneNumber { get; set; }
        public string OfficeManagerExtension { get; set; }
        public string OfficeManagerEmail { get; set; }
        public string BillingManagerName { get; set; }
        public string BillingManagerPhoneNumber { get; set; }
        public string BillingManagerExtension { get; set; }
        public string BillingManagerEmail { get; set; }
        public System.DateTime CreatedDT { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        public bool isRemoved { get; set; }
        public List<ProviderFull> Providers { get; set; }
    }
}
