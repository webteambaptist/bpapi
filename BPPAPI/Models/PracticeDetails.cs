﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    /// <summary>
    /// Object to hold all detail info for a practice
    /// </summary>
    public class PracticeDetail
    {
        public List<PracticeExt> _Practices { get; set; }
        public List<ProviderFull> _Providers { get; set; }
        public List<Facility> _Facilities { get; set; }
        public List<SpecialtyMatrix> _Specialties { get; set; }
        public List<PracticeContracts> _Contracts { get; set; }
        public List<Contract> _AcceptingNewPatientContracts { get; set; }
        public List<DegreeType> _Degrees { get; set; }
        public LegalEntity _Entity { get; set; }
        public List<MSOVerificationStatu> _MsoDD { get; set; }
        public List<MQRecommendationStatu> _MqRecommendDD { get; set; }
        public List<BoardOutcomeStatu> BoardOutcomeDD { get; set; }
        public List<DenialReasonStatu> DenialReasonDD { get; set; }
        public List<ProviderMemberStatu> ProviderMemberStatusDD { get; set; }
        public List<InactiveReasonStatu> InactiveReasonStatusDD { get; set; }
        public List<LegalEntityStatu> LegalEntityStatusDD { get; set; }
        //public List<Location> _Locations { get; set; }
        //public List<ProviderApplicationWorkflow> _Workflows { get; set; }
    }

}
