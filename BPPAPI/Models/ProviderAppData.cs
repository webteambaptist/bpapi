﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class ProviderAppData
    {
        public Provider provider { get; set; }

        public Practice practice { get; set; }

        public List<PracticeLocations> practiceLocations { get; set; }

        public string ProviderApprovalStatus { get; set; }

        public string ProviderMemberStatus { get; set; }

        public string ApplicationSubStatus { get; set; }

        public string StatusChangeReason { get; set; }
        public string OtherReason { get; set; }
        public List<Provider> Providers { get; set; }
    }
}
