﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class MemberRequest
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}