﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    /// <summary>
    /// Practice Business Associate Agreement
    /// </summary>
    public class PracticeBAA
    {
        public int ID { get; set; }
        public DateTime BAAEffectiveDT { get; set; }
        public string BAAEffectiveYear { get; set;}
        public string CoveredEntity { get; set; }
        public string CoveredEntityAddress { get; set; }
        public string CoveredEntityATTNField { get; set; }
        public string CoveredEntityFacsimile { get; set; }
        public string CoveredEntityEmail { get; set; }
        public string Witness1CoveredEntityOrganizationName { get; set; }
        public string Witness1CoveredEntitySignature { get; set; }
        public string Witness1CoveredEntityName { get; set; }
        public string Witness1CoveredEntityTitle { get; set; }
        public string Witness2BusinessAssociateOrganizationName { get; set; }
        public string Witness2BusinessAssociateSignature { get; set; }
        public string Witness2BusinessAssociateName { get; set; }
        public string Witness2BusinessAssociateTitle { get; set; }
        public DateTime ModifiedDT { get; set; }
        public string ModifiedBy { get; set; }

    }
}