﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Models
{
    public class ChangeDetails
    {
        public long Id { get; set; }
        public string SubmitterName { get; set; }
        public string SubmitterGroup { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string SubmitterTIN { get; set; }
        public string SubmitterEmail { get; set; }
        public string SubmitterPhone { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string ProviderNPI { get; set; }
        public string ProviderName { get; set; }
        public string ChangeReason { get; set; }
        public string StatusChangeReason { get; set; }
        public string NameChange { get; set; }
        public string Other { get; set; }
        public string PracticeName { get; set; }
        public Guid? Practice { get; set; }
        public int? ProcessedResults { get; set; }
    }
}