﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace BPPAPI.Helpers
{
    public class Mail
    {
        #region Properties
        public string from { get; set; }
        public string sendto { get; set; }
        public string cc { get; set; }
        public string bcc { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public Attachment attach { get; set; }
        public int priority { get; set; }
        #endregion

        public static string sendMail(msg m)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();
            //var SmtpUser = new System.Net.NetworkCredential("baptistrelay@gmail.com", "Generalcluster2017$");

           // smtpClient.Credentials = SmtpUser;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                smtpClient.Host = ConfigurationManager.AppSettings["RELAY"];
                smtpClient.Port = 25;
                //smtpClient.Host = "smtp.gmail.com";
                //smtpClient.Port = 587;
                smtpClient.EnableSsl = false;

                MailAddress fromAddress = new MailAddress(m.from);
                message.From = fromAddress;

                if (string.IsNullOrEmpty(m.sendto)) return "NO SENDER"; //must have a sender
                string[] sTO = m.sendto.Split(Convert.ToChar(","));
                for (int i = 0; i < sTO.Length; i++)
                    message.To.Add(sTO[i]);

                if (m.cc != null)
                {
                    string[] sCC = m.cc.Split(Convert.ToChar(","));
                    for (int i = 0; i < sCC.Length; i++)
                        message.CC.Add(sCC[i]);
                }

                if (m.bcc != null)
                {
                    string[] sBCC = m.bcc.Split(Convert.ToChar(","));
                    for (int i = 0; i < sBCC.Length; i++)
                        message.Bcc.Add(sBCC[i]);
                }

                message.Subject = m.subject;
                message.IsBodyHtml = true;
                message.Body = m.body;

                if (m.attachments != null)
                {
                    foreach (var attach in m.attachments)
                    {
                        message.Attachments.Add(attach);
                    }
                }
                    

                smtpClient.Send(message);

                return "";

            }
            catch (Exception err)
            {
                return err.Message;
            }
        }
        public static string sendMail(MailMessage m)
        {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            try
            {
                m.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();
                smtpClient.Host = ConfigurationManager.AppSettings["RELAY"];
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;
                
                MailAddress fromAddress = m.From;
                
                if (string.IsNullOrEmpty(m.To.ToString())) return "NO SENDER"; //must have a sender

                smtpClient.Send(m);

                return "";

            }
            catch (Exception err)
            {
                return err.Message;
            }
        }
    }

    public class msg
    {
        internal AttachmentCollection attachments;
        internal string bcc;
        internal string body;
        internal string cc;
        internal string from;
        internal string sendto;
        internal string subject;
        internal string attatchment;
    }
}