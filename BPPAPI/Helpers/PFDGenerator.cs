﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using BPPAPI.Models;
using System.Configuration;
using System.Net.Mail;
using BPPAPI.Controllers;

namespace BPPAPI.Helpers
{
    /// <summary>
    /// Helper class that generates PDF 
    /// </summary>
    public class PFDGenerator
    {
        /// <summary>
        /// Generates PDF based on a string with html
        /// </summary>
        /// <param name="html"></param>
        public static byte[] generatePDF(string html)
        {
            var pdfBytes = new byte[0];
            try
            {
                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                htmlToPdf.PageHeaderHtml = $@"<p>Page <b><span class=""page""></span></b> of <b><span class=""topage""></span></b></p><p>Last revised: February 6, 2016</p><hr/>";
                //<p style=""margin-bottom: 18px"">Last revised: February 6, 2016</p>
                //<div>Page<b><span class=""page""></span></b> of<b><span class=""topage""></span></b></div>
                pdfBytes = htmlToPdf.GeneratePdf(html);
            }
            catch (Exception e)
            {
                MailMessage jMail = new MailMessage(ConfigurationManager.AppSettings["sender"].ToString(), "tammi.anthony@bmcjax.com");
                
                jMail.Subject = "ERROR IN GENERATE PDF";
                jMail.Body = e.Message + " " + e.InnerException.Message + " " + html.ToString();
                new MessageController().sendMessage(jMail);
                // Console.WriteLine(e.Message);
            }
            return pdfBytes;
        }

        /// <summary>
        /// Generates PDF based on the html stored in the db
        /// </summary>
        /// <param name="ID"></param>
        public static byte[] generatePDF_FromDB(int ID)
        {
            string htmlCode = "";
            var bppEntities = new BPPEntities();

            var pdfgen = bppEntities.PDFGenerators; 

            htmlCode = (from x in pdfgen where x.ID == ID select x.HTML).FirstOrDefault();


            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBytes = htmlToPdf.GeneratePdf(htmlCode);

            return pdfBytes;            
        }

        /// <summary>
        /// Generates PDF based on the url provided
        /// </summary>
        /// <param name="url"></param>
        /// <param name="FileName"></param>
        public static byte[] generatePDF_ByUrl(string url, string FileName)
        {
            string htmlCode = "";
            using (WebClient client = new WebClient())
            {
                htmlCode = client.DownloadString(url);
            }
            
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            var pdfBytes = htmlToPdf.GeneratePdf(htmlCode);

            return pdfBytes;
        }
    }
}