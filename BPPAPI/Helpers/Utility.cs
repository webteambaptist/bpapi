﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI
{
    /// <summary>
    /// Utility class for API data cleanup/null testing
    /// </summary>
    public static class Utility
    {
        internal static DateTime TryDate(DateTime? targetDate)
        {
            DateTime retVal = new DateTime();

            if (targetDate != null)
            {
                try
                {
                    retVal = (DateTime)targetDate;
                }
                catch(Exception e)
                {
                    retVal = DateTime.Now;
                }
            }

            return retVal;
        }

        internal static string TryDateString(DateTime? targetDate)
        {
            string retVal = "";

            if (targetDate != null)
            {
                try
                {
                    retVal = ((DateTime)targetDate).ToShortDateString();
                }
                catch (Exception e)
                {
                    retVal = DateTime.Now.ToShortDateString();
                }
            }

            return retVal;
        }
    }
    }
