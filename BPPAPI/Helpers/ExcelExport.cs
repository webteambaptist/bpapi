﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BPPAPI.Helpers
{
    public class ExcelExport
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="refPg"></param>
        /// <param name="gv"></param>
        /// <param name="filename"></param>
        public static void ExportGridToExcel(Page refPg, GridView gv, string filename)
        {
            refPg.Response.ClearContent();
            refPg.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".xls");
            refPg.Response.ContentType = "application/excel";
            //refPg.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            gv.RenderControl(htw);

            //refPg.Response.Write(@"<style> .text { mso-number-format:\@; } </style> ");
            refPg.Response.Write(sw.ToString());
            refPg.Response.End();
        }

        /// <summary>
        /// Converts a list to a DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static DataTable ConvertTo<T>(IList<T> list)
        {
            DataTable table = CreateTable<T>();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (T item in list)
            {
                DataRow row = table.NewRow();
                
                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        /// <summary>
        /// Creates a DataTable with the headers
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static DataTable CreateTable<T>()
        {
            //Type entityType = typeof(T);
            //DataTable table = new DataTable(entityType.Name);
            DataTable table = new DataTable(typeof(T).Name);
            // PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                Type propType = prop.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    propType = new NullableConverter(propType).UnderlyingType;
                
                table.Columns.Add(prop.Name, propType);

            }
            
            //foreach (PropertyDescriptor prop in properties)
            //{
            //    table.Columns.Add(prop.Name, prop.PropertyType);
            //}

            return table;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static StringWriter writeFile(DataTable table)
        {
            StringWriter sw = new StringWriter();


            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (i == table.Columns.Count - 1)
                {
                    sw.Write(string.Concat(table.Columns[i].ColumnName));
                }
                else
                {
                    sw.Write(string.Concat(table.Columns[i].ColumnName, ","));
                }
            }
            sw.WriteLine();           

            foreach (DataRow row in table.Rows)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    sw.Write(row[i].ToString());

                    if (i < table.Columns.Count - 1)
                        sw.Write(",");
                }
                sw.WriteLine();
            }

            return sw;
        }

        /// <summary>
        /// Create CSV
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string ToCSV(DataTable table)
        {
            var result = new StringBuilder();
            for (int i = 0; i < table.Columns.Count; i++)
            {
                result.Append(table.Columns[i].ColumnName);
                result.Append(i == table.Columns.Count - 1 ? "\n" : ",");
            }

            foreach (DataRow row in table.Rows)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    result.Append(row[i].ToString());
                    result.Append(i == table.Columns.Count - 1 ? "\n" : ",");
                }
            }

            return result.ToString();
        }
    }
}