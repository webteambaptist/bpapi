﻿using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BPPAPI.Helpers
{
    public class Audit
    {
        public static void LegalEntityAudit(LegalEntity currentEntity, string UserName)
        {
            var bppEntities = new BPPEntities();
            var legalEntityAudit = new LegalEntityAudit();
            legalEntityAudit.BPPEffectiveDate = currentEntity.BPPEffectiveDate;
            legalEntityAudit.BusinessAssociationSigned = currentEntity.BusinessAssociationSigned;
            legalEntityAudit.CAPAddress1 = currentEntity.CAPAddress1;
            legalEntityAudit.CAPAddress2 = currentEntity.CAPAddress2;
            legalEntityAudit.CAPCity = currentEntity.CAPCity;
            legalEntityAudit.CAPCompleted = currentEntity.CAPCompleted;
            legalEntityAudit.CAPName = currentEntity.CAPName;
            legalEntityAudit.CAPState = currentEntity.CAPState;
            legalEntityAudit.CAPZip = currentEntity.CAPZip;
            legalEntityAudit.Created = currentEntity.Created;
            legalEntityAudit.ExecutedAgreementReturned = currentEntity.ExecutedAgreementReturned;
            legalEntityAudit.InactiveDate = currentEntity.InactiveDate;
            legalEntityAudit.InactiveNotes = currentEntity.InactiveNotes;
            legalEntityAudit.LastUpdated = currentEntity.LastUpdated;
            legalEntityAudit.LegalEntityID = currentEntity.LegalEntityID;
            legalEntityAudit.LE_Address1 = currentEntity.LE_Address1;
            legalEntityAudit.LE_Address2 = currentEntity.LE_Address2;
            legalEntityAudit.LE_City = currentEntity.LE_City;
            legalEntityAudit.LE_Name = currentEntity.LE_Name;
            legalEntityAudit.LE_State = currentEntity.LE_State;
            legalEntityAudit.LE_Zip = currentEntity.LE_Zip;
            legalEntityAudit.MemberApplicationSigned = currentEntity.MemberApplicationSigned;
            legalEntityAudit.OutcomeNotification = currentEntity.OutcomeNotification;
            legalEntityAudit.OutcomeNotificationNote = currentEntity.OutcomeNotificationNote;
            legalEntityAudit.ParticipationAgreementSigned = currentEntity.ParticipationAgreementSigned;
            legalEntityAudit.POCEmail = currentEntity.POCEmail;
            legalEntityAudit.POCExtension = currentEntity.POCExtension;
            legalEntityAudit.POCFax = currentEntity.POCFax;
            legalEntityAudit.POCName = currentEntity.POCName;
            legalEntityAudit.POCPhoneNumber = currentEntity.POCPhoneNumber;
            legalEntityAudit.Status = currentEntity.Status;
            legalEntityAudit.TaxID = currentEntity.TaxID;
            legalEntityAudit.InNetwork = (bool) currentEntity.InNetwork;
            legalEntityAudit.ModifiedUser = UserName;
            legalEntityAudit.ModifiedDt = DateTime.Now;
            bppEntities.LegalEntityAudits.Add(legalEntityAudit);
            bppEntities.SaveChanges();
        }
        public static void LocationAudit(Location location, string UserName)
        {
            var bppEntities = new BPPEntities();
            var locationAudit = new LocationAudit();
            locationAudit.Address1 = location.Address1;
            locationAudit.Address2 = location.Address2;
            locationAudit.BillingManagerEmail = location.BillingManagerEmail;
            locationAudit.BillingManagerExtension = location.BillingManagerExtension;
            locationAudit.BillingManagerName = location.BillingManagerName;
            locationAudit.BillingManagerPhoneNumber = location.BillingManagerPhoneNumber;
            locationAudit.City = location.City;
            locationAudit.CreatedDT = location.CreatedDT;
            locationAudit.EMRVendor = location.EMRVendor;
            locationAudit.EMRVersion = location.EMRVersion;
            locationAudit.Extension = location.Extension;
            locationAudit.Fax = location.Fax;
            locationAudit.IsPrimary = location.IsPrimary;
            locationAudit.isRemoved = location.isRemoved;
            locationAudit.LocationGUID = location.LocationGUID;
            locationAudit.LocationName = location.LocationName;
            locationAudit.ModifiedDT = DateTime.Now;
            locationAudit.ModifiedUser = UserName;
            locationAudit.OfficeManagerEmail = location.OfficeManagerEmail;
            locationAudit.OfficeManagerExtension = location.OfficeManagerExtension;
            locationAudit.OfficeManagerName = location.OfficeManagerName;
            locationAudit.OfficeManagerPhoneNumber = location.OfficeManagerPhoneNumber;
            locationAudit.PhoneNumber = location.PhoneNumber;
            locationAudit.PracticeGUID = location.PracticeGUID;
            locationAudit.State = location.State;
            locationAudit.Zip = location.Zip;
            bppEntities.LocationAudits.Add(locationAudit);
            bppEntities.SaveChanges();                
         }
        public static void PracticeAudit(Practice practice, string UserName)
        {
            var bppEntities = new BPPEntities();
            var practiceAudit = new PracticeAudit();
            practiceAudit.CreatedDT = practice.CreatedDT;
            practiceAudit.LegalEntityID = practice.LegalEntityID;
            practiceAudit.ModifiedDT = DateTime.Now;
            practiceAudit.ModifiedUser = UserName;
            practiceAudit.PracticeApprovalStatus = practice.PracticeApprovalStatus;
            practiceAudit.PracticeGUID = practice.PracticeGUID;
            practiceAudit.PracticeName = practice.PracticeName;
            bppEntities.PracticeAudits.Add(practiceAudit);
            bppEntities.SaveChanges();
         }
        public static void PracticeNotesAudit(PracticeNote notes, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new PracticeNotesAudit();
            audit.CreateDT = notes.CreateDT;
            audit.ModifedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.Note = notes.Note;
            audit.PracticeGuid = notes.PracticeGuid;
            bppEntities.PracticeNotesAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderAncillaryAudit(ProviderAncillary ancillary, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderAncillaryAudit();
            audit.AKA = ancillary.AKA;
            audit.CreatedDT = ancillary.CreatedDT;
            audit.EmploymentStatus = ancillary.EmploymentStatus;
            audit.Maiden_Name = ancillary.Maiden_Name;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.NPINumber = ancillary.NPINumber;
            audit.PracticeGuid = ancillary.PracticeGuid;
            audit.ProviderPracticeMatrixID = ancillary.ProviderPracticeMatrixID;
            audit.ServicePopulation = ancillary.ServicePopulation;
            audit.SpecialtyType = ancillary.SpecialtyType;
            audit.FacilitySetting = ancillary.FacilitySetting;
            bppEntities.ProviderAncillaryAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderFacilityAudit(ProviderFacility facility, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderFacilitiesAudit();
            audit.FacilityID = facility.FacilityID;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.NPINumber = facility.NPINumber;
            audit.PracticeGUID = facility.PracticeGUID;
            audit.ProviderPracticeMatrixID = facility.ProviderPracticeMatrixID;
            bppEntities.ProviderFacilitiesAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderLocationsAudit(ProviderLocation location, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderLocationsAudit();
            audit.IsPrimary = location.IsPrimary;
            audit.LocationID = location.LocationID;
            audit.ModifedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.NPINumber = location.NPINumber;
            audit.PracticeGUID = location.PracticeGUID;
            audit.ProviderPracticeMatrixID = location.ProviderPracticeMatrixID;
            bppEntities.ProviderLocationsAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderNotesAudit(ProviderNote notes, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderNotesAudit();
            audit.CreateDT = notes.CreateDT;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.Note = notes.Note;
            audit.NPINumber = notes.NPINumber;
            bppEntities.ProviderNotesAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderPracticeMatrixAudit(ProviderPracticeMatrix matrix, string userName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderPracticeMatrixAudit();
            audit.EndDate = matrix.EndDate;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = userName;
            audit.NPINumber = matrix.NPINumber;
            audit.PracticeGUID = matrix.PracticeGUID;
            audit.StartDate = matrix.StartDate;
            bppEntities.ProviderPracticeMatrixAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderPracticeWorkflowAudit(ProviderPracticeWorkflow workflow, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderPracticeWorkflowAudit();
            audit.BoardConsiderationDate = workflow.BoardConsiderationDate;
            audit.BoardOutcomeStatusID = workflow.BoardOutcomeStatusID;
            audit.BPPEffectiveDate = workflow.BPPEffectiveDate;
            audit.DateJoinderReceived = workflow.DateJoinderReceived;
            audit.DateNotifcationOutcome = workflow.DateNotifcationOutcome;
            audit.DatePresentedMQProcess = workflow.DatePresentedMQProcess;
            audit.DatePresentedReady = workflow.DatePresentedReady;
            audit.DateRecommendationSent = workflow.DateRecommendationSent;
            audit.DateSignedJoinder = workflow.DateSignedJoinder;
            audit.DateSignedMemberApp = workflow.DateSignedMemberApp;
            audit.DenialReasonStatusID = workflow.DenialReasonStatusID;
            audit.EligibilityCertDate = workflow.EligibilityCertDate;
            audit.InactiveReasonStatusID = workflow.InactiveReasonStatusID;
            audit.MedStaffPrivileges = workflow.MedStaffPrivileges;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.MQRecommendationStatusID = workflow.MQRecommendationStatusID;
            audit.MSOVerificationStatusID = workflow.MSOVerificationStatusID;
            audit.NotificationDate = workflow.NotificationDate;
            audit.NPINumber = workflow.NPINumber;
            audit.Outcome = workflow.Outcome;
            audit.OutcomeNotes = workflow.OutcomeNotes;
            audit.PracticeGUID = workflow.PracticeGUID;
            audit.PrimaryPracticeLocation = workflow.PrimaryPracticeLocation;
            audit.ProviderInactiveDate = workflow.ProviderInactiveDate;
            audit.ProviderPracticeMatrixID = workflow.ProviderPracticeMatrixID;
            audit.ProviderStatusID = workflow.ProviderStatusID;
            audit.Witness = workflow.Witness;
            bppEntities.ProviderPracticeWorkflowAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProviderSpecialtyMatrixAudit(ProviderSpecialtyMatrix matrix, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProviderSpecialtyMatrixAudit();
            audit.IsPrmary = matrix.IsPrmary;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.NPINumber = matrix.NPINumber;
            audit.PracticeGUID = matrix.PracticeGUID;
            audit.ProviderPracticeMatrixID = matrix.ProviderPracticeMatrixID;
            audit.SpecialtyID = matrix.SpecialtyID;
            bppEntities.ProviderSpecialtyMatrixAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ProvidersAudit(Provider provider, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ProvidersAudit();
            audit.CMS = provider.CMS;
            audit.CreatedDT = provider.CreatedDT;
            audit.DateOfBirth = provider.DateOfBirth;
            audit.DegreeID = provider.DegreeID;
            audit.Email = provider.Email;
            audit.FirstName = provider.FirstName;
            audit.GUID = provider.GUID;
            audit.isBHPrivileged = provider.isBHPrivileged;
            audit.isDeleted = provider.isDeleted;
            audit.LastName = provider.LastName;
            audit.MiddleName = provider.MiddleName;
            audit.ModifiedDT = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.NPINumber = provider.NPINumber;
            audit.PhoneNumber = provider.PhoneNumber;
            audit.SpecialtyNotes = provider.SpecialtyNotes;
            audit.Suffix = provider.Suffix;
            //audit.TaxID = provider.TaxID;
            bppEntities.ProvidersAudits.Add(audit);
            bppEntities.SaveChanges();
        }
        public static void ContractsOptOutAudit(ContractsOptOut contract, string UserName)
        {
            var bppEntities = new BPPEntities();
            var audit = new ContractsOptOutAudit();
            audit.ContractID = contract.ContractID;
            audit.LegalEntityID = contract.LegalEntityID;
            audit.ModifiedDt = DateTime.Now;
            audit.ModifiedUser = UserName;
            audit.OptOut = contract.OptOut;
            bppEntities.ContractsOptOutAudits.Add(audit);
            bppEntities.SaveChanges();
        }

        public static void ProviderAcceptingPatients(ProviderAcceptingPatient pa, string userName)
        {
            var bppEntities = new BPPEntities();
            var providerAcceptingPatients = new ProviderAcceptingPatientsAudit();
            providerAcceptingPatients.ContractID = pa.ContractID;
            providerAcceptingPatients.NPINumber = pa.NPINumber;
            providerAcceptingPatients.PracticeGUID = pa.PracticeGUID;
            providerAcceptingPatients.ProviderPracticeMatrixID = pa.ProviderPracticeMatrixID;
            providerAcceptingPatients.ModifiedDt = DateTime.Now;
            providerAcceptingPatients.ModifiedUser = userName;
            bppEntities.ProviderAcceptingPatientsAudits.Add(providerAcceptingPatients);
            bppEntities.SaveChanges();
        }
    }
}