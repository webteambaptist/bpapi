﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Microsoft.SharePoint.Client;
using System.Configuration;
using System.Data.Services.Client;
using System.Net.Http;
using System.IO;
using BPPAPI.Models;
//using BPPAPI.BPP;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using BPPAPI.Helpers;
using System.Diagnostics;
using System.Security.Cryptography.Xml;
using Microsoft.SharePoint.DataService;
//using BPPAPI.PRODBPP;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for file I/O.
    /// </summary>

    [RoutePrefix("api/files")]
    public class SPDocumentController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string url = ConfigurationManager.AppSettings["SP"].ToString();
        /// <summary>
        /// Get filenames that are available to retrieve for a given NPI.
        /// </summary>
        [HttpPost]
        [Route("filesNPI")]
        public IHttpActionResult retrieveFilesByNPI()
        {
            var headers = Request.Headers;
            string link = "";
            try
            {
                if (headers.Contains("NPI"))
                {
                    link = headers.GetValues("NPI").First();
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files", ex);
                return BadRequest();
            }

            BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
            //bpp.Credentials = CredentialCache.DefaultCredentials;
            bpp.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");

            List<Attachments0Item> docs = new List<Attachments0Item>();
            docs = bpp.Attachments0.ToList();
            docs = (from x in docs where x.NPI == link select x).ToList();

            List<Attachments0Item> docsout = new List<Attachments0Item>();

            foreach (var a in docs)
            {
                a.ContentTypeID = Crypto.Encrypt(a.Id.ToString());
                //a.TaxID = Crypto.Encrypt(a.TaxID);
                // a.NPI = Crypto.Encrypt(a.NPI);
                docsout.Add(a);
            }

            return Ok(docsout);
        }

        /// <summary>
        /// Get filenames that are available to retrieve for a given TaxID.
        /// </summary>
        [HttpPost]
        [Route("filesTaxID")]
        public IHttpActionResult retrieveFilesByTaxID()
        {
            var headers = Request.Headers;
            string link = "";
            try
            {
                // Now using LegalEntityID
                if (headers.Contains("TaxID"))
                {
                    link = headers.GetValues("TaxID").First();
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files", ex);
                return BadRequest();
            }

            BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
            //bpp.Credentials = CredentialCache.DefaultCredentials;
            bpp.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");


            List<Attachments0Item> docs = new List<Attachments0Item>();
            docs = bpp.Attachments0.ToList();
            docs = (from x in docs where x.TaxID == link select x).ToList();

            List<Attachments0Item> docsout = new List<Attachments0Item>();

            foreach (var a in docs)
            {
                a.ContentTypeID = Crypto.Encrypt(a.Id.ToString());
                //a.TaxID = Crypto.Encrypt(a.TaxID);
                // a.NPI = Crypto.Encrypt(a.NPI);
                docsout.Add(a);
            }

            return Ok(docsout);
        }

        /// <summary>
        /// method to retrieve specific file from SP
        /// </summary>
        /// <returns></returns>
        [Route("getstream")]
        [HttpPost]
        public IHttpActionResult getFile()
        {
            var headers = Request.Headers;
            string link = "";
            int ID = 0;

            try
            {
                if (headers.Contains("ID"))
                {
                    link = Crypto.Decrypt(headers.GetValues("ID").First());

                    try
                    {
                        ID = int.Parse(link);
                    }
                    catch (Exception e)
                    {
                        ID = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files", ex);
                return BadRequest();
            }

            BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
            //bpp.Credentials = CredentialCache.DefaultCredentials;
            bpp.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");

            List<Attachments0Item> docs = new List<Attachments0Item>();
            docs = bpp.Attachments0.ToList();

            var doc = (from x in docs where x.Id == ID select x).ToList();

            System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = bpp.GetReadStream(doc.First());
            Stream stream = DataServiceStreamResponse.Stream;

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(stream)
            };

            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = doc.First().Name
            };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            var response = ResponseMessage(result);

            return response;
        }

        /// <summary>
        /// method to retrieve specific file from SP
        /// </summary>
        /// <param name="NPIID"></param>
        /// <returns></returns>
        [Route("getstreamdoc")]
        [HttpPost]
        public IHttpActionResult getFileDoc()
        {
            var headers = Request.Headers;
            string link = "";
            string ID = "0";

            try
            {
                if (headers.Contains("NPIID"))
                {
                    link = headers.GetValues("NPIID").First();

                    try
                    {
                        ID = link;
                    }
                    catch (Exception e)
                    {
                        ID = "0";
                    }


                    BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
                    //bpp.Credentials = CredentialCache.DefaultCredentials;
                    bpp.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");

                    List<Attachments0Item> docs = new List<Attachments0Item>();
                    docs = bpp.Attachments0.ToList();

                    var docs2 = docs.Where(x => x.NPI == ID).ToList();

                    var doc = docs2.Where(x => x.Title.StartsWith("joinder", true, null)).ToList();

                    System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = bpp.GetReadStream(doc.First());
                    Stream stream = DataServiceStreamResponse.Stream;

                    var result = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(stream)
                    };

                    result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                    {
                        FileName = doc.First().Name
                    };

                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    var response = ResponseMessage(result);

                    return response;

                }
                else
                {
                    if (headers.Contains("TAXID"))
                    {
                        link = headers.GetValues("TAXID").First();

                        try
                        {
                            ID = link;
                        }
                        catch (Exception e)
                        {
                            ID = "0";
                        }

                        string mode = headers.GetValues("MODE").First();

                        BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
                        //bpp.Credentials = CredentialCache.DefaultCredentials;
                        bpp.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");

                        List<Attachments0Item> docs = new List<Attachments0Item>();
                        docs = bpp.Attachments0.ToList();

                        //var doc = (from x in docs where x.TaxID == ID && x.Title.StartsWith(mode, true, null) select x).ToList();

                        var docs2 = docs.Where(x => x.TaxID == ID).ToList();

                        var doc = docs2.Where(x => x.Title.StartsWith(mode+"Form", true, null)).ToList();

                        System.Data.Services.Client.DataServiceStreamResponse DataServiceStreamResponse = bpp.GetReadStream(doc.First());
                        Stream stream = DataServiceStreamResponse.Stream;

                        var result = new HttpResponseMessage(HttpStatusCode.OK)
                        {
                            Content = new StreamContent(stream)
                        };

                        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                        {
                            FileName = doc.First().Name
                        };

                        result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                        var response = ResponseMessage(result);

                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("api/files", ex);

                return BadRequest(ex.InnerException.Message);
            }

            return Ok("Not Found");
        }

        /// <summary>
        /// Method to save files back to SP
        /// </summary>
        /// <returns></returns>
        [Route("savefile")]
        [HttpPost]
        public HttpResponseMessage savefile()
        {
            var httpRequest = HttpContext.Current.Request;
            var headers = Request.Headers;

            if (httpRequest.Files.Count > 0)
            {
                for (int i = 0; i < (httpRequest.Files.Count); i++)
                {
                    var file = httpRequest.Files[i];

                    BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
                    //bpp.Credentials = CredentialCache.DefaultCredentials;
                    bpp.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");

                    string _fileName = Path.GetFileNameWithoutExtension(file.FileName);
                    string _path = ConfigurationManager.AppSettings["SPENV"]; //"/Teams/QaBpp/Attachments/"; //+ file.FileName;
                    string _contentType = "document";
                    string _npi = "";
                    string _taxID = "";

                    try
                    {
                        if (headers != null)
                        {
                            if (headers.Contains("NPI"))
                            {
                                _npi = headers.GetValues("NPI").First();
                                _fileName = _fileName + "_" + _npi + "_" + DateTime.Now.Ticks.ToString();
                            }

                            if (headers.Contains("TAXID"))
                            {
                                _taxID = headers.GetValues("TAXID").First();
                                _fileName = _fileName + "_" + _taxID + "_" + DateTime.Now.Ticks.ToString();
                            }
                        }

                        _path = _path + _fileName + Path.GetExtension(file.FileName);
                    }
                    catch(Exception ex)
                    {
                        log.Info("api/files", ex);

                        return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    }


                    Attachments0Item ai = new Attachments0Item()
                    {
                        TaxID = _taxID,
                        NPI = _npi,
                        Path = _path,
                        ContentType = _contentType,
                        Name = _fileName,
                        Title = _fileName
                    };

                    bpp.AddToAttachments0(ai);
                    bpp.SetSaveStream(ai, file.InputStream, false, _contentType, _path);

                    try
                    {
                        bpp.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                        log.Info("api/files", ex);
                        return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }
    }
}
