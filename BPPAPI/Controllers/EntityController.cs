﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for Practice Data
    /// </summary>

    [RoutePrefix("api/entities")]
    public class EntityController : ApiController
    {
        /// <summary>
        /// Get all practice data by GUID
        /// </summary>
        /// <returns></returns>
        [Route("SELECTENTITY")]
        [HttpGet]
        public IHttpActionResult selEntityByGuid()
        {
            var headers = Request.Headers;
            string _LegalEntityID = "";
            var bppEntities = new BPPEntities();
            PracticeDetail pd = new PracticeDetail();
            try
            {
                if (headers.Contains("LegalEntityID"))
                {
                    _LegalEntityID = headers.GetValues("LegalEntityID").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }
            
            try
            {
                if (!string.IsNullOrEmpty(_LegalEntityID))
                {
                    var entityID = Guid.Parse(_LegalEntityID);
                    var le = bppEntities.LegalEntities.Where(x => x.LegalEntityID == entityID).First();

                    pd._Contracts = new List<PracticeContracts>();
                    var contracts = bppEntities.selOptOutByLegalEntityID(le.LegalEntityID);
                    foreach (var item in contracts)
                    {
                        
                        PracticeContracts pc = new PracticeContracts();
                        pc.ContractID = item.ID;
                        pc.ContractName = item.ContractName;
                        pc.OptOut = Convert.ToBoolean(item.Opt);
                        pd._Contracts.Add(pc);
                    }
                    pd._Contracts.ToList();
                    pd._Entity = le;

                    pd.LegalEntityStatusDD = bppEntities.LegalEntityStatus.OrderBy(e => e.StatusName).ToList();
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }
            return Json(pd);
        }
        [Route("UPDATEENTITY")]
        [HttpPost]
       public IHttpActionResult UpdateEntity([FromBody] PracticeFull practiceFull)
        {
            var bppEntities = new BPPEntities();
            var legalEntities = bppEntities.LegalEntities;
            var contracts = new List<PracticeContracts>();
            var headers = Request.Headers;
            string UpdateUser = "";
            try
            {
                if (headers.Contains("UserName"))
                {
                    UpdateUser = headers.GetValues("UserName").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }
            try
            {
                var le = bppEntities.LegalEntities.Where(x => x.TaxID == practiceFull._Entity.TaxID).FirstOrDefault();
               
                le.LastUpdated = DateTime.Now;
                le.LE_Address1 = practiceFull._Entity.LE_Address1;
                le.LE_Address2 = practiceFull._Entity.LE_Address2;
                le.LE_City = practiceFull._Entity.LE_City;
                le.LE_Name = practiceFull._Entity.LE_Name;
                le.LE_State = practiceFull._Entity.LE_State;
                le.LE_Zip = practiceFull._Entity.LE_Zip;
                le.MemberApplicationSigned = practiceFull._Entity.MemberApplicationSigned;
                le.OutcomeNotification = practiceFull._Entity.OutcomeNotification;
                le.OutcomeNotificationNote = practiceFull._Entity.OutcomeNotificationNote;
                le.ParticipationAgreementSigned = practiceFull._Entity.ParticipationAgreementSigned;
                le.POCEmail = practiceFull._Entity.POCEmail;
                le.POCExtension = practiceFull._Entity.POCExtension;
                le.POCFax = practiceFull._Entity.POCFax;
                le.POCName = practiceFull._Entity.POCName;
                le.POCPhoneNumber = practiceFull._Entity.POCPhoneNumber;
                le.Status = practiceFull._Entity.Status;
                var shortdate = practiceFull._Entity.BPPEffectiveDate;
                le.BPPEffectiveDate = practiceFull._Entity.BPPEffectiveDate;
                le.BusinessAssociationSigned = practiceFull._Entity.BusinessAssociationSigned;
                le.CAPAddress1 = practiceFull._Entity.CAPAddress1;
                le.CAPAddress2 = practiceFull._Entity.CAPAddress2;
                le.CAPCity = practiceFull._Entity.CAPCity;
                le.CAPCompleted = practiceFull._Entity.CAPCompleted;
                le.CAPName = practiceFull._Entity.CAPName;
                le.CAPState = practiceFull._Entity.CAPState;
                le.CAPZip = practiceFull._Entity.CAPZip;
                le.ExecutedAgreementReturned = practiceFull._Entity.ExecutedAgreementReturned;
                le.InactiveDate = practiceFull._Entity.InactiveDate;
                le.InactiveNotes = practiceFull._Entity.InactiveNotes;
                le.InNetwork = (bool) practiceFull._Entity.InNetwork;

                bppEntities.SaveChanges();
                Audit.LegalEntityAudit(le, UpdateUser);
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }
            try
            {
                contracts = practiceFull._Contracts;
            }
            catch (Exception ec)
            {
                contracts = null;
            }

            if (contracts != null)
            {
                foreach (var item in contracts)
                {
                    try
                    {
                        // insert/update
                        //bppEntities.insUpdateOptOutByPractice(practiceFull._Entity.LegalEntityID, item.ContractID, Convert.ToInt32(item.OptOut));
                        var contract = bppEntities.ContractsOptOuts.Where(x => x.LegalEntityID == practiceFull._Entity.LegalEntityID && x.ContractID == item.ContractID).FirstOrDefault();
                        if (contract != null)
                        {
                            contract.OptOut = item.OptOut;
                            bppEntities.SaveChanges();
                            Audit.ContractsOptOutAudit(contract, UpdateUser);
                        }
                        else
                        {
                            var optout = new ContractsOptOut();
                            optout.ContractID = item.ContractID;
                            optout.LegalEntityID = practiceFull._Entity.LegalEntityID;
                            optout.OptOut = item.OptOut;
                            bppEntities.ContractsOptOuts.Add(optout);
                            bppEntities.SaveChanges();
                            Audit.ContractsOptOutAudit(optout, UpdateUser);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            return Ok();
       }
        [HttpGet]
        [Route("ISTINVALIDRETURNING")]
        [AllowCrosssiteJson]
        public IHttpActionResult IsTINVALIDRETURNING()
        {
            var bppEntities = new BPPEntities();

            try
            {
                var headers = Request.Headers;
                string TIN = "";

                try
                {
                    if (headers.Contains("TIN"))
                    {
                        TIN = headers.GetValues("TIN").First();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest("Invalid TIN");
                }
                using (BPPEntities db = new BPPEntities())
                {
                    var le = db.LegalEntities.Where(x => x.TaxID == TIN && x.InactiveDate == null).FirstOrDefault();
                    if (le != null)
                    {
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Invalid TIN");
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message + " " + e.InnerException);
            }
        }
        [HttpGet]
        [Route("ISTINVALIDAPPLICATION")]
        [AllowCrosssiteJson]
        public IHttpActionResult IsTINValidApplication()
        {
            var bppEntities = new BPPEntities();

            try
            {
                var headers = Request.Headers;
                string TIN = "";

                try
                {
                    if (headers.Contains("TIN"))
                    {
                        TIN = headers.GetValues("TIN").First();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest("Invalid TIN");
                }
                using (BPPEntities db = new BPPEntities())
                {
                    var le = db.LegalEntities.Where(x => x.TaxID == TIN && x.InactiveDate == null).FirstOrDefault();
                    if (le != null)
                    {
                       return BadRequest("Tin Exists");
                    }
                    else
                    {
                        return Ok();
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message + " " + e.InnerException);
            }
        }
        //[Route("GetLegalStatus")]
        //[HttpGet]
        //public IHttpActionResult GetLegalEntityStatus()
        //{
        //    var headers = Request.Headers;
        //    var bppEntities = new BPPEntities();
        //    string _LegalEntityStatusID = "";
        //    if (headers.Contains("ID"))
        //    {
        //        _LegalEntityStatusID = headers.GetValues("ID").First();
        //    }
        //    int statusid = Convert.ToInt32(_LegalEntityStatusID);
        //    try
        //    {
        //        var statusName = bppEntities.LegalEntityStatus.Where(i => i.ID == statusid).Select(i => i.StatusName).FirstOrDefault();
        //        return Json(statusName);
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest();
        //    }
        //}
    }
}
