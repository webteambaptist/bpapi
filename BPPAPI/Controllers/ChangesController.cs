﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for Survey 
    /// </summary>
    [RoutePrefix("api/changes")]
    public class ChangesController : ApiController
    {

        /// <summary>
        /// Get Screen Controls
        /// </summary>        
        /// <returns></returns>        
        [HttpGet]
        [Route("GETSCREENCONTROLS")]
        public IHttpActionResult GetScreenControls()
        {
            var results = new Changes();
            //var changes = new StatusChanges();
            var bppEntities = new BPPEntities();
            try
            {
                var StatusChange = bppEntities.StatusChanges.ToList();
                var StatusReason = bppEntities.StatusChangeReasons.OrderBy(x => x.StatusChangeReason1).ToList();
                //var Statuses = bppEntities.ProviderMemberStatus.ToList();

                results.Change = StatusChange;
                results.Reason = StatusReason;
                //results.Statuses = Statuses;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }



            return Json(results);
        }
        [HttpGet]
        [Route("ISTINVALID")]
        [AllowCrosssiteJson]
        public IHttpActionResult IsTINValid()
        {
            var bppEntities = new BPPEntities();

            try
            {
                var headers = Request.Headers;
                string TIN = "";

                try
                {
                    if (headers.Contains("TIN"))
                    {
                        TIN = headers.GetValues("TIN").First();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest("Invalid TIN");
                }
                using (BPPEntities db = new BPPEntities())
                {
                    var le = db.LegalEntities.Where(x => x.TaxID == TIN && x.InactiveDate == null).FirstOrDefault();
                    if (le != null)
                    {
                        if (db.Practices.Any(o => o.LegalEntityID == le.LegalEntityID))
                        {
                            return Ok();
                        }
                        else
                        {
                            return BadRequest("Invalid TIN");
                        }
                    }
                    else
                    {
                        return BadRequest("Invalid TIN");
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message + " " + e.InnerException);
            }
        }
        [HttpGet]
        [Route("ISTINUNIQUE")]
        [AllowCrosssiteJson]
        public IHttpActionResult IsTINUnique()
        {
            var bppEntities = new BPPEntities();

            try
            {
                var headers = Request.Headers;
                string TIN = "";

                try
                {
                    if (headers.Contains("TIN"))
                    {
                        TIN = headers.GetValues("TIN").First();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest("Invalid TIN");
                }
                using (BPPEntities db = new BPPEntities())
                {
                    var le = db.LegalEntities.Where(x => x.TaxID == TIN && x.InactiveDate == null).FirstOrDefault();
                    if (le != null)
                    {
                        return BadRequest("Invalid TIN");
                    }
                    else
                    {
                        return Ok();
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message + " " + e.InnerException);
            }
        }
        [HttpGet]
        [Route("ISNPIVALID")]
        [AllowCrosssiteJson]
        public IHttpActionResult IsNPIValid()
        {
            try
            {
                var headers = Request.Headers;
                string TIN = "";
                string NPI = "";

                try
                {
                    if (headers.Contains("TaxID"))
                    {
                        TIN = headers.GetValues("TaxID").First();
                    }
                    if (headers.Contains("ProviderNPI"))
                    {
                        NPI = headers.GetValues("ProviderNPI").First();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest("Invalid Data");
                }
                using (BPPEntities db = new BPPEntities())
                {
                    var legalEntity = db.LegalEntities.Where(x => x.TaxID == TIN).ToList();
                    foreach (var le in legalEntity)
                    {
                        var practices = db.Practices.Where(x => x.LegalEntityID == le.LegalEntityID).ToList();
                        foreach (var practice in practices)
                        {
                            if (db.ProviderPracticeMatrices.Any(o => o.PracticeGUID == practice.PracticeGUID && o.NPINumber == NPI))
                            {

                                return Ok();
                            }
                        }

                    }
                    return BadRequest("Invalid Data");

                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message + " " + e.InnerException);
            }
        }
        /// <summary>
        /// Submit Change Request
        /// </summary>        
        /// <returns></returns> 
        [HttpPost]
        [Route("REQUESTCHANGES")]
        public IHttpActionResult RequestChange([FromBody] Changes changes)
        {
            try
            {
                Guid guid = Guid.NewGuid();
                using (var context = new BPPEntities())
                {
                    var submitter = context.Set<ProviderUpdateSubmitter>();
                    submitter.Add(new ProviderUpdateSubmitter
                    {

                        SubmitterName = changes.SubmitterName,
                        SubmittersEmailAddress = changes.SubmitterEmailAddress,
                        SubmittersPhoneNumber = changes.SubmitterPhoneNumber,
                        SubmittersPhysicianGroup = changes.SubmitterPhysicianGroup,
                        SubmittersTIN = changes.SubmitterTIN,
                        Created = DateTime.Now,
                        GUID = guid
                    });

                    var providers = context.Set<ProviderUpdateStaging>();
                    foreach (var provider in changes._Providers)
                    {
                        providers.Add(new ProviderUpdateStaging
                        {
                            ProviderName = provider.ProviderName,
                            NationalProviderIdentification = provider.ProviderNPI,
                            EffectiveDate = provider.EffectiveDate,
                            StatusChange = provider.StatusChange,
                            StatusChangeReason = provider.StatusChangeReason,
                            Other = provider.Other,
                            Created = DateTime.Now,
                            Proccessed = 0,
                            SubmitterGUID = guid,
                            UpdateLastName = provider.UpdateLastName,
                            UpdateStatus = provider.UpdateStatus,
                            Practice = provider.Practice
                        });
                    }
                    context.SaveChanges();

                    SendSurveyChangeRequestEmail(changes);

                }
            }
            catch (Exception e)
            {
                BadRequest(e.InnerException.Message);
            }
            return Ok();
        }

        private void SendSurveyChangeRequestEmail(Changes changes)
        {
            var bppEntities = new BPPEntities();

            string htmlTableStart = "<table width=600 style=\"border-collapse:collapse; text-align:center;\" >";
            string htmlTableEnd = "</table>";
            string htmlHeaderRowStart = "<tr style =\"background-color:#000000; color:#ffffff;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlHeaderRowEnd = "</font></tr>";
            string htmlTrStart = "<tr style =\"color:#000000;\">";
            string htmlTrEnd = "</tr>";
            string htmlTdCol1Start = "<td width=150  style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlTdEnd = "</font></td>";
            string htmlTdCol2Start = "<td width=450 align=\"left\" style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string message = string.Empty;

            message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Submitter Information</td>" + htmlHeaderRowEnd;
            message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + changes.SubmitterName + htmlTdEnd + htmlTrEnd;
            message = message + htmlTrStart + htmlTdCol1Start + "TIN" + htmlTdEnd + htmlTdCol2Start + changes.SubmitterTIN + htmlTdEnd + htmlTrEnd;
            message = message + htmlTrStart + htmlTdCol1Start + "Email Adress" + htmlTdEnd + htmlTdCol2Start + changes.SubmitterEmailAddress + htmlTdEnd + htmlTrEnd;
            message = message + htmlTrStart + htmlTdCol1Start + "Phone Number" + htmlTdEnd + htmlTdCol2Start + changes.SubmitterPhoneNumber + htmlTdEnd + htmlTrEnd;
            message = message + htmlTrStart + htmlTdCol1Start + "Physician Group (Legal Business Name)" + htmlTdEnd + htmlTdCol2Start + changes.SubmitterPhysicianGroup + htmlTdEnd + htmlTrEnd;
            message = message + htmlTableEnd + "<BR><BR>";

            foreach (var provider in changes._Providers)
            {
                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Provider Information</td>" + htmlHeaderRowEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + provider.ProviderName + htmlTdEnd + htmlTrEnd;

                var practiceName = bppEntities.Practices.Where(x => x.PracticeGUID == provider.Practice).Select(x => x.PracticeName).FirstOrDefault();

                message = message + htmlTrStart + htmlTdCol1Start + "Practice" + htmlTdEnd + htmlTdCol2Start + practiceName + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Effective Date" + htmlTdEnd + htmlTdCol2Start + provider.EffectiveDate.ToShortDateString() + htmlTdEnd + htmlTrEnd;

                var changetype = bppEntities.StatusChanges.Where(s => s.ID == provider.StatusChange).Select(s => s.StatusChange1).FirstOrDefault();

                message = message + htmlTrStart + htmlTdCol1Start + "Change Type" + htmlTdEnd + htmlTdCol2Start + changetype + htmlTdEnd + htmlTrEnd;
                
                if (provider.UpdateLastName == null || provider.UpdateLastName == "")
                {
                    var statuschangereason = bppEntities.StatusChangeReasons.Where(sc => sc.ID == provider.StatusChangeReason).Select(sc => sc.StatusChangeReason1).FirstOrDefault();

                    message = message + htmlTrStart + htmlTdCol1Start + "Status Change Reason" + htmlTdEnd + htmlTdCol2Start + statuschangereason + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Name Change" + htmlTdEnd + htmlTdCol2Start + "" + htmlTdEnd + htmlTrEnd;                    
                }
                else
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Name Change" + htmlTdEnd + htmlTdCol2Start + provider.UpdateLastName + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Status Change Reason" + htmlTdEnd + htmlTdCol2Start + "" + htmlTdEnd + htmlTrEnd;
                }
                    message = message + htmlTrStart + htmlTdCol1Start + "Additional Notes" + htmlTdEnd + htmlTdCol2Start + provider.Other + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTableEnd + "<BR><BR>";
            }

            Message m = new Message();
            m.Sender = ConfigurationManager.AppSettings["sender"].ToString();
            m.Recipient = ConfigurationManager.AppSettings["recipient"];
            m.Subject = "Survey Change Request Submitted";
            m.Body = message;

            new MessageController().sendMessage(m);
        }


        /// <summary>
        /// Get List of Changes
        /// </summary>        
        /// <returns></returns> 
        [HttpGet]
        [Route("GETCHANGES")]
        public IHttpActionResult GetChanges()
        {
            try
            {
                BPPEntities bppEntities = new BPPEntities();
                var approvalList = new ChangeList();
                var cs = (from ps in bppEntities.ProviderUpdateStagings
                          join sub in bppEntities.ProviderUpdateSubmitters on ps.SubmitterGUID equals sub.GUID
                          join s in bppEntities.StatusChanges on ps.StatusChange equals s.ID
                          where ps.Proccessed == 0
                          select new Change
                          {
                              SubmittedDate = ps.Created,
                              EffectiveDate = ps.EffectiveDate,
                              SubmitterGroup = sub.SubmittersPhysicianGroup,
                              ProviderName = ps.ProviderName,
                              ProviderNPI = ps.NationalProviderIdentification,
                              ChangeReason = s.StatusChange1,
                              NameChange = ps.UpdateLastName,
                              SubmitterTIN = sub.SubmittersTIN,
                              Id = ps.ID,
                              Practice = ps.Practice
                              //StatusChange = ps.StatusChange
                          }).ToList();
                approvalList._ChangeList = cs;
                return Ok(approvalList);
            } catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }
        /// <summary>
        /// Save Changes to Live Tables
        /// </summary>        
        /// <returns></returns> 
        [HttpPost]
        [Route("ACCEPTCHANGES")]
        public IHttpActionResult AcceptChanges([FromBody] UpdateChanges changeList)
        {
            foreach (Change item in changeList.Change)
            {
                BPPEntities entites = new BPPEntities();
                try
                {
                    var provider = entites.Providers.Where(p => p.NPINumber == item.ProviderNPI).FirstOrDefault();
                    // var memberStatus = entites.ProviderMemberStatusMatrices.Where(p => p.NPINumber == item.ProviderNPI).FirstOrDefault();
                    // Update the Live Tables
                    if (item.ChangeReason.Equals("Name Change"))
                    {
                        // assuming it's Name Change 
                        if (provider != null)
                        {
                            try
                            {
                                var oldValue = provider.LastName;
                                provider.LastName = item.NameChange;

                                //Insert Old Name into ProviderPracticeWorkflow table notes section
                                try
                                {
                                    var _ppw = entites.ProviderPracticeWorkflows.Where(ppw => ppw.NPINumber == item.ProviderNPI && ppw.PracticeGUID == item.Practice).FirstOrDefault();

                                    DateTime date = Convert.ToDateTime(item.EffectiveDate);
                                    var _dateChange = date.ToShortDateString();
                                    _ppw.OutcomeNotes = "Name Change from (" + oldValue + ") on " + _dateChange;
                                }
                                catch (Exception ex)
                                {
                                    return BadRequest(ex.InnerException.Message);
                                }                               
                                entites.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                return BadRequest(ex.Message);
                            }
                        }
                    }
                    else if (item.ChangeReason.Equals("Status Change"))
                    {
                        try
                        {
                            //Update the Provider info into ProviderPracticeWorkflow
                            var _ppw = entites.ProviderPracticeWorkflows.Where(ppw => ppw.NPINumber == item.ProviderNPI && ppw.PracticeGUID == item.Practice).FirstOrDefault();

                            //Set InactiveReasonStatusID to the selected Status Change reason from survey
                            var _surveyChangeInfo = entites.ProviderUpdateStagings.Where(s => s.ID == item.Id && s.NationalProviderIdentification == item.ProviderNPI).FirstOrDefault();
                            //Status Change Reason 10 = Other 
                            if (_surveyChangeInfo.StatusChangeReason == 10)
                            {
                                //If Status Change Reason "Other" is selected set the Inactive Status Reason to "Unknown"
                                var _inactiveReasonStatus = entites.InactiveReasonStatus.Where(i => i.StatusName == "Unknown").Select(i => i.ID).FirstOrDefault();
                                _ppw.InactiveReasonStatusID = _inactiveReasonStatus;
                            }
                            else
                            {
                                var _inactiveReasonStatus = entites.InactiveReasonStatus.Where(i => i.ID == _surveyChangeInfo.StatusChangeReason).Select(i => i.ID).FirstOrDefault();
                                _ppw.InactiveReasonStatusID = _inactiveReasonStatus;
                            }
                            //var inactivestatus = entites.ProviderMemberStatus.Where(p => p.MemberStatus == "Inactive").Select(p => p.StatusID).FirstOrDefault();
                            // _ppw.ProviderStatusID = inactivestatus;

                            _ppw.ProviderInactiveDate = item.EffectiveDate;

                            //DateTime date = Convert.ToDateTime(_ppw.ProviderInactiveDate);
                            //var _dateChange = date.ToShortDateString();
                            if (_surveyChangeInfo.Other == "")
                            {
                                _ppw.OutcomeNotes = "Status Change approved on " + DateTime.Now.ToShortDateString();
                            }
                            else
                            {
                                _ppw.OutcomeNotes = "Status Change to and approved (" + _surveyChangeInfo.Other + ") on " + DateTime.Now.ToShortDateString();
                            }                           
                            //_ppw.OutcomeNotes = _surveyChangeInfo.Other;
                        }
                        catch (Exception e)
                        {
                            return BadRequest(e.InnerException.Message);
                        }                       
                        entites.SaveChanges();
                    }
                    try
                    {
                        // Mark the Record Complete
                        var staging = entites.ProviderUpdateStagings.Where(s => s.ID == item.Id && s.NationalProviderIdentification == item.ProviderNPI).FirstOrDefault();
                        //var staging = entites.ProviderUpdateStagings.FirstOrDefault(s => s.NationalProviderIdentification == item.ProviderNPI);
                        if (staging != null)
                        {
                            staging.Proccessed = 1;
                            entites.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            return Ok();
        }
        /// <summary>
        /// Reject Changes
        /// </summary>        
        /// <returns></returns> 
        [HttpPost]
        [Route("REJECTCHANGES")]
        public IHttpActionResult RejectChanges([FromBody] UpdateChanges changeList)
        {
            foreach (Change item in changeList.Change)
            {
                BPPEntities entites = new BPPEntities();
                try
                {

                    // Mark the Record Complete
                    var staging = entites.ProviderUpdateStagings.Where(s => s.ID == item.Id && s.NationalProviderIdentification == item.ProviderNPI).FirstOrDefault();
                    //var staging = entites.ProviderUpdateStagings.FirstOrDefault(s => s.NationalProviderIdentification == item.ProviderNPI);
                    if (staging != null)
                    {
                        staging.Proccessed = 2; // marking 2 for rejected to know the difference
                        entites.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return Ok();
        }
        /// Get List of Changes
        /// </summary>        
        /// <returns></returns> 
        [HttpGet]
        [Route("GETSURVEYDETAILS")]
        public IHttpActionResult GetSurveyDetails()
        {
            try
            {
                string ProviderNPI = null;
                string SubmitterTIN = null;
                string Id = null;
                var headers = Request.Headers;

                if (headers.Contains("ProviderNPI"))
                {
                    ProviderNPI = headers.GetValues("ProviderNPI").First();
                }

                if (headers.Contains("SubmitterTIN"))
                {
                    SubmitterTIN = headers.GetValues("SubmitterTIN").First();
                }

                if (headers.Contains("Id"))
                {
                    Id = headers.GetValues("Id").First();
                }
                int _ID = Convert.ToInt32(Id);
                BPPEntities bppEntities = new BPPEntities();
                var details = new ChangeDetails();

                var cs = (from ps in bppEntities.ProviderUpdateStagings where ps.NationalProviderIdentification == ProviderNPI
                          join sub in bppEntities.ProviderUpdateSubmitters on ps.SubmitterGUID equals sub.GUID where sub.SubmittersTIN == SubmitterTIN
                          join s in bppEntities.StatusChanges on ps.StatusChange equals s.ID
                          join p in bppEntities.Practices on ps.Practice equals p.PracticeGUID
                          where ps.ID == _ID
                          select new ChangeDetails
                          {
                              Id = ps.ID,
                              SubmitterName = sub.SubmitterName,
                              SubmitterGroup = sub.SubmittersPhysicianGroup,
                              SubmittedDate = ps.Created,
                              SubmitterTIN = sub.SubmittersTIN,
                              SubmitterEmail = sub.SubmittersEmailAddress,
                              SubmitterPhone = sub.SubmittersPhoneNumber,
                              ProviderNPI = ps.NationalProviderIdentification,
                              ProviderName = ps.ProviderName,
                              EffectiveDate = ps.EffectiveDate,
                              ChangeReason = s.StatusChange1,
                              NameChange = ps.UpdateLastName,
                              Other = ps.Other,
                              PracticeName = p.PracticeName,
                              Practice = ps.Practice,
                              ProcessedResults = ps.Proccessed
                          }).FirstOrDefault();
                details = cs;
                var reason = (from ps in bppEntities.ProviderUpdateStagings where ps.NationalProviderIdentification == ProviderNPI
                              join r in bppEntities.StatusChangeReasons on ps.StatusChangeReason equals r.ID
                              where ps.ID == _ID
                              select r.StatusChangeReason1).FirstOrDefault();
                details.StatusChangeReason = reason;

                return Ok(details);
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }
        /// <summary>
        /// Get Practices based on LegalEntity
        /// </summary>        
        /// <returns></returns> 
        [HttpGet]
        [Route("getpracticesforsurvery")]
        public IHttpActionResult GetPracticesforSurvery()
        {
            try
            {
                string SubmitterTIN = null;
                var headers = Request.Headers;

                if (headers.Contains("TIN"))
                {
                    SubmitterTIN = headers.GetValues("TIN").First();
                }
                BPPEntities bppEntities = new BPPEntities();

                var _legalEntityID = bppEntities.LegalEntities.Where(l => l.TaxID == SubmitterTIN).Select(l => l.LegalEntityID).FirstOrDefault();

                var _practices = bppEntities.Practices.Where(p => p.LegalEntityID == _legalEntityID).ToList();
                return Ok(_practices);

            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }

        /// <summary>
        /// Get List of processed surveys
        /// </summary>        
        /// <returns></returns> 
        [HttpGet]
        [Route("GETSURVEYARCHIVELIST")]
        public IHttpActionResult GetSurveyArchiveList()
        {
            try
            {
                BPPEntities bppEntities = new BPPEntities();
                var approvalList = new ChangeList();
                var cs = (from ps in bppEntities.ProviderUpdateStagings
                          join sub in bppEntities.ProviderUpdateSubmitters on ps.SubmitterGUID equals sub.GUID
                          join s in bppEntities.StatusChanges on ps.StatusChange equals s.ID
                          where ps.Proccessed != 0
                          select new Change
                          {
                              SubmittedDate = ps.Created,
                              EffectiveDate = ps.EffectiveDate,
                              SubmitterGroup = sub.SubmittersPhysicianGroup,
                              ProviderName = ps.ProviderName,
                              ProviderNPI = ps.NationalProviderIdentification,
                              ChangeReason = s.StatusChange1,
                              NameChange = ps.UpdateLastName,
                              SubmitterTIN = sub.SubmittersTIN,
                              Id = ps.ID,
                              Practice = ps.Practice,
                              ProcessedResults = ps.Proccessed
                              //StatusChange = ps.StatusChange
                          }).ToList();               
                approvalList._ChangeList = cs;
                return Ok(approvalList);
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }


    }
    
}
 
