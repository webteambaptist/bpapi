﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
//using System.Data.Objects.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;
using System.Collections;
namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for Practice Data
    /// </summary>
    
    [RoutePrefix("api/practices")]
    public class PracticeController : ApiController
    {
        /// <summary> I don't believe this is being used
        /// Get all practice data by GUID
        /// </summary>
        /// <returns></returns>
        [Route("SELECTPRACTICE")]
        [HttpGet]
        public IHttpActionResult selPracticeByGuid()
        {
            var headers = Request.Headers;
            string GUID = "";

            try
            {
                if (headers.Contains("GUID"))
                {
                    GUID = headers.GetValues("GUID").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }

            var bppEntities = new BPPEntities();
            var results = new PracticeDetail();
            var practices = bppEntities.Practices;
            var locations = bppEntities.Locations;
            var providers = bppEntities.Providers;
            var facilities = bppEntities.Facilities;
            
            var pLocations = new ProviderLocation();
            //check for key and status 4, if so, return null
            try
            {
                if (!string.IsNullOrEmpty(GUID))
                {

                    Guid practiceGuid = new Guid(GUID);
                    var pract = practices.Where(x => x.PracticeGUID == practiceGuid).FirstOrDefault();

                    try
                    {
                        PracticeExt ex = new PracticeExt();
                        ex.CreatedDT = pract.CreatedDT;
                        ex.PracticeGUID = pract.PracticeGUID.ToString();
                        ex.ID = pract.ID;
                        ex.LegalEntityID = pract.LegalEntityID.ToString();
                        ex.ModifiedDT = pract.ModifiedDT;
                        ex.PracticeApprovalStatus = pract.PracticeApprovalStatus;
                        var Location = bppEntities.Locations.Where(p => p.PracticeGUID == practiceGuid).ToList();
                        ex.PracticeLocations = new List<PracticeLocations>();
                        
                        // For each Location we need a list of providers.
                        foreach (var _locations in Location)
                        {
                            if (!_locations.isRemoved)
                            {
                                PracticeLocations p = new PracticeLocations();
                                p.Providers = new List<ProviderFull>();
                                p.Address1 = _locations.Address1;
                                p.Address2 = _locations.Address2;
                                p.BillingManagerEmail = _locations.BillingManagerEmail;
                                p.BillingManagerExtension = _locations.BillingManagerExtension;
                                p.BillingManagerName = _locations.BillingManagerName;
                                p.BillingManagerPhoneNumber = _locations.BillingManagerPhoneNumber;
                                p.City = _locations.City;
                                p.CreatedDT = _locations.CreatedDT;
                                p.EMRVendor = _locations.EMRVendor;
                                p.EMRVersion = _locations.EMRVersion;
                                p.Extension = _locations.Extension;
                                p.Fax = _locations.Fax;
                                p.ID = _locations.ID;
                                p.IsPrimary = _locations.IsPrimary;
                                p.LocationGUID = _locations.LocationGUID;
                                p.LocationName = _locations.LocationName;
                                p.ModifiedDT = _locations.ModifiedDT;
                                p.OfficeManagerEmail = _locations.OfficeManagerEmail;
                                p.OfficeManagerExtension = _locations.OfficeManagerExtension;
                                p.OfficeManagerName = _locations.OfficeManagerName;
                                p.OfficeManagerPhoneNumber = _locations.OfficeManagerPhoneNumber;
                                p.PhoneNumber = _locations.PhoneNumber;
                                p.PracticeGUID = _locations.PracticeGUID;
                                p.State = _locations.State;
                                p.Zip = _locations.Zip;
                                p.isRemoved = _locations.isRemoved;
                                List<ProviderFull> provs = getProvidersForLocation(bppEntities, pract, _locations);
                                p.Providers = provs;
                                ex.PracticeLocations.Add(p);
                            }
                        }
                        
                        ex.PracticeName = pract.PracticeName;
                        results._Practices = new List<PracticeExt>();
                        results._Practices.Add(ex);
                    }
                    catch (Exception ee)
                    {
                        return BadRequest(ee.Message);
                    }

                    results._Providers = new List<ProviderFull>();
                    results._Providers = getProvidersForLocation(bppEntities, pract, null);
                    LegalEntity legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == pract.LegalEntityID).FirstOrDefault();
                    
                    results._Facilities = new List<Facility>();
                    results._Facilities = bppEntities.Facilities.ToList();
                    results._Specialties = bppEntities.SpecialtyMatrices.ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }


            return Json(results);
        }
        public static List<ProviderFull> getProvidersForLocation(BPPEntities bppEntities, Practice pract, Location location)
        {
            var providerfacs = bppEntities.ProviderFacilities;
            List<ProviderFull> providerList = new List<ProviderFull>();
            List<Provider> list = new List<Provider>();
            List<int> npis = new List<int>();
            // get a list of ProviderLocations in that location
            if (location != null)
            {
                var locs = bppEntities.ProviderLocations.Where(x => x.LocationID == location.ID && x.PracticeGUID == pract.PracticeGUID).Select(x => x.ProviderPracticeMatrixID).ToList();
                // get NPIs for this Location
                
                foreach (var loc in locs)
                {
                    //npis.Add(bppEntities.ProviderPracticeMatrices
                    //    .Where(x => x.PracticeGUID == pract.PracticeGUID && x.EndDate == null)// don't include providers with an end date
                    //    .Where(x => x.NPINumber == loc.NPINumber)
                    //    .Select(x => x.NPINumber).FirstOrDefault());
                    var test = bppEntities.ProviderPracticeMatrices
                        .Where(x => x.PracticeGUID == pract.PracticeGUID && x.EndDate == null)// don't include providers with an end date
                        .Where(x => x.ID == loc)
                        .Select(x => x.ID).FirstOrDefault();

                    npis.Add(test);
                    //npis = bppEntities.ProviderPracticeMatrices
                    //    .Where(x => x.PracticeGUID == pract.PracticeGUID && x.EndDate == null)// don't include providers with an end date
                    //    .Where(x => x.NPINumber == loc.NPINumber)
                    //    .Select(x => x.ID).FirstOrDefault());
                }
            }
            else
            {
                npis = bppEntities.ProviderPracticeMatrices
                       .Where(x => x.PracticeGUID == pract.PracticeGUID && x.EndDate == null)// don't include providers with an end date
                       .Select(x => x.ID).ToList();
            }
            foreach (var npi in npis)
            {
                var providerNPI = bppEntities.ProviderPracticeMatrices.Where(x => x.ID == npi).Select(x => x.NPINumber).FirstOrDefault();

                var _providerList = bppEntities.Providers.Where(x => x.NPINumber == providerNPI).ToList();
                foreach (var pitem in _providerList)
                {
                    list.Add(pitem);
                    try
                    {
                        var matrix = bppEntities.ProviderPracticeMatrices.Where(x => x.PracticeGUID == pract.PracticeGUID && x.NPINumber == pitem.NPINumber && x.ID == npi).FirstOrDefault();

                        var ancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == pitem.NPINumber && x.PracticeGuid == pract.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).FirstOrDefault();
                        ProviderFull pf = new ProviderFull();
                        pf.matrix = matrix;
                        if (ancillary != null)
                        {
                            ProviderAncillary a = new ProviderAncillary();
                            a.AKA = ancillary.AKA;
                            a.EmploymentStatus = ancillary.EmploymentStatus;
                            a.Maiden_Name = ancillary.Maiden_Name;
                            a.ServicePopulation = ancillary.ServicePopulation;
                            a.SpecialtyType = ancillary.SpecialtyType;
                            pf.ProviderAncillary = a;
                        }
                        pf.CMS = pitem.CMS;
                        pf.CreatedDT = pitem.CreatedDT;
                        pf.DateOfBirth = pitem.DateOfBirth;
                        var degree = bppEntities.DegreeTypes.Where(x => x.DegreeID == pitem.DegreeID).FirstOrDefault();
                        pf.Degree = degree.Degree;
                        pf.Email = pitem.Email;
                        pf.Facilties = bppEntities.Facilities.ToList();
                        pf.FirstName = pitem.FirstName;
                        pf.GUID = pitem.GUID;
                        pf.ID = pitem.ID;
                        pf.isBHPrivileged = pitem.isBHPrivileged;
                        pf.LastName = pitem.LastName;
                        pf.Locations = bppEntities.ProviderLocations.Where(e => e.NPINumber == pitem.NPINumber && e.PracticeGUID == pract.PracticeGUID && e.ProviderPracticeMatrixID == matrix.ID).ToList();
                        pf.ProviderMemberStatus = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == pitem.NPINumber && x.PracticeGUID == pract.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).Select(x => x.ProviderStatusID).FirstOrDefault();
                        pf.MiddleName = pitem.MiddleName;
                        pf.ModifiedDT = pitem.ModifiedDT;
                        pf.NPINumber = pitem.NPINumber;
                        pf.PhoneNumber = pitem.PhoneNumber;
                        var prPracticeWF = bppEntities.ProviderPracticeWorkflows.Where(e => e.PracticeGUID == pract.PracticeGUID && e.NPINumber == pitem.NPINumber && e.ProviderPracticeMatrixID == matrix.ID).FirstOrDefault();
                        if (prPracticeWF != null)
                        {
                            pf.ProviderPracticeWorkFlow = prPracticeWF;
                            pf.PrimaryPracticeLocation = prPracticeWF.PrimaryPracticeLocation;
                        }
                        else
                        {
                            pf.ProviderPracticeWorkFlow = new ProviderPracticeWorkflow();
                            pf.PrimaryPracticeLocation = "0";
                        }
                        pf.MemberStatus = bppEntities.ProviderMemberStatus.Where(e => e.StatusID == pf.ProviderMemberStatus).FirstOrDefault();

                        var specialties = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == pitem.NPINumber && e.ProviderPracticeMatrixID == matrix.ID).ToList();
                        foreach (var spec in specialties)
                        {
                            try
                            {
                                var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == spec.SpecialtyID).FirstOrDefault();
                                if (spec.IsPrmary == true)
                                {
                                    pf.PrimarySpecialty = specLookup.Specialty;
                                }
                                else
                                {
                                    pf.SecondarySpecialty = specLookup.Specialty;
                                }
                            }
                            catch (Exception e)
                            {
                                continue;
                            }
                        }
                        pf.ProviderSpecialties = specialties;
                        pf.Specialties = bppEntities.SpecialtyMatrices.ToList();
                        pf.Suffix = pitem.Suffix;
                        //pf.TaxID = pitem.TaxID;

                        pf.OtherPracticeLocations = new List<int>();

                        if (pf.Locations.Count<ProviderLocation>() > 0)
                        {
                            foreach (var l in pf.Locations)
                            {
                                try
                                {
                                    pf.OtherPracticeLocations.Add(l.LocationID);
                                }
                                catch (Exception e)
                                {
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            pf.OtherPracticeLocations = new List<int>();
                        }

                        var facs = providerfacs.Where(x => x.NPINumber == pitem.NPINumber && x.PracticeGUID == pract.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID);
                        pf.ProviderFacilities = new List<int>();

                        if (facs.Count<ProviderFacility>() > 0)
                        {
                            foreach (var f in facs)
                            {
                                try
                                {
                                    pf.ProviderFacilities.Add(f.FacilityID);
                                }
                                catch (Exception e)
                                {
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            pf.ProviderFacilities = null;
                        }

                        //results._Providers.Add(pf);
                        providerList.Add(pf);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
                //list.Add(bppEntities.Providers.Where(x => x.NPINumber == providerNPI).FirstOrDefault());
            }
            // get a list of providers in this locaiton only

            //foreach (var pitem in list)
            //{
            //    try
            //    {
            //        var matrix = bppEntities.ProviderPracticeMatrices.Where(x => x.PracticeGUID == pract.PracticeGUID && x.NPINumber == pitem.NPINumber).FirstOrDefault();

            //        var ancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == pitem.NPINumber && x.PracticeGuid == pract.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).FirstOrDefault();
            //        ProviderFull pf = new ProviderFull();
            //        pf.matrix = matrix;
            //        if (ancillary != null)
            //        {
            //            ProviderAncillary a = new ProviderAncillary();
            //            a.AKA = ancillary.AKA;
            //            a.EmploymentStatus = ancillary.EmploymentStatus;
            //            a.Maiden_Name = ancillary.Maiden_Name;
            //            a.ServicePopulation = ancillary.ServicePopulation;
            //            a.SpecialtyType = ancillary.SpecialtyType;
            //            pf.ProviderAncillary = a;
            //        }
            //        pf.CMS = pitem.CMS;
            //        pf.CreatedDT = pitem.CreatedDT;
            //        pf.DateOfBirth = pitem.DateOfBirth;
            //        var degree = bppEntities.DegreeTypes.Where(x => x.DegreeID == pitem.DegreeID).FirstOrDefault();
            //        pf.Degree = degree.Degree;
            //        pf.Email = pitem.Email;
            //        pf.Facilties = bppEntities.Facilities.ToList();
            //        pf.FirstName = pitem.FirstName;
            //        pf.GUID = pitem.GUID;
            //        pf.ID = pitem.ID;
            //        pf.isBHPrivileged = pitem.isBHPrivileged;
            //        pf.LastName = pitem.LastName;
            //        pf.Locations = bppEntities.ProviderLocations.Where(e => e.NPINumber == pitem.NPINumber && e.PracticeGUID == pract.PracticeGUID && e.ProviderPracticeMatrixID == matrix.ID).ToList();
            //        pf.ProviderMemberStatus = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == pitem.NPINumber && x.PracticeGUID == pract.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).Select(x => x.ProviderStatusID).FirstOrDefault();
            //        pf.MiddleName = pitem.MiddleName;
            //        pf.ModifiedDT = pitem.ModifiedDT;
            //        pf.NPINumber = pitem.NPINumber;
            //        pf.PhoneNumber = pitem.PhoneNumber;
            //        var prPracticeWF = bppEntities.ProviderPracticeWorkflows.Where(e => e.PracticeGUID == pract.PracticeGUID && e.NPINumber == pitem.NPINumber && e.ProviderPracticeMatrixID == matrix.ID).FirstOrDefault();
            //        if (prPracticeWF != null)
            //        {
            //            pf.ProviderPracticeWorkFlow = prPracticeWF;
            //            pf.PrimaryPracticeLocation = prPracticeWF.PrimaryPracticeLocation;
            //        }
            //        else
            //        {
            //            pf.ProviderPracticeWorkFlow = new ProviderPracticeWorkflow();
            //            pf.PrimaryPracticeLocation = "0";
            //        }
            //        pf.MemberStatus = bppEntities.ProviderMemberStatus.Where(e => e.StatusID == pf.ProviderMemberStatus).FirstOrDefault();

            //        var specialties = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == pitem.NPINumber && e.ProviderPracticeMatrixID == matrix.ID).ToList();
            //        foreach (var spec in specialties)
            //        {
            //            try
            //            {
            //                var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == spec.SpecialtyID).FirstOrDefault();
            //                if (spec.IsPrmary == true)
            //                {
            //                    pf.PrimarySpecialty = specLookup.Specialty;
            //                }
            //                else
            //                {
            //                    pf.SecondarySpecialty = specLookup.Specialty;
            //                }
            //            }
            //            catch (Exception e)
            //            {
            //                continue;
            //            }
            //        }
            //        pf.ProviderSpecialties = specialties;
            //        pf.Specialties = bppEntities.SpecialtyMatrices.ToList();
            //        pf.Suffix = pitem.Suffix;
            //        //pf.TaxID = pitem.TaxID;

            //        pf.OtherPracticeLocations = new List<int>();

            //        if (pf.Locations.Count<ProviderLocation>() > 0)
            //        {
            //            foreach (var l in pf.Locations)
            //            {
            //                try
            //                {
            //                    pf.OtherPracticeLocations.Add(l.LocationID);
            //                }
            //                catch (Exception e)
            //                {
            //                    continue;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            pf.OtherPracticeLocations = new List<int>();
            //        }

            //        var facs = providerfacs.Where(x => x.NPINumber == pitem.NPINumber && x.PracticeGUID == pract.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID);
            //        pf.ProviderFacilities = new List<int>();

            //        if (facs.Count<ProviderFacility>() > 0)
            //        {
            //            foreach (var f in facs)
            //            {
            //                try
            //                {
            //                    pf.ProviderFacilities.Add(f.FacilityID);
            //                }
            //                catch (Exception e)
            //                {
            //                    continue;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            pf.ProviderFacilities = null;
            //        }

            //        //results._Providers.Add(pf);
            //        providerList.Add(pf);
            //    }
            //    catch (Exception ee)
            //    {
            //        continue;
            //    }
            //}
            return providerList;
        }
        [Route("REMOVELOCATION")]
        [HttpPost]
        public IHttpActionResult RemoveLocation()
        {
            string LocationID = "";
            var headers = Request.Headers;
            var bppEntities = new BPPEntities();
            try
            {
                if (headers.Contains("LocationID"))
                {
                    LocationID = headers.GetValues("LocationID").First();
                }

                int locID = int.Parse(LocationID);
                var location = bppEntities.Locations.Where(x => x.ID == locID).FirstOrDefault();
                location.isRemoved = true;
                bppEntities.SaveChanges();
                return Ok();
            }
            catch (Exception ee)
            {
                return BadRequest();
            }
        }
    }
}
