﻿using BPPAPI.Helpers;
using BPPAPI.Models;
//using BPPAPI.PRODBPP;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for generating PDF file.
    /// </summary>

    [RoutePrefix("api/pdf")]
    public class PDFGeneratorController : ApiController
    {
        string url = ConfigurationManager.AppSettings["SP"].ToString();
        
        [Route("createbaapa")]
        [HttpPost]
        public IHttpActionResult createdocs()
        {
            var headers = Request.Headers;
            string ActivationKey = "";
            try
            {
                if (headers.Contains("ActivationKey"))
                {
                    ActivationKey = headers.GetValues("ActivationKey").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest("Value missing from the header");
            }

            var path = ConfigurationManager.AppSettings["FILESERVERPATH"];
            var pdfurl = ConfigurationManager.AppSettings["FILESERVERURL"];
            var bppEntities = new BPPEntities();
            var downloads = new Downloads();
            var files = new List<BppDocument>();

            //Get LE_GUID based on ActivationKey
            Guid guid_parm;
            string _guid = Crypto.Decrypt(ActivationKey.ToString());
            guid_parm = Guid.Parse(_guid);
            var applicant = bppEntities.NewMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
            var le_guid = applicant.LE_Guid;

            string LE_GUID = le_guid.ToString();

            var convertedGuid = new Guid();
            bool isValid = Guid.TryParse(LE_GUID, out convertedGuid);
            if (!isValid)
            {
                return BadRequest("Improperly formatted GUID");
            }

            try
            {
                ArrayList baaList = generateBAAPDF(convertedGuid);
                if (baaList != null)
                {
                    byte[] bytes = (byte[])baaList[0];
                    string filename = baaList[1].ToString();

                    System.IO.File.WriteAllBytes(path + filename, bytes);

                    var link = pdfurl + filename;
                    var pdfFile = new BppDocument();

                    pdfFile.Title = "Business Associate Agreement";
                    pdfFile.Link = link;

                    files.Add(pdfFile);
                }

                ArrayList bppList = generateBPPPDF(convertedGuid);
                if (bppList != null)
                {
                    byte[] bytes = (byte[])bppList[0];
                    string filename = bppList[1].ToString();

                    System.IO.File.WriteAllBytes(path + filename, bytes);

                    var link = pdfurl + filename;
                    var pdfFile = new BppDocument();

                    pdfFile.Title = "Participation Agreement";
                    pdfFile.Link = link;

                    files.Add(pdfFile);
                }

                downloads.DownloadLinks = files;
            }
            catch (Exception ex)
            {

            }

            if (downloads != null)
            {
                return Ok(downloads);
            }
            else
            {
                return BadRequest("Documents failed to be created");
            }
        }

        [Route("createcombined")]
        [HttpPost]
        public IHttpActionResult createcombined()
        {
            var headers = Request.Headers;
            string ActivationKey = "";
            try
            {
                if (headers.Contains("ActivationKey"))
                {
                    ActivationKey = headers.GetValues("ActivationKey").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest("Value missing from the header");
            }

            var path = ConfigurationManager.AppSettings["FILESERVERPATH"];
            var pdfurl = ConfigurationManager.AppSettings["FILESERVERURL"];
            var bppEntities = new BPPEntities();
            var downloads = new Downloads();
            var files = new List<BppDocument>();

            //Get LE_GUID based on ActivationKey
            Guid guid_parm;
            string _guid = Crypto.Decrypt(ActivationKey.ToString());

            guid_parm = Guid.Parse(_guid);

            var applicant = bppEntities.NewMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
            var le_guid = applicant.LE_Guid;

            string LE_GUID = le_guid.ToString();

            var convertedGuid = new Guid();
            bool isValid = Guid.TryParse(LE_GUID, out convertedGuid);
            if (!isValid)
            {
                return BadRequest("Improperly formatted GUID");
            }

            try
            {
                ArrayList baaList = generateCombinedPDF(convertedGuid);
                if (baaList != null)
                {
                    byte[] bytes = (byte[])baaList[0];
                    string filename = baaList[1].ToString();

                    System.IO.File.WriteAllBytes(path + filename, bytes);

                    var link = pdfurl + filename;
                    var pdfFile = new BppDocument();

                    pdfFile.Title = "Business Associate and Participation Agreements";
                    pdfFile.Link = link;

                    files.Add(pdfFile);
                }

                downloads.DownloadLinks = files;
            }
            catch (Exception ex)
            {

            }

            if (downloads != null)
            {
                return Ok(downloads);
            }
            else
            {
                return BadRequest("Documents failed to be created");
            }
        }
        // Not being used
        //[Route("createjoinder")]
        //[HttpPost]
        //public IHttpActionResult createJoinder()
        //{
        //    var headers = Request.Headers;

        //    string NPINumber = "";

        //    if (headers.Contains("NPINumber"))
        //    {
        //        NPINumber = headers.GetValues("NPINumber").First();
        //    }
        //    else
        //    {
        //        return BadRequest("Value missing from the header");
        //    }

        //    var path = ConfigurationManager.AppSettings["FILESERVERPATH"];
        //    var pdfurl = ConfigurationManager.AppSettings["FILESERVERURL"];
        //    var bppEntities = new BPPEntities();
        //    var downloads = new Downloads();
        //    var files = new List<BppDocument>();

        //    string _npi = Crypto.Decrypt(NPINumber.ToString());

        //    try
        //    {
        //        ArrayList joinderList = generateJoinderPDF(_npi);
        //        if (joinderList != null)
        //        {
        //            byte[] bytes = (byte[])joinderList[0];
        //            string filename = joinderList[1].ToString();

        //            System.IO.File.WriteAllBytes(path + filename, bytes);

        //            var link = pdfurl + filename;
        //            var pdfFile = new BppDocument();

        //            pdfFile.Title = "Joinder Agreement";
        //            pdfFile.Link = link;

        //            files.Add(pdfFile);
        //        }
        //        downloads.DownloadLinks = files;
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    if (downloads != null)
        //    {
        //        return Ok(downloads);
        //    }
        //    else
        //    {
        //        return BadRequest("Documents failed to be created");
        //    }
        //}

        [Route("getstaticjoinder")]
        [HttpPost]
        public IHttpActionResult getStaticJoinder()
        {
            var headers = Request.Headers;
            string ActivationKey = "";
            try
            {
                if (headers.Contains("ActivationKey"))
                {
                    ActivationKey = headers.GetValues("ActivationKey").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest("Value missing from the header");
            }

            var path = ConfigurationManager.AppSettings["FILESERVERPATH"];
            var pdfurl = ConfigurationManager.AppSettings["FILESERVERURL"];
            var bppEntities = new BPPEntities();
            var downloads = new Downloads();
            var files = new List<BppDocument>();

            //Get LE_GUID based on ActivationKey
            Guid guid_parm;
            string _guid = Crypto.Decrypt(ActivationKey.ToString());
            guid_parm = Guid.Parse(_guid);
            var applicant = bppEntities.NewMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
            var _returningapplicant = bppEntities.ReturningMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
            Guid? le_guid;
            if (applicant == null)
            {
                le_guid = _returningapplicant.LE_GUID;
            }
            else
            {
                le_guid = applicant.LE_Guid;
            }

            string LE_GUID = le_guid.ToString();

            var convertedGuid = new Guid();
            bool isValid = Guid.TryParse(LE_GUID, out convertedGuid);
            if (!isValid)
            {
                return BadRequest("Improperly formatted GUID");
            }

            try
            {
                ArrayList joinderList = generateStaticJoinderPDF(convertedGuid);
                if (joinderList != null)
                {
                    byte[] bytes = (byte[])joinderList[0];
                    string filename = joinderList[1].ToString();

                    System.IO.File.WriteAllBytes(path + filename, bytes);

                    var link = pdfurl + filename;
                    var pdfFile = new BppDocument();

                    pdfFile.Title = "Joinder Agreement";
                    pdfFile.Link = link;

                    files.Add(pdfFile);
                }
                downloads.DownloadLinks = files;
            }
            catch (Exception ex)
            {

            }

            if (downloads != null)
            {
                return Ok(downloads);
            }
            else
            {
                return BadRequest("Documents failed to be created");
            }
        }

        /// <summary> This isn't being used anymore
        /// non API version
        /// </summary>
        /// <param name="npi"></param>
        /// <returns></returns>
        //public ArrayList savejoinder(string npi)
        //{
        //    try
        //    {
        //        var bppEntities = new BPPEntities();
        //        var providers = bppEntities.Providers;
        //        var provider = new Provider();
        //        string _npi = "";
        //        ArrayList list = new ArrayList();
        //        try
        //        {
        //            if (npi != null)
        //            {
        //                _npi = npi;
        //                provider = (from p in providers where p.NPINumber == _npi select p).FirstOrDefault();
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.Message);
        //            return null;
        //        }

        //        BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
        //        bpp.Credentials = CredentialCache.DefaultCredentials;

        //        string _path = ConfigurationManager.AppSettings["SPENV"] + "JoinderAgreement" + DateTime.Now.Ticks.ToString() + _npi + ".pdf";
        //        string _contentType = "document";

        //        var legalEntity = bppEntities.LegalEntities.Where(x => x.TaxID == provider.TaxID).First();

        //        var practice = (from p in bppEntities.Practices where p.LegalEntityID == legalEntity.LegalEntityID select p).FirstOrDefault();
        //        var jHtml = bppEntities.PDFGenerators;
        //        var html = (from j in jHtml where j.ID == 1 select j.HTML).FirstOrDefault();

        //        html = html.Replace("Practice_Name", practice.PracticeName);
        //        html = html.Replace("_Witness", " ");
        //        html = html.Replace("_Provider", " ");
        //        html = html.Replace("Full_Name_With_Suffix", provider.FirstName + " " + provider.LastName);
        //        html = html.Replace("Date_Signed_Joinder", " ");
        //        html = html.Replace("NPI_Number", _npi);
        //        var pSpecialtyMatrix = (from p in bppEntities.ProviderSpecialtyMatrices where p.NPINumber == _npi && p.IsPrmary == true select p).FirstOrDefault();
        //        var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == pSpecialtyMatrix.SpecialtyID).FirstOrDefault();

        //        html = html.Replace("Primary_Specialty", specLookup.Specialty);

        //        var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
        //        string filename = "JoinderAgreement" + DateTime.Now.Ticks.ToString() + _npi + ".pdf";
        //        Attachments0Item ai = new Attachments0Item()
        //        {
        //            TaxID = provider.TaxID,
        //            NPI = _npi,
        //            Path = _path,
        //            ContentType = _contentType,
        //            Name = filename,
        //            Title = filename
        //        };

        //        bpp.AddToAttachments0(ai);
        //        var stream = new MemoryStream(pdfbytes);
        //        bpp.SetSaveStream(ai, stream, false, _contentType, _path);

        //        try
        //        {
        //            bpp.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.Message);
        //            return null;
        //        }
        //        list.Add(pdfbytes);
        //        list.Add(filename);
        //        return list;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.Message);
        //        return null;
        //    }

        //}

        //public ArrayList saveBPPForm(Guid Guid)
        //{
        //    ArrayList list = new ArrayList();
        //    try
        //    {
        //        var bppEntities = new BPPEntities();

        //        var practice = bppEntities.Practices.Where(x => x.PracticeGUID == Guid).First();
        //        var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == practice.LegalEntityID).First();

        //        BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
        //        bpp.Credentials = CredentialCache.DefaultCredentials;

        //        string _path = ConfigurationManager.AppSettings["SPENV"] + "BAAForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";

        //        string _contentType = "document";

        //        var jHtml = bppEntities.PDFGenerators;
        //        var html = (from j in jHtml where j.ID == 2 select j.HTML).FirstOrDefault();
        //        var location = bppEntities.Locations.Where(x => x.PracticeGUID == Guid && x.IsPrimary == true).First();

        //        html = html.Replace("Date_Signed_NPA", " ");
        //        html = html.Replace("Practice_Name", practice.PracticeName);
        //        html = html.Replace("Primary_Address", location.Address1);
        //        html = html.Replace("Primary_City", location.City);
        //        html = html.Replace("Primary_State", location.State);
        //        html = html.Replace("Primary_Zip", location.Zip);
        //        html = html.Replace("Practice_Manager", legalEntity.POCName);
        //        html = html.Replace("Group_Name", practice.PracticeName);
        //        html = html.Replace("_Its", " ");
        //        html = html.Replace("Tax_ID", legalEntity.TaxID);
        //        html = html.Replace("_By", " ");
        //        html = html.Replace("Printed_Name", " ");
        //        html = html.Replace("_Title", " ");

        //        var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
        //        string filename = "BPPForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";
        //        Attachments0Item ai = new Attachments0Item()
        //        {
        //            TaxID = legalEntity.TaxID,
        //            NPI = "",
        //            Path = _path,
        //            ContentType = _contentType,
        //            Name = filename,
        //            Title = filename
        //        };
        //        bpp.AddToAttachments0(ai);

        //        var stream = new MemoryStream(pdfbytes);

        //        bpp.SetSaveStream(ai, stream, false, _contentType, _path);

        //        try
        //        {
        //            bpp.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            //log.Info("api/pdf", ex);
        //            return null;
        //        }
        //        list.Add(pdfbytes);
        //        list.Add(filename);
        //    }
        //    catch (Exception ex)
        //    {
        //        //log.Info("api/pdf", ex);

        //        return null;
        //    }

        //    return list;
        //}

        //public ArrayList saveBAAForm(Guid Guid)
        //{
        //    ArrayList list = new ArrayList();
        //    try
        //    {
        //        var bppEntities = new BPPEntities();

        //        var practice = bppEntities.Practices.Where(x => x.PracticeGUID == Guid).FirstOrDefault();
        //        var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == practice.LegalEntityID).FirstOrDefault();

        //        BPPTeamDataContext bpp = new BPPTeamDataContext(new Uri(url));
        //        bpp.Credentials = CredentialCache.DefaultCredentials;

        //        string _path = ConfigurationManager.AppSettings["SPENV"] + "BAAForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";
        //        string _contentType = "document";

        //        var jHtml = bppEntities.PDFGenerators;
        //        var html = (from j in jHtml where j.ID == 3 select j.HTML).FirstOrDefault();
        //        var location = bppEntities.Locations.Where(x => x.PracticeGUID == Guid).FirstOrDefault(); //&& x.IsPrimary == true

        //        // var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == practice.LegalEntityID).First();

        //        html = html.Replace("Date_Signed_BAA", " ");
        //        html = html.Replace("Practice_Name", practice.PracticeName);
        //        html = html.Replace("Covered_Entity", practice.PracticeName);
        //        html = html.Replace("_Address", location.Address1);
        //        html = html.Replace("_City", location.City);
        //        html = html.Replace("_State", location.State);
        //        html = html.Replace("_Zip", location.Zip);
        //        html = html.Replace("_Attention", legalEntity.POCName);
        //        html = html.Replace("_Facsimile", location.Fax);
        //        html = html.Replace("_Email", legalEntity.POCEmail);
        //        html = html.Replace("Printed_Name", "");
        //        html = html.Replace("_By", "");
        //        html = html.Replace("_Its", "");
        //        html = html.Replace("Printed_Name", "");

        //        var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
        //        string filename = "BAAForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";
        //        Attachments0Item ai = new Attachments0Item()
        //        {
        //            TaxID = legalEntity.TaxID,
        //            NPI = "",
        //            Path = _path,
        //            ContentType = _contentType,
        //            Name = filename,
        //            Title = filename
        //        };

        //        bpp.AddToAttachments0(ai);

        //        var stream = new MemoryStream(pdfbytes);

        //        bpp.SetSaveStream(ai, stream, false, _contentType, _path);
        //        list.Add(pdfbytes);
        //        list.Add(filename);
        //        try
        //        {
        //            bpp.SaveChanges();
        //        }
        //        catch (Exception ex)
        //        {
        //            //log.Info("api/pdf", ex);
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //log.Info("api/pdf", ex);
        //        return null;
        //    }

        //    return list;
        //}

        public ArrayList generateCombinedPDF(Guid Guid)
        {
            ArrayList list = new ArrayList();
            try
            {
                var bppEntities = new BPPEntities();

                //var practice = bppEntities.Practices.Where(x => x.GUID == Guid).FirstOrDefault();
                var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == Guid).FirstOrDefault();

                var jHtml = bppEntities.PDFGenerators;
                var html = (from j in jHtml where j.ID == 8 select j.HTML).FirstOrDefault();
                // var location = bppEntities.Locations.Where(x => x.PracticeGUID == Guid && x.IsPrimary == true).FirstOrDefault(); 
                html = html.Replace("Date_Signed_BAA", " ");
                html = html.Replace("Practice_Name", legalEntity.LE_Name);
                html = html.Replace("Covered_Entity", legalEntity.LE_Name);
                //html = html.Replace("_Address", legalEntity.LE_Address1);
                //html = html.Replace("_City", legalEntity.LE_City);
                //html = html.Replace("_State", legalEntity.LE_State);
                //html = html.Replace("_Zip", legalEntity.LE_Zip);
                //html = html.Replace("_Attention", legalEntity.POCName);
                //html = html.Replace("_Facsimile", legalEntity.POCFax);
                //html = html.Replace("_Email", legalEntity.POCEmail);
                html = html.Replace("Primary_Address", "");
                html = html.Replace("Primary_City", "");
                html = html.Replace("Primary_State", "");
                html = html.Replace("Primary_Zip", "");
                html = html.Replace("_Attention", "");
                html = html.Replace("_Facsimile", "");
                html = html.Replace("_Email", "");
                html = html.Replace("Printed_Name", "");
                html = html.Replace("_By", "");
                html = html.Replace("_Its", "");
                html = html.Replace("Printed_Name", "");

                html = html.Replace("Date_Signed_NPA", " ");
                // html = html.Replace("Practice_Name", legalEntity.LE_Name);
                //html = html.Replace("Primary_Address", legalEntity.LE_Address1);
                //html = html.Replace("Primary_City", legalEntity.LE_City);
                //html = html.Replace("Primary_State", legalEntity.LE_State);
                //html = html.Replace("Primary_Zip", legalEntity.LE_Zip);
                //html = html.Replace("Practice_Manager", legalEntity.POCName);

                html = html.Replace("_Address", "");
                html = html.Replace("_City", "");
                html = html.Replace("_State", "");
                html = html.Replace("_Zip", "");
                html = html.Replace("Practice_Manager", "");

                html = html.Replace("Group_Name", legalEntity.LE_Name);
                html = html.Replace("_Its", " ");
                html = html.Replace("Tax_ID", legalEntity.TaxID);
                //html = html.Replace("_By", " ");
                //html = html.Replace("Printed_Name", " ");
                html = html.Replace("_Title", " ");

                var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
                string filename = "CombinedForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";

                list.Add(pdfbytes);
                list.Add(filename);
            }
            catch (Exception ex)
            {
                return null;
            }

            return list;
        }

        public ArrayList generateBAAPDF(Guid Guid)
        {
            ArrayList list = new ArrayList();
            try
            {
                var bppEntities = new BPPEntities();

                //var practice = bppEntities.Practices.Where(x => x.GUID == Guid).FirstOrDefault();
                var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == Guid).FirstOrDefault();

                var jHtml = bppEntities.PDFGenerators;
                var html = (from j in jHtml where j.ID == 3 select j.HTML).FirstOrDefault();
                // var location = bppEntities.Locations.Where(x => x.PracticeGUID == Guid && x.IsPrimary == true).FirstOrDefault(); 
                html = html.Replace("Date_Signed_BAA", " ");
                html = html.Replace("Practice_Name", legalEntity.LE_Name);
                html = html.Replace("Covered_Entity", legalEntity.LE_Name);
                html = html.Replace("_Address", legalEntity.LE_Address1);
                html = html.Replace("_City", legalEntity.LE_City);
                html = html.Replace("_State", legalEntity.LE_State);
                html = html.Replace("_Zip", legalEntity.LE_Zip);
                html = html.Replace("_Attention", legalEntity.POCName);
                html = html.Replace("_Facsimile", legalEntity.POCFax);
                html = html.Replace("_Email", legalEntity.POCEmail);
                html = html.Replace("Printed_Name", "");
                html = html.Replace("_By", "");
                html = html.Replace("_Its", "");
                html = html.Replace("Printed_Name", "");

                var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
                string filename = "BAAForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";

                list.Add(pdfbytes);
                list.Add(filename);
            }
            catch (Exception ex)
            {
                return null;
            }

            return list;
        }

        public ArrayList generateBPPPDF(Guid Guid)
        {
            ArrayList list = new ArrayList();
            try
            {
                var bppEntities = new BPPEntities();

                // var practice = bppEntities.Practices.Where(x => x.GUID == Guid).FirstOrDefault();
                var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == Guid).FirstOrDefault();

                var jHtml = bppEntities.PDFGenerators;
                var html = (from j in jHtml where j.ID == 2 select j.HTML).FirstOrDefault();
                //var location = bppEntities.Locations.Where(x => x.PracticeGUID == Guid && x.IsPrimary == true).FirstOrDefault(); 


                html = html.Replace("Date_Signed_NPA", " ");
                html = html.Replace("Practice_Name", legalEntity.LE_Name);
                html = html.Replace("Primary_Address", legalEntity.LE_Address1);
                html = html.Replace("Primary_City", legalEntity.LE_City);
                html = html.Replace("Primary_State", legalEntity.LE_State);
                html = html.Replace("Primary_Zip", legalEntity.LE_Zip);
                html = html.Replace("Practice_Manager", legalEntity.POCName);
                html = html.Replace("Group_Name", legalEntity.LE_Name);
                html = html.Replace("_Its", " ");
                html = html.Replace("Tax_ID", legalEntity.TaxID);
                html = html.Replace("_By", " ");
                html = html.Replace("Printed_Name", " ");
                html = html.Replace("_Title", " ");

                var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
                string filename = "BPPForm" + DateTime.Now.Ticks.ToString() + legalEntity.TaxID + ".pdf";

                list.Add(pdfbytes);
                list.Add(filename);
            }
            catch (Exception ex)
            {
                return null;
            }

            return list;
        }
        // Not being used
        //public ArrayList generateJoinderPDF(string npi)
        //{
        //    ArrayList list = new ArrayList();
        //    try
        //    {
        //        var bppEntities = new BPPEntities();

        //        var provider = bppEntities.Providers.Where(x => x.NPINumber == npi).FirstOrDefault();
        //        var ppMatrix = bppEntities.ProviderPracticeMatrices
        //            .Where(x => x.NPINumber == provider.NPINumber && x.EndDate == null)
        //            .FirstOrDefault();
        //        var practice = bppEntities.Practices.Where(x => x.PracticeGUID == ppMatrix.PracticeGUID).FirstOrDefault();
        //        var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == practice.LegalEntityID).FirstOrDefault();

        //        var jHtml = bppEntities.PDFGenerators;
        //        var html = (from j in jHtml where j.ID == 1 select j.HTML).FirstOrDefault();

        //        html = html.Replace("Practice_Name", practice.PracticeName);
        //        html = html.Replace("_Witness", " ");
        //        html = html.Replace("_Provider", " ");
        //        html = html.Replace("Full_Name_With_Suffix", provider.FirstName + " " + provider.LastName);
        //        html = html.Replace("Date_Signed_Joinder", " ");
        //        html = html.Replace("NPI_Number", npi);
        //        var pSpecialtyMatrix = (from p in bppEntities.ProviderSpecialtyMatrices where p.NPINumber == npi && p.IsPrmary == true select p).FirstOrDefault();
        //        var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == pSpecialtyMatrix.SpecialtyID).FirstOrDefault();

        //        html = html.Replace("Primary_Specialty", specLookup.Specialty);

        //        var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
        //        string filename = "JoinderAgreement" + DateTime.Now.Ticks.ToString() + npi + ".pdf";

        //        list.Add(pdfbytes);
        //        list.Add(filename);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //    return list;
        //}

        public ArrayList generateStaticJoinderPDF(Guid Guid)
        {
            ArrayList list = new ArrayList();
            try
            {
                var bppEntities = new BPPEntities();

                var jHtml = bppEntities.PDFGenerators;
                var html = (from j in jHtml where j.ID == 1 select j.HTML).FirstOrDefault();

                html = html.Replace("Practice_Name", " ");
                html = html.Replace("_Witness", " ");
                html = html.Replace("_Provider", " ");
                html = html.Replace("Full_Name_With_Suffix", " ");
                html = html.Replace("Date_Signed_Joinder", " ");
                html = html.Replace("NPI_Number", " ");

                html = html.Replace("Primary_Specialty", " ");

                var pdfbytes = Helpers.PFDGenerator.generatePDF(html);
                string filename = "JoinderAgreement" + DateTime.Now.Ticks.ToString() + Guid.ToString() + ".pdf";

                list.Add(pdfbytes);
                list.Add(filename);
            }
            catch (Exception ex)
            {
                return null;
            }

            return list;
        }
    }
}
