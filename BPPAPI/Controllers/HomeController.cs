﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BPPAPI.Controllers
{
    [RoutePrefix("api/home")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "BPP API";

            return View();
        }
    }
}
