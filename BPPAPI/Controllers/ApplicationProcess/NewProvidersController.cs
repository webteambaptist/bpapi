﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web.Http;

namespace BPPAPI.Controllers.ApplicationProcess
{
    [RoutePrefix("api/providers")]
    public class NewProvidersController : ApiController
    {
        TextInfo caseFormat = new CultureInfo("en-US", false).TextInfo;

        [HttpPost]
        [Route("submitProviders")]
        public IHttpActionResult SubmitProviders([FromBody] PracticeAppData practiceAppData)
        {
            var headers = Request.Headers;
            var ActivationKey = "";
            if (headers.Contains("ActivationKey"))
            {
                ActivationKey = headers.GetValues("ActivationKey").First();
            }

            var bppEntities = new BPPEntities();
            var providers = bppEntities.Providers;
            var practices = bppEntities.Practices;
            var locations = bppEntities.Locations;
            int matrixID = 0;
            var emailData = new Dictionary<string, Guid>();
            var providercount = 0;

            //ProviderPracticeMatrix providerprofileexists = new ProviderPracticeMatrix();

            foreach (ProviderFull pd in practiceAppData._Providers)
            {
                int locationID = int.Parse(pd.PrimaryPracticeLocation);
                var primaryLocation = locations.Where(l => l.ID == locationID).FirstOrDefault();
                var practiceGuid = primaryLocation.PracticeGUID;

                string AKA = string.Empty;

                if (pd.ProviderAncillary != null)
                {
                    AKA = pd.ProviderAncillary.AKA;
                }

                Provider p = new Provider();

                bool provExists = false;
                // Check if Provider Exists
                var prov = providers.Where(x => x.NPINumber == pd.NPINumber).FirstOrDefault();
                if (prov != null)
                {
                    provExists = true;
                    //try
                    //{
                    //    SendProviderEmail(prov.NPINumber, practiceGuid, locationID, AKA);
                    //}
                    //catch (Exception e)
                    //{
                    //    return BadRequest(e.InnerException.Message);
                    //}
                }

                // Insert provder into providers table
                if (!provExists)
                {
                    int degree = Convert.ToInt32(pd.Degree);
                    var degreeID = bppEntities.DegreeTypes.Where(x => x.DegreeID == degree).FirstOrDefault();

                    //p.TaxID = pd.TaxID;
                    p.FirstName = caseFormat.ToTitleCase(pd.FirstName.ToLower());
                    p.MiddleName = caseFormat.ToTitleCase(pd.MiddleName.ToLower());
                    p.LastName = caseFormat.ToTitleCase(pd.LastName.ToLower());
                    p.Suffix = pd.Suffix;
                    p.DegreeID = degreeID.DegreeID;
                    p.DateOfBirth = pd.DateOfBirth;
                    p.PhoneNumber = pd.PhoneNumber;
                    p.Email = pd.Email;
                    p.NPINumber = pd.NPINumber;
                    p.CreatedDT = DateTime.Now;
                    p.ModifiedDT = DateTime.Now;
                    p.isBHPrivileged = pd.isBHPrivileged;
                    p.CMS = pd.CMS;
                    //p.Suffix = pd.Suffix;
                    p.GUID = Guid.NewGuid();
                    bppEntities.Providers.Add(p);

                    Audit.ProvidersAudit(p, "application");
                    //ProvidersAudit pa = new ProvidersAudit();
                    //pa.TaxID = p.TaxID;
                    //pa.FirstName = p.FirstName;
                    //pa.MiddleName = p.MiddleName;
                    //pa.LastName = p.LastName;
                    //pa.Suffix = p.Suffix;
                    //pa.DegreeID = degreeID.DegreeID;
                    //pa.DateOfBirth = p.DateOfBirth;
                    //pa.PhoneNumber = p.PhoneNumber;
                    //pa.Email = p.Email;
                    //pa.NPINumber = p.NPINumber;
                    //pa.CreatedDT = DateTime.Now;
                    //pa.ModifiedDT = DateTime.Now;
                    //pa.isBHPrivileged = p.isBHPrivileged;
                    //pa.CMS = p.CMS;
                    //pa.Suffix = p.Suffix;
                    //pa.GUID = p.GUID;
                    //pa.ModifiedUser = "application";
                    //bppEntities.ProvidersAudits.Add(pa);

                    try
                    {
                        bppEntities.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.InnerException.Message);
                    }

                    //if (pd.ProviderAncillary != null)
                    //{
                    //    ProviderAncillary ancillary = new ProviderAncillary();
                    //    ancillary.AKA = pd.ProviderAncillary.AKA;
                    //    ancillary.EmploymentStatus = pd.ProviderAncillary.EmploymentStatus;
                    //    ancillary.Maiden_Name = pd.ProviderAncillary.Maiden_Name;
                    //    ancillary.NPINumber = pd.NPINumber;
                    //    ancillary.PracticeGuid = practiceGuid;
                    //    ancillary.ServicePopulation = pd.ProviderAncillary.ServicePopulation;
                    //    ancillary.SpecialtyType = pd.ProviderAncillary.SpecialtyType;
                    //    ancillary.CreatedDT = DateTime.Now;
                    //    ancillary.ProviderPracticeMatrixID = matrixID;
                    //    bppEntities.ProviderAncillaries.Add(ancillary);
                    //}

                    //try
                    //{
                    //    SendProviderEmail(p.NPINumber, practiceGuid, locationID, AKA);
                    //}
                    //catch (Exception e)
                    //{
                    //    return BadRequest(e.InnerException.Message);
                    //}
                }

                try
                {
                    // are their any that are active (null finish date) for this practice
                    var matrix = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == pd.NPINumber && x.PracticeGUID == practiceGuid && x.EndDate == null).FirstOrDefault();
                    //providerprofileexists = matrix;

                    //// if there are any active don't add
                    if (matrix == null)
                    {
                        ProviderPracticeMatrix x = new ProviderPracticeMatrix();
                        x.NPINumber = pd.NPINumber;
                        x.PracticeGUID = practiceGuid;
                        bppEntities.ProviderPracticeMatrices.Add(x);

                        Audit.ProviderPracticeMatrixAudit(x, "application");
                        //ProviderPracticeMatrixAudit ppma = new ProviderPracticeMatrixAudit();
                        //ppma.NPINumber = x.NPINumber;
                        //ppma.PracticeGUID = x.PracticeGUID;
                        //ppma.ModifiedDt = DateTime.Now;
                        //ppma.ModifiedUser = "application";
                        //bppEntities.ProviderPracticeMatrixAudits.Add(ppma);

                        bppEntities.SaveChanges();
                        matrixID = x.ID;

                        providercount++;
                    }
                    // var matrix = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == pd.NPINumber).FirstOrDefault();
                    //bppEntities.insProvPracMatrix(pd.NPINumber, practiceGuid);
                    //bppEntities.SaveChanges();

                }
                catch (Exception e)
                {
                    BadRequest(e.InnerException.Message);
                }

                if (pd.ProviderAncillary != null)
                {
                    var providerancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == pd.NPINumber && x.PracticeGuid == practiceGuid).FirstOrDefault();
                    if (providerancillary == null)
                    {
                        ProviderAncillary ancillary = new ProviderAncillary();
                        ancillary.AKA = caseFormat.ToTitleCase(pd.ProviderAncillary.AKA.ToLower());
                        ancillary.EmploymentStatus = pd.ProviderAncillary.EmploymentStatus;
                        ancillary.Maiden_Name = pd.ProviderAncillary.Maiden_Name;
                        ancillary.NPINumber = pd.NPINumber;
                        ancillary.PracticeGuid = practiceGuid;
                        ancillary.ServicePopulation = pd.ProviderAncillary.ServicePopulation;
                        ancillary.SpecialtyType = pd.ProviderAncillary.SpecialtyType;
                        ancillary.CreatedDT = DateTime.Now;
                        ancillary.ProviderPracticeMatrixID = matrixID;
                        bppEntities.ProviderAncillaries.Add(ancillary);

                        Audit.ProviderAncillaryAudit(ancillary, "appliaction");
                        //ProviderAncillaryAudit paa = new ProviderAncillaryAudit();
                        //paa.AKA = ancillary.AKA;
                        //paa.EmploymentStatus = ancillary.EmploymentStatus;
                        //paa.Maiden_Name = ancillary.Maiden_Name;
                        //paa.NPINumber = ancillary.NPINumber;
                        //paa.PracticeGuid = ancillary.PracticeGuid;
                        //paa.ServicePopulation = ancillary.ServicePopulation;
                        //paa.CreatedDT = DateTime.Now;
                        //paa.ProviderPracticeMatrixID = ancillary.ProviderPracticeMatrixID;
                        //paa.ModifiedDt = DateTime.Now;
                        //paa.ModifiedUser = "application";
                        //bppEntities.ProviderAncillaryAudits.Add(paa);
                    }
                }

                // Update or insert primary specialty into ProviderSpecialtyMatrix table
                var provSpecialtyMatrix = new ProviderSpecialtyMatrix();

                // check to see if the primary specialty already exists for user
                var primary = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == pd.NPINumber && e.PracticeGUID == practiceGuid && e.IsPrmary == true && e.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                if (primary != null)
                {
                    try
                    {
                        var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == primary.SpecialtyID).FirstOrDefault();

                        // if user already has specialties and primary is different (update)
                        if (specLookup != null && specLookup.ID.ToString() != pd.PrimarySpecialty)
                        {
                            primary.SpecialtyID = (int)specLookup.SpecialtyID;

                            bppEntities.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.InnerException.Message);
                    }
                }
                else // otherwise add
                {
                    try
                    {
                        var providerspecialty = bppEntities.ProviderSpecialtyMatrices.Where(x => x.NPINumber == pd.NPINumber && x.PracticeGUID == practiceGuid).FirstOrDefault();
                        if (providerspecialty == null)
                        {
                            int specIndex = int.Parse(pd.PrimarySpecialty);
                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.ID == specIndex).FirstOrDefault();

                            provSpecialtyMatrix = new ProviderSpecialtyMatrix();
                            provSpecialtyMatrix.IsPrmary = true;
                            provSpecialtyMatrix.NPINumber = pd.NPINumber;
                            provSpecialtyMatrix.SpecialtyID = (int)specLookup.SpecialtyID;
                            provSpecialtyMatrix.PracticeGUID = practiceGuid;
                            provSpecialtyMatrix.ProviderPracticeMatrixID = matrixID;
                            bppEntities.ProviderSpecialtyMatrices.Add(provSpecialtyMatrix);

                            Audit.ProviderSpecialtyMatrixAudit(provSpecialtyMatrix, "application");
                            //ProviderSpecialtyMatrixAudit psma_true = new ProviderSpecialtyMatrixAudit();
                            //psma_true.IsPrmary = provSpecialtyMatrix.IsPrmary;
                            //psma_true.NPINumber = provSpecialtyMatrix.NPINumber;
                            //psma_true.SpecialtyID = provSpecialtyMatrix.SpecialtyID;
                            //psma_true.PracticeGUID = provSpecialtyMatrix.PracticeGUID;
                            //psma_true.ProviderPracticeMatrixID = provSpecialtyMatrix.ProviderPracticeMatrixID;
                            //psma_true.ModifiedDt = DateTime.Now;
                            //psma_true.ModifiedUser = "application";
                            //bppEntities.ProviderSpecialtyMatrixAudits.Add(psma_true);

                            bppEntities.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.InnerException.Message);
                    }

                }

                // Update or insert secondary specialty into ProviderSpecialtyMatrix table
                if (!string.IsNullOrEmpty(pd.SecondarySpecialty))
                {
                    var secondary = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == pd.NPINumber && e.PracticeGUID == practiceGuid && e.IsPrmary == false && e.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                    if (secondary != null)
                    {
                        try
                        {
                            var secSpecLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == secondary.SpecialtyID).FirstOrDefault();
                            if (secSpecLookup != null && secSpecLookup.Specialty != pd.SecondarySpecialty)
                            {
                                secondary.SpecialtyID = (int)secSpecLookup.SpecialtyID;
                                bppEntities.SaveChanges();
                            }
                        }
                        catch (Exception e)
                        {
                            return BadRequest(e.InnerException.Message);
                        }
                    }
                    else
                    {
                        try
                        {
                            int specIndex = int.Parse(pd.SecondarySpecialty);
                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.ID == specIndex).FirstOrDefault();
                            if (specLookup != null)
                            {
                                provSpecialtyMatrix = new ProviderSpecialtyMatrix();
                                provSpecialtyMatrix.IsPrmary = false;
                                provSpecialtyMatrix.NPINumber = pd.NPINumber;
                                provSpecialtyMatrix.SpecialtyID = (int)specLookup.SpecialtyID;
                                provSpecialtyMatrix.PracticeGUID = practiceGuid;
                                provSpecialtyMatrix.ProviderPracticeMatrixID = matrixID;
                                bppEntities.ProviderSpecialtyMatrices.Add(provSpecialtyMatrix);

                                Audit.ProviderSpecialtyMatrixAudit(provSpecialtyMatrix, "application");
                                //ProviderSpecialtyMatrixAudit psma_false = new ProviderSpecialtyMatrixAudit();
                                //psma_false.IsPrmary = provSpecialtyMatrix.IsPrmary;
                                //psma_false.NPINumber = provSpecialtyMatrix.NPINumber;
                                //psma_false.SpecialtyID = provSpecialtyMatrix.SpecialtyID;
                                //psma_false.PracticeGUID = provSpecialtyMatrix.PracticeGUID;
                                //psma_false.ProviderPracticeMatrixID = provSpecialtyMatrix.ProviderPracticeMatrixID;
                                //psma_false.ModifiedDt = DateTime.Now;
                                //psma_false.ModifiedUser = "application";
                                //bppEntities.ProviderSpecialtyMatrixAudits.Add(psma_false);

                                bppEntities.SaveChanges();
                            }
                        }
                        catch (Exception e)
                        {
                            return BadRequest(e.InnerException.Message);
                        }
                    }
                }

                // Update or insert into ProviderPracticeWorkflow table
                var provPracWF = new ProviderPracticeWorkflow();
                try
                {
                    //var primaryProvLocation = bppEntities.ProviderLocations.Where(l => l.ID == primaryLocation.ID && l.NPINumber == pd.NPINumber && l.PracticeGUID == practiceGuid && l.ProviderPracticeMatrixID == matrixID).FirstOrDefault();

                    var providerpractiveworkflow = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == pd.NPINumber && x.PracticeGUID == practiceGuid).FirstOrDefault();
                    if (providerpractiveworkflow == null)
                    {
                        provPracWF.NPINumber = pd.NPINumber;
                        provPracWF.PracticeGUID = practiceGuid;
                        // provPracWF.ProviderApprovalStatus = 1;
                        provPracWF.PrimaryPracticeLocation = primaryLocation.ID.ToString();
                        provPracWF.ProviderPracticeMatrixID = matrixID;
                        provPracWF.DateSignedMemberApp = DateTime.Now;
                        provPracWF.ProviderStatusID = bppEntities.ProviderMemberStatus.Where(x => x.MemberStatus == "Applicant").Select(x => x.StatusID).FirstOrDefault();
                        bppEntities.ProviderPracticeWorkflows.Add(provPracWF);
                        //bppEntities.insertProviderPracticeWF(provPracWF.NPINumber, provPracWF.PracticeGUID,
                        //    provPracWF.ProviderApprovalStatus, provPracWF.PrimaryPracticeLocation);

                        Audit.ProviderPracticeWorkflowAudit(provPracWF, "application");
                        //ProviderPracticeWorkflowAudit ppwa = new ProviderPracticeWorkflowAudit();
                        //ppwa.NPINumber = provPracWF.NPINumber;
                        //ppwa.PracticeGUID = provPracWF.PracticeGUID;
                        //ppwa.PrimaryPracticeLocation = provPracWF.PrimaryPracticeLocation;
                        //ppwa.ProviderPracticeMatrixID = provPracWF.ProviderPracticeMatrixID;
                        //ppwa.DateSignedMemberApp = DateTime.Now;
                        //ppwa.ProviderStatusID = provPracWF.ProviderStatusID;
                        //ppwa.ModifiedDt = DateTime.Now;
                        //ppwa.ModifiedUser = "application";
                        //bppEntities.ProviderPracticeWorkflowAudits.Add(ppwa);

                        bppEntities.SaveChanges();

                        // set the DateSignedMemberApp
                        //var wf = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == pd.NPINumber && x.PracticeGUID == practiceGuid).FirstOrDefault();
                        // wf.DateSignedMemberApp = DateTime.Now;
                        //wf.ProviderStatusID = bppEntities.ProviderMemberStatus.Where(x => x.MemberStatus == "Applicant").Select(x => x.StatusID).FirstOrDefault();
                        // bppEntities.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.InnerException.Message);
                }

                // Update or insert into ProviderMemberStatusMatrix table
                //try
                //{
                //    var memberStatusMatrix = bppEntities.ProviderMemberStatusMatrices.Where(l => l.PracticeGUID == practiceGuid && l.NPINumber == pd.NPINumber).FirstOrDefault();
                //    if (memberStatusMatrix != null)
                //    {
                //        memberStatusMatrix.ProviderMemberStatus = 0;
                //    }
                //    else
                //    {
                //        var msm = new ProviderMemberStatusMatrix();
                //        msm.PracticeGUID = practiceGuid;
                //        msm.NPINumber = pd.NPINumber;
                //        msm.ProviderMemberStatus = 0;
                //        bppEntities.ProviderMemberStatusMatrices.Add(msm);
                //    }
                //    bppEntities.SaveChanges();
                //}
                //catch (Exception e)
                //{
                //    return BadRequest(e.InnerException.Message);
                //}



                // Update or insert primary Provider Location
                try
                {
                    var primaryProvLocation = bppEntities.ProviderLocations.Where(l => l.ID == primaryLocation.ID && l.NPINumber == pd.NPINumber && l.PracticeGUID == practiceGuid && l.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                    if (primaryProvLocation != null)
                    {
                        primaryProvLocation.IsPrimary = true;
                    }
                    else
                    {
                        var providerlocation = bppEntities.ProviderLocations.Where(x => x.NPINumber == pd.NPINumber && x.PracticeGUID == practiceGuid).FirstOrDefault();
                        if (providerlocation == null)
                        {
                            var plp = new ProviderLocation();
                            plp.LocationID = primaryLocation.ID;
                            plp.NPINumber = pd.NPINumber;
                            plp.IsPrimary = true;
                            plp.PracticeGUID = practiceGuid;
                            plp.ProviderPracticeMatrixID = matrixID;
                            bppEntities.ProviderLocations.Add(plp);

                            Audit.ProviderLocationsAudit(plp, "application");
                            //ProviderLocationsAudit pla_true = new ProviderLocationsAudit();
                            //pla_true.LocationID = plp.LocationID;
                            //pla_true.NPINumber = plp.NPINumber;
                            //pla_true.IsPrimary = plp.IsPrimary;
                            //pla_true.PracticeGUID = plp.PracticeGUID;
                            //pla_true.ProviderPracticeMatrixID = plp.ProviderPracticeMatrixID;
                            //pla_true.ModifedDt = DateTime.Now;
                            //pla_true.ModifiedUser = "application";
                            //bppEntities.ProviderLocationsAudits.Add(pla_true);
                        }

                    }
                    bppEntities.SaveChanges();
                }
                catch (Exception ex)
                {
                    BadRequest(ex.InnerException.Message);

                }

                try
                {
                    // Update or insert other Provider Location
                    if (pd.OtherPracticeLocations.Count > 0)
                    {
                        foreach (var other in pd.OtherPracticeLocations)
                        {
                            try
                            {
                                var otherLoc = bppEntities.ProviderLocations.Where(x => x.LocationID == other).Where(x => x.NPINumber == pd.NPINumber && x.PracticeGUID == practiceGuid && x.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                                if (otherLoc != null)
                                {
                                    otherLoc.IsPrimary = false;
                                }
                                else
                                {
                                    var plp = new ProviderLocation();
                                    plp.LocationID = other;
                                    plp.NPINumber = pd.NPINumber;
                                    plp.IsPrimary = false;
                                    plp.PracticeGUID = practiceGuid;
                                    plp.ProviderPracticeMatrixID = matrixID;
                                    bppEntities.ProviderLocations.Add(plp);

                                    Audit.ProviderLocationsAudit(plp, "application");
                                    //ProviderLocationsAudit pla_false = new ProviderLocationsAudit();
                                    //pla_false.LocationID = plp.LocationID;
                                    //pla_false.NPINumber = plp.NPINumber;
                                    //pla_false.IsPrimary = plp.IsPrimary;
                                    //pla_false.PracticeGUID = plp.PracticeGUID;
                                    //pla_false.ProviderPracticeMatrixID = plp.ProviderPracticeMatrixID;
                                    //pla_false.ModifedDt = DateTime.Now;
                                    //pla_false.ModifiedUser = "application";
                                    //bppEntities.ProviderLocationsAudits.Add(pla_false);
                                }
                                bppEntities.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                BadRequest(ex.InnerException.Message);

                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    bppEntities.SaveChanges();
                }

                emailData.Add(pd.NPINumber, practiceGuid);
            }

            if (providercount > 0)
            {
                ////Process email for all providers
                SendProviderEmail(emailData);
            }
            else
            {
                SendDuplicateProviderAttemptEmail(emailData);
                SendDuplicateProviderAttemptToApplicant(emailData, ActivationKey);
            }

            return Ok("Success");
        }

        /// <summary>
        /// Generates an email for all providers passed in the dictionary. 
        /// The dictionary object contains an NPINumber and a PracticeGUID
        /// </summary>
        /// <param name="emailData"></param>
        private void SendProviderEmail(Dictionary<string, Guid> emailData)
        {
            var bppEntities = new BPPEntities();
            var providers = bppEntities.Providers;
            var practices = bppEntities.Practices;
            var legalEntities = bppEntities.LegalEntities;
            var providerLocations = bppEntities.ProviderLocations;
            var locations = bppEntities.Locations;
            var providerPracticeMatrices = bppEntities.ProviderPracticeMatrices;
            var providerAncillary = bppEntities.ProviderAncillaries;

            string htmlTableStart = "<table width=600 style=\"border-collapse:collapse; text-align:center;\" >";
            string htmlTableEnd = "</table>";
            string htmlHeaderRowStart = "<tr style =\"background-color:#000000; color:#ffffff;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlHeaderRowEnd = "</font></tr>";
            string htmlTrStart = "<tr style =\"color:#000000;\">";
            string htmlTrEnd = "</tr>";
            string htmlTdCol1Start = "<td width=150  style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlTdEnd = "</font></td>";
            string htmlTdCol2Start = "<td width=450 align=\"left\" style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string message = string.Empty;

            var providerCount = 0;

            foreach (KeyValuePair<string, Guid> entry in emailData)
            {
                bool isExistingProvider = false;
                bool isInactiveProvider = false;
                bool isNewProvider = false;
                bool isDuplicate = false;

                var newPractice = new Practice();
                var oldPractice = new Practice();

                var newProvPractMatrix = providerPracticeMatrices.Where(x => x.NPINumber == entry.Key && x.PracticeGUID == entry.Value && x.StartDate == null && x.EndDate == null).FirstOrDefault();
                var oldProvPractMatrix = new ProviderPracticeMatrix();

                var orderByDescendingResult = new List<ProviderPracticeMatrix>();

                if (newProvPractMatrix == null)
                {
                    return; //no new provider practice matrix record; return and process next disctionary entry
                }

                var checkMatrix = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == entry.Key && x.StartDate != null).ToList();
                if (checkMatrix.Count > 0)
                {
                    orderByDescendingResult = checkMatrix.OrderByDescending(s => s.StartDate).ToList();
                    oldProvPractMatrix = orderByDescendingResult.FirstOrDefault();
                    if (oldProvPractMatrix.EndDate != null)
                    {
                        isInactiveProvider = true;
                    }
                    else
                    {
                        isExistingProvider = true;
                    }
                }
                else
                {
                    isNewProvider = true;

                    if (checkMatrix.Count == 0)
                    {
                        var providerstillapplicant = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == entry.Key).FirstOrDefault();
                        oldProvPractMatrix = providerstillapplicant;
                    }
                }

                var getMatrixIDs = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == entry.Key).Select(x => x.ID).ToList();
                if (getMatrixIDs.Count > 1)
                {
                    foreach (var _matrixID in getMatrixIDs)
                    {
                        int? matrixId = _matrixID;

                        var ppm = bppEntities.ProviderPracticeWorkflows.Where(x => x.ProviderPracticeMatrixID == matrixId).FirstOrDefault();

                        if (ppm.ProviderStatusID == 1 || ppm.ProviderStatusID == 0)
                        {
                            var setDuplicate = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == entry.Key && x.PracticeGUID == entry.Value).FirstOrDefault();
                            int? _updateMatrixID = setDuplicate.ID;

                            var ppm_update = bppEntities.ProviderPracticeWorkflows.Where(x => x.ProviderPracticeMatrixID == _updateMatrixID).FirstOrDefault();
                            var duplicateProviderStatus = bppEntities.ProviderMemberStatus.Where(x => x.MemberStatus == "Duplicate Applicant").Select(x => x.StatusID).FirstOrDefault();
                            ppm_update.ProviderStatusID = duplicateProviderStatus;
                            bppEntities.SaveChanges();

                            isDuplicate = true;
                            isNewProvider = false;
                            isExistingProvider = false;
                            isInactiveProvider = false;
                            break;
                        }
                    }
                
                }

                //logic for building the email content

                var provider = providers.Where(p => p.NPINumber == entry.Key).FirstOrDefault();
                var ancillary = providerAncillary.Where(a => a.ProviderPracticeMatrixID == newProvPractMatrix.ID).FirstOrDefault();
                var degree = bppEntities.DegreeTypes.Where(x => x.DegreeID == provider.DegreeID).FirstOrDefault();


                //The info provided by the provider in the web form

                #region Provider Details

                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Provider Details</td>" + htmlHeaderRowEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + provider.FirstName + " " + provider.MiddleName + " " + provider.LastName + " " + provider.Suffix + htmlTdEnd + htmlTrEnd;
                if (!string.IsNullOrEmpty(ancillary.AKA))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Also Known As" + htmlTdEnd + htmlTdCol2Start + ancillary.AKA + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTrStart + htmlTdCol1Start + "Degree" + htmlTdEnd + htmlTdCol2Start + degree.DegreeName + htmlTdEnd + htmlTrEnd;
                if (!String.IsNullOrEmpty(provider.PhoneNumber))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Phone Number" + htmlTdEnd + htmlTdCol2Start + provider.PhoneNumber + htmlTdEnd + htmlTrEnd;
                }

                message = message + htmlTrStart + htmlTdCol1Start + "NPI Number" + htmlTdEnd + htmlTdCol2Start + provider.NPINumber + htmlTdEnd + htmlTrEnd;

                if (provider.isBHPrivileged != null)
                {
                    if ((bool)provider.isBHPrivileged)
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Is Baptist Hospital Privileged" + htmlTdEnd + htmlTdCol2Start + "Yes" + htmlTdEnd + htmlTrEnd;
                    }
                    else
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Is Baptist Hospital Privileged" + htmlTdEnd + htmlTdCol2Start + "No" + htmlTdEnd + htmlTrEnd;
                    }
                }
                if (isNewProvider)
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Applicant Status" + htmlTdEnd + htmlTdCol2Start + "New Applicant" + htmlTdEnd + htmlTrEnd;
                }
                else if (isExistingProvider)
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Applicant Status" + htmlTdEnd + htmlTdCol2Start + "Existing Provider" + htmlTdEnd + htmlTrEnd;
                }
                else if (isInactiveProvider)
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Applicant Status" + htmlTdEnd + htmlTdCol2Start + "Inactive Provider" + htmlTdEnd + htmlTrEnd;
                }
                else if (isDuplicate)
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Applicant Status" + htmlTdEnd + htmlTdCol2Start + "Duplicate Applicant" + htmlTdEnd + htmlTrEnd;
                }

                message = message + htmlTableEnd + "<BR><BR>";
                #endregion

                #region Legal Entity Info
                newPractice = practices.Where(p => p.PracticeGUID == newProvPractMatrix.PracticeGUID).FirstOrDefault();

                var newLegalEntity = legalEntities.Where(l => l.LegalEntityID == newPractice.LegalEntityID).FirstOrDefault();
                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">New Legal Entity Details</td>" + htmlHeaderRowEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.LE_Name + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Address" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.LE_Address1 + " " + newLegalEntity.LE_Address2 + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + " " + htmlTdEnd + htmlTdCol2Start + newLegalEntity.LE_City + ", " + newLegalEntity.LE_State + " " + newLegalEntity.LE_Zip + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.POCName + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact Phone" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.POCPhoneNumber + htmlTdEnd + htmlTrEnd;
                if (!string.IsNullOrEmpty(ancillary.AKA))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact Ext" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.POCExtension + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact Email" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.POCEmail + htmlTdEnd + htmlTrEnd;
                message = message + htmlTableEnd + "<BR><BR>";

                if (isExistingProvider || isInactiveProvider || isDuplicate)
                {
                    oldPractice = practices.Where(p => p.PracticeGUID == oldProvPractMatrix.PracticeGUID).FirstOrDefault();

                    var oldLegalEntity = legalEntities.Where(l => l.LegalEntityID == oldPractice.LegalEntityID).FirstOrDefault();
                    message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Existing Legal Entity Details</td>" + htmlHeaderRowEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.LE_Name + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Address" + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.LE_Address1 + " " + oldLegalEntity.LE_Address2 + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + " " + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.LE_City + ", " + oldLegalEntity.LE_State + " " + oldLegalEntity.LE_Zip + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact" + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.POCName + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact Phone" + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.POCPhoneNumber + htmlTdEnd + htmlTrEnd;
                    if (!string.IsNullOrEmpty(ancillary.AKA))
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact Ext" + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.POCExtension + htmlTdEnd + htmlTrEnd;
                    }
                    message = message + htmlTrStart + htmlTdCol1Start + "Point of Contact Email" + htmlTdEnd + htmlTdCol2Start + oldLegalEntity.POCEmail + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTableEnd + "<BR><BR>";
                }

                #endregion

                #region Practice Details
                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">New Practice Details</td>" + htmlHeaderRowEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + newPractice.PracticeName + htmlTdEnd + htmlTrEnd;
                message = message + htmlTableEnd + "<BR><BR>";

                if (isExistingProvider || isInactiveProvider || isDuplicate)
                {
                    message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Existing Practice Details</td>" + htmlHeaderRowEnd;
                    message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + oldPractice.PracticeName + htmlTdEnd + htmlTrEnd;
                    message = message + htmlTableEnd + "<BR><BR>";
                }
                #endregion

                #region Location Details
                var newProviderLocation = providerLocations.Where(l => l.ProviderPracticeMatrixID == newProvPractMatrix.ID).FirstOrDefault();
                var newPrimaryLocation = locations.Where(l => l.ID == newProviderLocation.LocationID).FirstOrDefault();
                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">New Primary Location</td>" + htmlHeaderRowEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + newPrimaryLocation.LocationName + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Address" + htmlTdEnd + htmlTdCol2Start + newPrimaryLocation.Address1 + " " + newPrimaryLocation.Address2 + htmlTdEnd + htmlTrEnd;
                message = message + htmlTrStart + htmlTdCol1Start + " " + htmlTdEnd + htmlTdCol2Start + newPrimaryLocation.City + ", " + newPrimaryLocation.State + " " + newPrimaryLocation.Zip + htmlTdEnd + htmlTrEnd;
                if (!string.IsNullOrEmpty(newPrimaryLocation.PhoneNumber))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Phone" + htmlTdEnd + htmlTdCol2Start + newPrimaryLocation.PhoneNumber + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTableEnd + "<BR><BR>";


                if (isExistingProvider || isInactiveProvider || isDuplicate)
                {
                    var oldProviderLocation = providerLocations.Where(pl => pl.ProviderPracticeMatrixID == oldProvPractMatrix.ID).FirstOrDefault();
                    if (oldProviderLocation != null)
                    {
                        var oldPrimaryLocation = locations.Where(l => l.ID == oldProviderLocation.LocationID).FirstOrDefault();

                        message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Existing Primary Location</td>" + htmlHeaderRowEnd;
                        message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + oldPrimaryLocation.LocationName + htmlTdEnd + htmlTrEnd;
                        message = message + htmlTrStart + htmlTdCol1Start + "Address" + htmlTdEnd + htmlTdCol2Start + oldPrimaryLocation.Address1 + " " + oldPrimaryLocation.Address2 + htmlTdEnd + htmlTrEnd;
                        message = message + htmlTrStart + htmlTdCol1Start + " " + htmlTdEnd + htmlTdCol2Start + oldPrimaryLocation.City + ", " + oldPrimaryLocation.State + " " + oldPrimaryLocation.Zip + htmlTdEnd + htmlTrEnd;
                        if (!string.IsNullOrEmpty(oldPrimaryLocation.PhoneNumber))
                        {
                            message = message + htmlTrStart + htmlTdCol1Start + "Phone" + htmlTdEnd + htmlTdCol2Start + oldPrimaryLocation.PhoneNumber + htmlTdEnd + htmlTrEnd;
                        }
                        message = message + htmlTableEnd + "<BR><BR>";
                    }
                }
                #endregion

                message = message + "<hr><BR><BR>";
                providerCount++;
            }

            if (providerCount > 0)
            {
                var intro = string.Empty;
                var subject = "Submitted Providers";

                if (providerCount == 1)
                {
                    intro = "<font face=\"Arial\">The following provider has been added: </font><br><br>";
                }
                else
                {
                    intro = "<font face=\"Arial\">The following providers (" + providerCount.ToString() + ") have been added: </font><br><br>";
                }

                var hostname = ConfigurationManager.AppSettings["ALC"];
                //Send New Member Request Information Email
                Message m = new Message();
                m.Sender = ConfigurationManager.AppSettings["sender"].ToString();
                m.Recipient = ConfigurationManager.AppSettings["recipient"].ToString();
                m.Subject = subject;
                m.Body = intro + message;

                new MessageController().sendMessage(m);

            }
        }

        /// <summary>
        /// Generates an email for providers that are attempting to submit a duplicate application passed in the dictionary. 
        /// The dictionary object contains an NPINumber and a PracticeGUID
        /// </summary>
        /// <param name="emailData"></param>
        private void SendDuplicateProviderAttemptEmail(Dictionary<string, Guid> emailData)
        {
            var bppEntities = new BPPEntities();
            var providers = bppEntities.Providers;
            var practices = bppEntities.Practices;
            var legalEntities = bppEntities.LegalEntities;
            var providerLocations = bppEntities.ProviderLocations;
            var locations = bppEntities.Locations;
            var providerPracticeMatrices = bppEntities.ProviderPracticeMatrices;
            var providerAncillary = bppEntities.ProviderAncillaries;

            string htmlTableStart = "<table width=600 style=\"border-collapse:collapse; text-align:center;\" >";
            string htmlTableEnd = "</table>";
            string htmlHeaderRowStart = "<tr style =\"background-color:#000000; color:#ffffff;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlHeaderRowEnd = "</font></tr>";
            string htmlTrStart = "<tr style =\"color:#000000;\">";
            string htmlTrEnd = "</tr>";
            string htmlTdCol1Start = "<td width=150  style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlTdEnd = "</font></td>";
            string htmlTdCol2Start = "<td width=450 align=\"left\" style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string message = string.Empty;

            //var providerCount = 0;

            foreach (KeyValuePair<string, Guid> entry in emailData)
            {
                var dupPractice = new Practice();

                var dupProvPractMatrix = providerPracticeMatrices.Where(x => x.NPINumber == entry.Key && x.PracticeGUID == entry.Value).FirstOrDefault();

                //logic for building the email content

                var provider = providers.Where(p => p.NPINumber == entry.Key).FirstOrDefault();
                var ancillary = providerAncillary.Where(a => a.ProviderPracticeMatrixID == dupProvPractMatrix.ID).FirstOrDefault();
                var degree = bppEntities.DegreeTypes.Where(x => x.DegreeID == provider.DegreeID).FirstOrDefault();


                //The info provided by the provider in the web form
                #region Details

                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Details</td>" + htmlHeaderRowEnd;
                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + provider.FirstName + " " + provider.MiddleName + " " + provider.LastName + " " + provider.Suffix + htmlTdEnd + htmlTrEnd;
                if (!string.IsNullOrEmpty(ancillary.AKA))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Also Known As" + htmlTdEnd + htmlTdCol2Start + ancillary.AKA + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTrStart + htmlTdCol1Start + "Degree" + htmlTdEnd + htmlTdCol2Start + degree.DegreeName + htmlTdEnd + htmlTrEnd;
                if (!String.IsNullOrEmpty(provider.PhoneNumber))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Phone Number" + htmlTdEnd + htmlTdCol2Start + provider.PhoneNumber + htmlTdEnd + htmlTrEnd;
                }

                message = message + htmlTrStart + htmlTdCol1Start + "NPI Number" + htmlTdEnd + htmlTdCol2Start + provider.NPINumber + htmlTdEnd + htmlTrEnd;

                if (provider.isBHPrivileged != null)
                {
                    if ((bool)provider.isBHPrivileged)
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Is Baptist Hospital Privileged" + htmlTdEnd + htmlTdCol2Start + "Yes" + htmlTdEnd + htmlTrEnd;
                    }
                    else
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Is Baptist Hospital Privileged" + htmlTdEnd + htmlTdCol2Start + "No" + htmlTdEnd + htmlTrEnd;
                    }
                }
                message = message + htmlTrStart + htmlTdCol1Start + "Applicant Status" + htmlTdEnd + htmlTdCol2Start + "This was an attempt to submit a duplicate provider" + htmlTdEnd + htmlTrEnd;

                dupPractice = practices.Where(p => p.PracticeGUID == dupProvPractMatrix.PracticeGUID).FirstOrDefault();

                var newLegalEntity = legalEntities.Where(l => l.LegalEntityID == dupPractice.LegalEntityID).FirstOrDefault();
                message = message + htmlTrStart + htmlTdCol1Start + "Legal Entity Name" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.LE_Name + htmlTdEnd + htmlTrEnd;

                message = message + htmlTrStart + htmlTdCol1Start + "Practice Name" + htmlTdEnd + htmlTdCol2Start + dupPractice.PracticeName + htmlTdEnd + htmlTrEnd;

                var oldProviderLocation = providerLocations.Where(pl => pl.ProviderPracticeMatrixID == dupProvPractMatrix.ID).FirstOrDefault();
                if (oldProviderLocation != null)
                {
                    var oldPrimaryLocation = locations.Where(l => l.ID == oldProviderLocation.LocationID).FirstOrDefault();

                    message = message + htmlTrStart + htmlTdCol1Start + "Location Name" + htmlTdEnd + htmlTdCol2Start + oldPrimaryLocation.LocationName + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTableEnd + "<BR><BR>";
                #endregion


                message = message + "<hr><BR><BR>";
            }

            var intro = string.Empty;
            var subject = "Duplicate Providers attempted to submit application";
            intro = "<font face=\"Arial\">The following provider(s) attempted to be resubmitted with identical information. </font><br><br>";


            var hostname = ConfigurationManager.AppSettings["ALC"];
            //Send New Member Request Information Email
            Message m = new Message();
            m.Sender = ConfigurationManager.AppSettings["sender"].ToString();
            m.Recipient = ConfigurationManager.AppSettings["recipient"].ToString();
            m.Subject = subject;
            m.Body = intro + message;

            new MessageController().sendMessage(m);

        }

        private void SendDuplicateProviderAttemptToApplicant(Dictionary<string, Guid> emailData, string activationKey)
        {
            Guid _guidparm;
            string _guid = Crypto.Decrypt(activationKey.ToString());
            _guidparm = Guid.Parse(_guid);
            var bppEntities = new BPPEntities();
            var applicantemail = "";

            var applicantprofile = new NewMemberApplication();
            var returningapplicantprofile = new ReturningMemberApplication();

            applicantprofile = bppEntities.NewMemberApplications.Where(x => x.GUID == _guidparm).FirstOrDefault();
            if (applicantprofile == null)
            {
                returningapplicantprofile = bppEntities.ReturningMemberApplications.Where(x => x.GUID == _guidparm).FirstOrDefault();
                applicantemail = returningapplicantprofile.Email;
            }
            else
            {
                applicantemail = applicantprofile.Email;
            }

            var providers = bppEntities.Providers;
            var practices = bppEntities.Practices;
            var legalEntities = bppEntities.LegalEntities;
            var providerLocations = bppEntities.ProviderLocations;
            var locations = bppEntities.Locations;
            var providerPracticeMatrices = bppEntities.ProviderPracticeMatrices;
            var providerAncillary = bppEntities.ProviderAncillaries;

            string htmlTableStart = "<table width=600 style=\"border-collapse:collapse; text-align:center;\" >";
            string htmlTableEnd = "</table>";
            string htmlHeaderRowStart = "<tr style =\"background-color:#000000; color:#ffffff;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlHeaderRowEnd = "</font></tr>";
            string htmlTrStart = "<tr style =\"color:#000000;\">";
            string htmlTrEnd = "</tr>";
            string htmlTdCol1Start = "<td width=150  style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string htmlTdEnd = "</font></td>";
            string htmlTdCol2Start = "<td width=450 align=\"left\" style=\" border-color:#000000; border-style:solid; border-width:thin; padding: 5px;font-size: 10pt;\"><font face=\"Arial\">";
            string message = string.Empty;


            foreach (KeyValuePair<string, Guid> entry in emailData)
            {
                var dupPractice = new Practice();

                var dupProvPractMatrix = providerPracticeMatrices.Where(x => x.NPINumber == entry.Key && x.PracticeGUID == entry.Value).FirstOrDefault();

                //logic for building the email content

                var provider = providers.Where(p => p.NPINumber == entry.Key).FirstOrDefault();
                var ancillary = providerAncillary.Where(a => a.ProviderPracticeMatrixID == dupProvPractMatrix.ID).FirstOrDefault();
                var degree = bppEntities.DegreeTypes.Where(x => x.DegreeID == provider.DegreeID).FirstOrDefault();


                //The info provided by the provider in the web form
                #region Details

                message = message + htmlTableStart + htmlHeaderRowStart + "<td colspan=\"2\">Details</td>" + htmlHeaderRowEnd;
                var date = Convert.ToDateTime(provider.CreatedDT);
                message = message + htmlTrStart + htmlTdCol1Start + "Initial Application submitted Date" + htmlTdEnd + htmlTdCol2Start + date.ToShortDateString() + htmlTdEnd + htmlTrEnd;

                message = message + htmlTrStart + htmlTdCol1Start + "Name" + htmlTdEnd + htmlTdCol2Start + provider.FirstName + " " + provider.MiddleName + " " + provider.LastName + " " + provider.Suffix + htmlTdEnd + htmlTrEnd;
                if (!string.IsNullOrEmpty(ancillary.AKA))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Also Known As" + htmlTdEnd + htmlTdCol2Start + ancillary.AKA + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTrStart + htmlTdCol1Start + "Degree" + htmlTdEnd + htmlTdCol2Start + degree.DegreeName + htmlTdEnd + htmlTrEnd;
                if (!String.IsNullOrEmpty(provider.PhoneNumber))
                {
                    message = message + htmlTrStart + htmlTdCol1Start + "Phone Number" + htmlTdEnd + htmlTdCol2Start + provider.PhoneNumber + htmlTdEnd + htmlTrEnd;
                }

                message = message + htmlTrStart + htmlTdCol1Start + "NPI Number" + htmlTdEnd + htmlTdCol2Start + provider.NPINumber + htmlTdEnd + htmlTrEnd;

                if (provider.isBHPrivileged != null)
                {
                    if ((bool)provider.isBHPrivileged)
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Is Baptist Hospital Privileged" + htmlTdEnd + htmlTdCol2Start + "Yes" + htmlTdEnd + htmlTrEnd;
                    }
                    else
                    {
                        message = message + htmlTrStart + htmlTdCol1Start + "Is Baptist Hospital Privileged" + htmlTdEnd + htmlTdCol2Start + "No" + htmlTdEnd + htmlTrEnd;
                    }
                }
                message = message + htmlTrStart + htmlTdCol1Start + "Applicant Status" + htmlTdEnd + htmlTdCol2Start + "This was an attempt to submit a duplicate provider" + htmlTdEnd + htmlTrEnd;

                dupPractice = practices.Where(p => p.PracticeGUID == dupProvPractMatrix.PracticeGUID).FirstOrDefault();

                var newLegalEntity = legalEntities.Where(l => l.LegalEntityID == dupPractice.LegalEntityID).FirstOrDefault();
                message = message + htmlTrStart + htmlTdCol1Start + "Legal Entity Name" + htmlTdEnd + htmlTdCol2Start + newLegalEntity.LE_Name + htmlTdEnd + htmlTrEnd;

                message = message + htmlTrStart + htmlTdCol1Start + "Practice Name" + htmlTdEnd + htmlTdCol2Start + dupPractice.PracticeName + htmlTdEnd + htmlTrEnd;

                var oldProviderLocation = providerLocations.Where(pl => pl.ProviderPracticeMatrixID == dupProvPractMatrix.ID).FirstOrDefault();
                if (oldProviderLocation != null)
                {
                    var oldPrimaryLocation = locations.Where(l => l.ID == oldProviderLocation.LocationID).FirstOrDefault();

                    message = message + htmlTrStart + htmlTdCol1Start + "Location Name" + htmlTdEnd + htmlTdCol2Start + oldPrimaryLocation.LocationName + htmlTdEnd + htmlTrEnd;
                }
                message = message + htmlTableEnd + "<BR><BR>";
                #endregion               

                message = message + "<hr><BR><BR>";
            }

            var intro = string.Empty;
            var subject = "Duplicate Providers attempted to submit application";
            intro = "<font face=\"Arial\">The following provider(s) attempted to be resubmitted with identical information. Please email bpp@bmcjax.com for questions or additional information. </font><br><br>";


            var hostname = ConfigurationManager.AppSettings["ALC"];
            //Send New Member Request Information Email
            Message m = new Message();
            m.Sender = ConfigurationManager.AppSettings["sender"].ToString();
            m.Recipient = applicantemail;
            m.Subject = subject;
            m.Body = intro + message;

            new MessageController().sendMessage(m);

        }

        [Route("getDegrees")]
        [HttpGet]
        public IHttpActionResult selDegrees()
        {
            var results = new PracticeDetail();
            var bppEntities = new BPPEntities();
            var headers = Request.Headers;
            var ActivationKey = "";
            if (headers.Contains("ActivationKey"))
            {
                ActivationKey = headers.GetValues("ActivationKey").First();
            }
            if (headers.Contains("SelectionOption"))
            {
                string selection = headers.GetValues("SelectionOption").First();
            }
            var guid = Crypto.Decrypt(ActivationKey.ToString());
            //Returns GUID from NewMemberApplication Table
            var guid_parm = Guid.Parse(guid);
            try
            {
                var _Degrees = bppEntities.DegreeTypes.ToList();
                var _Specialties = bppEntities.SpecialtyMatrices.ToList();

                //Returns LE_Guid from NewMemberApplication based on returned GUID from NewMemberApplications Table
                var legalentityId = bppEntities.NewMemberApplications.Where(p => p.GUID == guid_parm).Select(p => p.LE_Guid).SingleOrDefault();
                var legalentityId_returning = bppEntities.ReturningMemberApplications.Where(p => p.GUID == guid_parm).Select(p => p.LE_GUID).SingleOrDefault();

                Guid? LE_guid;
                if (legalentityId == null)
                {
                    LE_guid = legalentityId_returning;
                }
                else
                {
                    LE_guid = legalentityId;
                }

                Guid? _newLegalEntityID = LE_guid;
                //Returns Practices from Practice Table based on returned LegalEntityId
                var practices = bppEntities.Practices.Where(p => p.LegalEntityID == _newLegalEntityID).OrderBy(p => p.PracticeName).ToList();
                results._Practices = new List<PracticeExt>();
                //Foreach practice add that to the PracticeExt Model
                foreach (var item in practices)
                {
                    var count = 0;
                    PracticeExt practiceExt = new PracticeExt();
                    practiceExt.CreatedDT = item.CreatedDT;
                    practiceExt.ID = item.ID;
                    practiceExt.LegalEntityID = item.LegalEntityID.ToString();
                    practiceExt.ModifiedDT = item.ModifiedDT;
                    practiceExt.PracticeApprovalStatus = item.PracticeApprovalStatus;
                    practiceExt.PracticeGUID = item.PracticeGUID.ToString();
                    // practiceExt.PracticeLogoReceived = item.PracticeLogoReceived;
                    practiceExt.PracticeName = item.PracticeName;

                    results._Practices.Add(practiceExt);

                    //results._Practices[count].PracticeLocations = new List<PracticeLocations>();
                    ////After adding the practice, get the location
                    //foreach (var practice in results._Practices)
                    //{
                    //    var practiceguid = Guid.Parse(practice.PracticeGUID);
                    //    var Location = bppEntities.Locations.Where(p => p.PracticeGUID == practiceguid).ToList();
                    //    foreach (var _locations in Location)
                    //    {
                    //        PracticeLocations p = new PracticeLocations();
                    //        p.Address1 = _locations.Address1;
                    //        p.Address2 = _locations.Address2;
                    //        p.BillingManagerEmail = _locations.BillingManagerEmail;
                    //        p.BillingManagerExtension = _locations.BillingManagerExtension;
                    //        p.BillingManagerName = _locations.BillingManagerName;
                    //        p.BillingManagerPhoneNumber = _locations.BillingManagerPhoneNumber;
                    //        p.City = _locations.City;
                    //        p.CreatedDT = _locations.CreatedDT;
                    //        p.EMRVendor = _locations.EMRVendor;
                    //        p.EMRVersion = _locations.EMRVersion;
                    //        p.Extension = _locations.Extension;
                    //        p.Fax = _locations.Fax;
                    //        p.ID = _locations.ID;
                    //        p.IsPrimary = _locations.IsPrimary;
                    //        p.Lat = _locations.Lat;
                    //        p.Lng = _locations.Lng;
                    //        p.LocationGUID = _locations.LocationGUID;
                    //        p.LocationName = _locations.LocationName;
                    //        p.ModifiedDT = _locations.ModifiedDT;
                    //        p.OfficeManagerEmail = _locations.OfficeManagerEmail;
                    //        p.OfficeManagerExtension = _locations.OfficeManagerExtension;
                    //        p.OfficeManagerName = _locations.OfficeManagerName;
                    //        p.OfficeManagerPhoneNumber = _locations.OfficeManagerPhoneNumber;
                    //        p.PhoneNumber = _locations.PhoneNumber;
                    //        p.PracticeGUID = _locations.PracticeGUID;
                    //        p.State = _locations.State;
                    //        p.Zip = _locations.Zip;
                    //        results._Practices[count].PracticeLocations.Add(p);
                    //    }
                    //}
                    //count++;
                }
                results._Degrees = _Degrees;
                results._Specialties = _Specialties;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Json(results);
        }

        [Route("selPracticeLocations")]
        [HttpGet]
        [AllowCrosssiteJson]
        public IHttpActionResult selPracticeLocations()
        {
            var results = new List<PracticeLocations>();
            var bppEntities = new BPPEntities();
            var headers = Request.Headers;
            var PracticeGuid = "";
            if (headers.Contains("PracticeGuid"))
            {
                PracticeGuid = headers.GetValues("PracticeGuid").First();
            }
            var _practiceGuid = Guid.Parse(PracticeGuid);
            try
            {

                //results._Practices = new List<PracticeExt>();

                //var practices = bppEntities.Practices.Where(p => p.PracticeGUID == _practiceGuid).OrderBy(p => p.PracticeName).ToList();
                //foreach (var item in practices)
                //{
                //    var count = 0;
                //    PracticeExt practiceExt = new PracticeExt();
                //    practiceExt.CreatedDT = item.CreatedDT;
                //    practiceExt.ID = item.ID;
                //    practiceExt.LegalEntityID = item.LegalEntityID.ToString();
                //    practiceExt.ModifiedDT = item.ModifiedDT;
                //    practiceExt.PracticeApprovalStatus = item.PracticeApprovalStatus;
                //    practiceExt.PracticeGUID = item.PracticeGUID.ToString();
                //    practiceExt.PracticeLogoReceived = item.PracticeLogoReceived;
                //    practiceExt.PracticeName = item.PracticeName;

                //    results._Practices.Add(practiceExt);

                //    results._Practices[count].PracticeLocations = new List<PracticeLocations>();
                //    foreach (var practice in results._Practices)
                //    {
                //        var practiceguid = Guid.Parse(practice.PracticeGUID);
                var Location = bppEntities.Locations.Where(p => p.PracticeGUID == _practiceGuid && p.isRemoved == false).OrderBy(p => p.LocationName).ToList();
                foreach (var _locations in Location)
                {
                    PracticeLocations p = new PracticeLocations();
                    p.Address1 = _locations.Address1;
                    p.Address2 = _locations.Address2;
                    p.BillingManagerEmail = _locations.BillingManagerEmail;
                    p.BillingManagerExtension = _locations.BillingManagerExtension;
                    p.BillingManagerName = _locations.BillingManagerName;
                    p.BillingManagerPhoneNumber = _locations.BillingManagerPhoneNumber;
                    p.City = _locations.City;
                    p.CreatedDT = _locations.CreatedDT;
                    p.EMRVendor = _locations.EMRVendor;
                    p.EMRVersion = _locations.EMRVersion;
                    p.Extension = _locations.Extension;
                    p.Fax = _locations.Fax;
                    p.ID = _locations.ID;
                    p.IsPrimary = _locations.IsPrimary;
                    p.LocationGUID = _locations.LocationGUID;
                    p.LocationName = _locations.LocationName;
                    p.ModifiedDT = _locations.ModifiedDT;
                    p.OfficeManagerEmail = _locations.OfficeManagerEmail;
                    p.OfficeManagerExtension = _locations.OfficeManagerExtension;
                    p.OfficeManagerName = _locations.OfficeManagerName;
                    p.OfficeManagerPhoneNumber = _locations.OfficeManagerPhoneNumber;
                    p.PhoneNumber = _locations.PhoneNumber;
                    p.PracticeGUID = _locations.PracticeGUID;
                    p.State = _locations.State;
                    p.Zip = _locations.Zip;
                    results.Add(p);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Json(results);
        }

        [Route("sendEmail")]
        [HttpGet]
        public IHttpActionResult sendEmail()
        {
            var emailData = new Dictionary<string, Guid>();
            emailData.Add("2577523346", Guid.Parse("C0898990-BC1F-4F8F-91E6-C6E8ADE350DB"));
            //emailData.Add("1811192388", Guid.Parse("2589D65E-7CF0-4482-AA22-19E2C4F9D61F"));
            // emailData.Add("1811192388", Guid.Parse("2589D65E-7CF0-4482-AA22-19E2C4F9D61F"));

            try
            {
                SendProviderEmail(emailData);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}
