﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BPPAPI.Controllers.ApplicationProcess
{
    [RoutePrefix("api/applicationprocess")]
    public class ApplicationProcessController : ApiController
    {
        TextInfo caseFormat = new CultureInfo("en-US", false).TextInfo;

        [HttpPost]
        [Route("sendnewrequest")]
        public IHttpActionResult SendNewMemberRequest([FromBody] MemberRequest memberRequest)
        {
            try
            {
                var bppEntities = new BPPEntities();
                //Save Member Applicant Information
                var name = caseFormat.ToTitleCase(memberRequest.Name.ToLower());
                var email = memberRequest.Email;
                var guid = Guid.NewGuid();
                string encrypted = Crypto.Encrypt(guid.ToString());
                var appstep = 1;
                
                NewMemberApplication nma = new NewMemberApplication();
                nma.Name = name;
                nma.Email = email;
                nma.GUID = guid;
                nma.AppStep = appstep;
                nma.CreateDt = DateTime.Now;
                bppEntities.NewMemberApplications.Add(nma);
                bppEntities.SaveChanges();

                //URL for the next step in the process - Legal Entity Section
                var hostname = ConfigurationManager.AppSettings["ALC"];
                //Send New Member Request Information Email
                Message m = new Message();
                m.Sender = ConfigurationManager.AppSettings["sender"].ToString();
                m.Recipient = memberRequest.Email.ToString();
                //Pulls Email from db for New Member Request
                var body = bppEntities.Emails.Where(x => x.ID == 10).First();
                m.Body = body.HTML + "<p><h2><a href=" + hostname + "/Application/GetAppStep?ActivationKey=" + encrypted + ">Apply Online!</a></h2></p>";
                m.Subject = body.Title;
                new MessageController().sendMessage(m);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        [HttpPost]
        [Route("updatemember")]
        public IHttpActionResult UpdateMember()
        {
            string guid = null;
            Guid guid_param;

            string entityGuid = null;
            Guid entityGuid_param;

            var headers = Request.Headers;
            if (headers.Contains("ActivationKey"))
            {
                guid = headers.GetValues("ActivationKey").First();
            }
            if (headers.Contains("EntityGuid"))
            {
                entityGuid = headers.GetValues("EntityGuid").First();
            }            
            try
            {
                string _guid = Crypto.Decrypt(guid.ToString());
                guid_param = Guid.Parse(_guid);
                entityGuid_param = Guid.Parse(entityGuid);

                using (BPPEntities db = new BPPEntities())
                {
                    var member = db.NewMemberApplications.Where(x => x.GUID == guid_param).FirstOrDefault();
                    member.LE_Guid = entityGuid_param;
                    member.AppStep = 2;
                    member.ModifiedDt = DateTime.Now;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
            return Ok();
        }
       
        [HttpPost]
        [Route("updatememberstep")]
        public IHttpActionResult UpdateMemberStep()
        {
            string guid = null;
            var headers = Request.Headers;
            if (headers.Contains("ActivationKey"))
            {
                guid = headers.GetValues("ActivationKey").First();
            }
            string _guid = Crypto.Decrypt(guid.ToString());

            //Get AppStep for Member
            Guid guid_parm;
            guid_parm = Guid.Parse(_guid);
            var bppEntities = new BPPEntities();
            var applicant = bppEntities.NewMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
            var _returningapplicant = bppEntities.ReturningMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
            int appstep;
            if (applicant == null)
            {
                appstep = _returningapplicant.AppStep;
            }
            else 
            {
                appstep = applicant.AppStep;
            }
            try
            {
                using (BPPEntities db = new BPPEntities())
                {
                    if (applicant == null)
                    {
                        //Returning Member
                        var returningmember = db.ReturningMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
                        returningmember.AppStep = appstep + 1;
                        returningmember.ModifiedDt = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        // New Applicant
                        var member = db.NewMemberApplications.Where(x => x.GUID == guid_parm).FirstOrDefault();
                        member.AppStep = appstep + 1;
                        member.ModifiedDt = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }

            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
            return Ok();
        }
        [HttpPost]
        [Route("InsertENTITY")]
        public IHttpActionResult InsertEntity([FromBody] LegalEntity le)
        {
            var guid = Guid.NewGuid();
            try
            {
                using (BPPEntities db = new BPPEntities())
                {
                    //db.insLegalEntity(guid, le.TaxID, le.LE_Name, le.LE_Address1, le.LE_Address2, le.LE_City, le.LE_State, le.LE_Zip, le.POCName, le.POCPhoneNumber, le.POCFax, le.POCEmail, le.CAPName, le.CAPAddress1, le.CAPAddress2, le.CAPCity, le.CAPState, le.CAPZip, le.POCExtension);

                    LegalEntity legalEntity = new LegalEntity();
                    legalEntity.LegalEntityID = guid;
                    legalEntity.TaxID = le.TaxID;
                    legalEntity.LE_Name = caseFormat.ToTitleCase(le.LE_Name.ToLower());
                    legalEntity.LE_Address1 = le.LE_Address1;
                    legalEntity.LE_Address2 = le.LE_Address2;
                    legalEntity.LE_City = le.LE_City;
                    legalEntity.LE_State = le.LE_State;
                    legalEntity.LE_Zip = le.LE_Zip;
                    legalEntity.POCName = caseFormat.ToTitleCase(le.POCName.ToLower());
                    legalEntity.POCPhoneNumber = le.POCPhoneNumber;
                    //legalEntity.POCFax = le.POCFax;
                    legalEntity.POCFax = null;
                    legalEntity.POCEmail = le.POCEmail;
                    legalEntity.CAPName = caseFormat.ToTitleCase(le.CAPName.ToLower());
                    legalEntity.CAPAddress1 = le.CAPAddress1;
                    legalEntity.CAPAddress2 = le.CAPAddress2;
                    legalEntity.CAPCity = le.CAPCity;
                    legalEntity.CAPState = le.CAPState;
                    legalEntity.CAPZip = le.CAPZip;
                    legalEntity.POCExtension = le.POCExtension;
                    legalEntity.Status = "Inactive";
                    legalEntity.Created = DateTime.Now;
                    legalEntity.LastUpdated = DateTime.Now; 
                    db.LegalEntities.Add(legalEntity);

                    db.SaveChanges();

                    Audit.LegalEntityAudit(legalEntity, "application");                   
                }
            }
            catch (Exception ee)
            {
                return BadRequest(ee.InnerException.Message);
            }

            return Ok(guid);
        }
        [HttpGet]
        [Route("getApplicantStep")]
        public IHttpActionResult GetApplicantStep()
        {
            string guid = null;
            Guid guid_param;
            var headers = Request.Headers;
            if (headers.Contains("ActivationKey"))
            {
                guid = headers.GetValues("ActivationKey").First();
            }
            if (headers.Contains("SelectionOption"))
            {
                string selection = headers.GetValues("SelectionOption").First();
            }
            string _guid = Crypto.Decrypt(guid.ToString());            
            guid_param = Guid.Parse(_guid);
            try
            {
                using (BPPEntities db = new BPPEntities())
                {
                    var applicant = db.NewMemberApplications.Where(x => x.GUID == guid_param).FirstOrDefault();
                    if (applicant == null)
                    {
                        var returningmember = db.ReturningMemberApplications.Where(x => x.GUID == guid_param).FirstOrDefault();                       
                        if (returningmember != null)
                        {
                           var results = new ReturningMemberApp();
                            results.GUID = returningmember.GUID.ToString();
                            results.LE_GUID = returningmember.LE_GUID.ToString();
                            results.TIN = returningmember.TIN;
                            results.Email = returningmember.Email;
                            results.AppStep = returningmember.AppStep;
                            return Json(results);
                        }
                    }
                    else
                    {
                        var results = new NewMemberApp();
                        results.Name = applicant.Name;
                        results.Email = applicant.Email;
                        results.GUID = applicant.GUID.ToString();
                        results.AppStep = applicant.AppStep;
                        return Json(results);
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return BadRequest();
        }
    }
}
