﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BPPAPI.Controllers.ApplicationProcess
{
    [RoutePrefix("api/practicenames")]
    public class PracticeNameController : ApiController
    {
        TextInfo caseFormat = new CultureInfo("en-US", false).TextInfo;

        [HttpPost]
        [Route("InsertPractice")]
        public IHttpActionResult InsertPractice([FromBody] List<NewPractices> np)
        {
            string activationkey = null;
            var headers = Request.Headers;
            if (headers.Contains("ActivationKey"))
            {
                activationkey = headers.GetValues("ActivationKey").First();
            }
            string _activationkey = Crypto.Decrypt(activationkey);
            Guid Activationkey;
            Activationkey = Guid.Parse(_activationkey);

            //Get Legal Entity Guid for Member
            var bppEntities = new BPPEntities();
            var applicant = bppEntities.NewMemberApplications.Where(x => x.GUID == Activationkey).FirstOrDefault();
            var _returningapplicant = bppEntities.ReturningMemberApplications.Where(x => x.GUID == Activationkey).FirstOrDefault();
            Guid? le_guid;
            if (applicant == null)
            {
                le_guid = _returningapplicant.LE_GUID;
            }
            else
            {
                le_guid = applicant.LE_Guid;
            }
            try
            {
                using (BPPEntities db = new BPPEntities())
                {
                    foreach (var newPracticeName in np)
                    {
                        var ExistingPractice = db.Practices.Where(i => i.PracticeName == newPracticeName.PracticeName && i.LegalEntityID == le_guid).FirstOrDefault();

                        // Only add new practice name if it doesn't already exist
                        if (ExistingPractice == null)
                        {
                            var practice = new Practice();
                            practice.PracticeName = caseFormat.ToTitleCase(newPracticeName.PracticeName.ToLower());
                            practice.LegalEntityID = le_guid;
                            practice.PracticeApprovalStatus = 2;
                            practice.PracticeGUID = Guid.NewGuid();
                           // practice.PracticeLogoReceived = false;
                            practice.CreatedDT = DateTime.Now;
                            practice.ModifiedDT = DateTime.Now;
                            db.Practices.Add(practice);
                            db.SaveChanges();

                            Audit.PracticeAudit(practice, "application");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
            return Ok();
        }

        [HttpPost]
        [Route("UpdatePracticeNames")]
        public IHttpActionResult UpdatePractice()
        {
            var headers = Request.Headers;
            var activationkey = "";
            if (headers.Contains("ActivationKey"))
            {
                activationkey = headers.GetValues("ActivationKey").First();
            }
            
            string _activationkey = Crypto.Decrypt(activationkey);
            Guid Activationkey;
            try
            {
                Activationkey = Guid.Parse(_activationkey);
                var bppEntities = new BPPEntities();
                var applicant = bppEntities.NewMemberApplications.Where(x => x.GUID == Activationkey).FirstOrDefault();
                var _returningapplicant = bppEntities.ReturningMemberApplications.Where(x => x.GUID == Activationkey).FirstOrDefault();
                Guid? le_guid;
                if (applicant == null)
                {
                    le_guid = _returningapplicant.LE_GUID;
                }
                else
                {
                    le_guid = applicant.LE_Guid;
                }

                var PracticeNames = bppEntities.Practices.Where(x => x.LegalEntityID == le_guid).ToList();

                foreach (var item in PracticeNames)
                {
                    item.PracticeApprovalStatus = 4;
                }
                bppEntities.SaveChanges();

                //Update the Member Application Signed column in the Legal Entity table. This is the Date Submitted. 
                var LegalEntity = bppEntities.LegalEntities.Where(l => l.LegalEntityID == le_guid).FirstOrDefault();

                if (LegalEntity.MemberApplicationSigned == null)
                {
                    LegalEntity.MemberApplicationSigned = DateTime.Now;
                    bppEntities.SaveChanges();
                }
                Audit.LegalEntityAudit(LegalEntity, "application");
            }
            catch (Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
            return Ok();
        }

        [HttpGet]
        [Route("ValidatePracticeName")]
        [AllowCrosssiteJson]
        public IHttpActionResult ValidatePracticeName()
        {
            try
            {
                var headers = Request.Headers;
                string PracticeName = "";
                string ActivationKey = "";
                Guid _activationkey;
                if (headers.Contains("ActivationKey"))
                {
                    ActivationKey = headers.GetValues("ActivationKey").First();
                    PracticeName = headers.GetValues("PracticeName").First();
                }
                string _ActivationKey = Crypto.Decrypt(ActivationKey.ToString());
                _activationkey = Guid.Parse(_ActivationKey);

                using (BPPEntities db = new BPPEntities())
                {
                    if (PracticeName != null)
                    {
                        var applicant = db.NewMemberApplications.Where(x => x.GUID == _activationkey).FirstOrDefault();
                        var _returningapplicant = db.ReturningMemberApplications.Where(x => x.GUID == _activationkey).FirstOrDefault();
                        Guid? le_guid;
                        if (applicant == null)
                        {
                            le_guid = _returningapplicant.LE_GUID;
                        }
                        else
                        {
                            le_guid = applicant.LE_Guid;
                        }
                        
                        var Practicename = db.Practices.Where(p => p.LegalEntityID == le_guid && p.PracticeName == PracticeName).FirstOrDefault();
                        if (Practicename == null)
                        {
                            return Ok();
                        }
                        else
                        {
                            return BadRequest("Invalid Practice Name");
                        }
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest("Practice Name already exists for this TIN.");
            }
        }
    }
}
