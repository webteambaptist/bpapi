﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;


namespace BPPAPI.Controllers.ApplicationProcess
{
    [RoutePrefix("api/location")]
    public class LocationController : ApiController
    {
        TextInfo caseFormat = new CultureInfo("en-US", false).TextInfo;

        [HttpPost]
        [Route("submitLocations")]
        public IHttpActionResult SubmitLocations([FromBody] List<PracticeLocations> locations)
        {
            try
            {
                using (BPPEntities db = new BPPEntities())
                {
                    foreach (var location in locations)
                    {
                        var loc = db.Locations.Where(x => x.Address1 == location.Address1 && x.Address2 == location.Address2 && x.City == location.City && x.State == location.State && x.Zip == location.Zip && x.PracticeGUID == location.PracticeGUID).FirstOrDefault();

                        // Only add new location if it doesn't already exist
                        if (loc == null)
                        {
                            var newLocation = new Location();
                            newLocation.PracticeGUID = location.PracticeGUID;
                            newLocation.LocationGUID = Guid.NewGuid();
                            newLocation.LocationName = caseFormat.ToTitleCase(location.LocationName.ToLower());
                            newLocation.Address1 = location.Address1;
                            newLocation.Address2 = location.Address2;
                            newLocation.City = location.City;
                            newLocation.State = location.State;
                            newLocation.Zip = location.Zip;
                            newLocation.PhoneNumber = location.PhoneNumber;
                            newLocation.Extension = location.Extension;
                            newLocation.Fax = location.Fax;
                            newLocation.EMRVendor = location.EMRVendor;
                            newLocation.EMRVersion = location.EMRVersion;
                            newLocation.OfficeManagerName = caseFormat.ToTitleCase(location.OfficeManagerName.ToLower());
                            newLocation.OfficeManagerPhoneNumber = location.OfficeManagerPhoneNumber;
                            newLocation.OfficeManagerExtension = location.OfficeManagerExtension;
                            newLocation.OfficeManagerEmail = location.OfficeManagerEmail;
                            newLocation.CreatedDT = DateTime.Now;
                            newLocation.ModifiedDT = DateTime.Now;
                            db.Locations.Add(newLocation);
                            db.SaveChanges();

                            Audit.LocationAudit(newLocation, "application");
                            //var locationAudit = new LocationAudit();
                            //locationAudit.PracticeGUID = newLocation.PracticeGUID;
                            //locationAudit.LocationGUID = newLocation.LocationGUID;
                            //locationAudit.LocationName = newLocation.LocationName;
                            //locationAudit.Address1 = newLocation.Address1;
                            //locationAudit.Address2 = newLocation.Address2;
                            //locationAudit.City = newLocation.City;
                            //locationAudit.State = newLocation.State;
                            //locationAudit.Zip = newLocation.Zip;
                            //locationAudit.PhoneNumber = newLocation.PhoneNumber;
                            //locationAudit.Extension = newLocation.Extension;
                            //locationAudit.Fax = newLocation.Fax;
                            //locationAudit.EMRVendor = newLocation.EMRVendor;
                            //locationAudit.EMRVersion = newLocation.EMRVersion;
                            //locationAudit.OfficeManagerName = newLocation.OfficeManagerName;
                            //locationAudit.OfficeManagerPhoneNumber = newLocation.OfficeManagerPhoneNumber;
                            //locationAudit.OfficeManagerExtension = newLocation.OfficeManagerExtension;
                            //locationAudit.OfficeManagerEmail = newLocation.OfficeManagerEmail;
                            //locationAudit.CreatedDT = DateTime.Now;
                            //locationAudit.ModifiedDT = DateTime.Now;
                            //locationAudit.ModifiedUser = "application";
                            //db.LocationAudits.Add(locationAudit);
                            //db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
            return Ok();
        }

        [HttpGet]
        [Route("getpractices")]
        public IHttpActionResult GetPractices()
        {
            string ActivationKey = "";

            var headers = Request.Headers;

            if (headers.Contains("ActivationKey"))
            {
                ActivationKey = headers.GetValues("ActivationKey").First();
            }
            var GUID = Crypto.Decrypt(ActivationKey.ToString());

            var guid_param = Guid.Parse(GUID);

            var bppEntities = new BPPEntities();

            var legalEntityId = bppEntities.NewMemberApplications.Where(x => x.GUID == guid_param).FirstOrDefault();
            var legalEntityId_returning = bppEntities.ReturningMemberApplications.Where(x => x.GUID == guid_param).FirstOrDefault();
            try
            {
                if (legalEntityId == null)
                {
                    var practices = bppEntities.Practices.Where(p => p.LegalEntityID == legalEntityId_returning.LE_GUID).OrderBy(p => p.PracticeName).ToList();
                    return Ok(practices);
                }
                else
                {
                    var practices = bppEntities.Practices.Where(p => p.LegalEntityID == legalEntityId.LE_Guid).OrderBy(p => p.PracticeName).ToList();
                    return Ok(practices);
                }
            }
            catch (Exception e)
            {
                return BadRequest("ActivationKey is not found");
            }
        }

        [HttpGet]
        [Route("validatelocname")]
        public IHttpActionResult ValidateLocName()
        {
            try
            {
                var headers = Request.Headers;
                string LocName = "";
                string PracticeGUID = "";
                try
                {
                    if (headers.Contains("LocationName"))
                    {
                        LocName = headers.GetValues("LocationName").First();
                        PracticeGUID = headers.GetValues("PracticeGuid").First();
                    }
                   
                }
                catch (Exception e)
                {
                    return BadRequest("Invalid Location Name");
                }
                using (BPPEntities db = new BPPEntities())
                {
                    if (PracticeGUID != null)
                    {
                        var guid = Guid.Parse(PracticeGUID);
                        var loc = db.Locations.Where(x => x.LocationName == LocName && x.PracticeGUID == guid).FirstOrDefault();
                        if (loc != null)
                        {
                            return BadRequest("Invalid Location Name");
                        }
                        else
                        {
                            return Ok();
                        }
                    }
                    else
                    {
                        return BadRequest("Invalid Request");
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message + " " + e.InnerException);
            }
        }
    }
}