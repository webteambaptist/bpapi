﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for file I/O.
    /// </summary>
    
    [RoutePrefix("api/filehandler")]
    public class FileController : ApiController
    {
        private static readonly string ServerUploadFolder = @"\\bhdiiswfe01v\d$\Documents\BPP\";


        //private static readonly string ServerUploadFolder = @"d:\temp\";
        /// <summary>
        /// Method to save files back to filesystem.
        /// </summary>
        /// <returns></returns>
        [Route("savefile")]
        [HttpPost]
        public HttpResponseMessage savefile()
        {
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string fileName in httpRequest.Files.Keys)
                {
                    var file = httpRequest.Files[fileName];
                    //var filePath = HttpContext.Current.Server.MapPath("~/" + file.FileName);
                    var filePath = ServerUploadFolder + file.FileName;
                    file.SaveAs(filePath);
                }

                return Request.CreateResponse(HttpStatusCode.Created);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

    }
}
