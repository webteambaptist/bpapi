﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using System;
using BPPAPI.Models;
using System.Net.Http.Headers;
using System.Data;
using BPPAPI.Helpers;
using System.Text;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for SSRS Reports
    /// </summary>

    [RoutePrefix("api/SSRS")]
    public class SSRSController : ApiController
    {
        /// <summary>
        /// retrieves csv formatted report by name
        /// </summary>
        /// <param name="reportname"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("reportname")]
        public IHttpActionResult getReport()
        {
            string reportname = "";
            var headers = Request.Headers;
            var bppEntities = new BPPEntities();

            DataTable dt = new DataTable();


            if (headers.Contains("reportname"))
            {
                reportname = headers.GetValues("reportname").First();
            }

            if (!string.IsNullOrEmpty(reportname))
            {
                try
                {
                    if (reportname == "Med Staff Report")
                    {
                        var rpt = bppEntities.BPP_MedStaffReport();
                        dt = ExcelExport.ConvertTo<BPP_MedStaffReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Committee Report")
                    {
                        var rpt = bppEntities.BPP_CommitteeReport();
                        dt = ExcelExport.ConvertTo<BPP_CommitteeReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Board Report")
                    {
                        var rpt = bppEntities.BPP_BoardMemberReport();
                        dt = ExcelExport.ConvertTo<BPP_BoardMemberReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Left Network Report")
                    {
                        var rpt = bppEntities.BPP_LeftNetworkReport();
                        dt = ExcelExport.ConvertTo<BPP_LeftNetworkReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Current Members")
                    {
                        var rpt = bppEntities.BPP_PhysicianMembers();
                        dt = ExcelExport.ConvertTo<BPP_PhysicianMembers_Result>(rpt.ToList());
                    }
                    else if (reportname == "Master Lookup")
                    {
                        var rpt = bppEntities.BPP_MasterLookupReport2();
                        dt = ExcelExport.ConvertTo<BPP_MasterLookupReport2_Result>(rpt.ToList());
                    }
                    else if (reportname == "Legal Entity Report")
                    {
                        var rpt = bppEntities.BPP_LegalEntityReport();
                        dt = ExcelExport.ConvertTo<BPP_LegalEntityReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Group Point of Contact (POC) Report")
                    {
                        var rpt = bppEntities.BPP_TINPOCReport();
                        dt = ExcelExport.ConvertTo<BPP_TINPOCReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Active Adult PCP Report")
                    {
                        var rpt = bppEntities.BPP_PCPReport();
                        dt = ExcelExport.ConvertTo<BPP_PCPReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Active Adult Specialists Report Report")
                    {
                        var rpt = bppEntities.BPP_SpecialistsReport();
                        dt = ExcelExport.ConvertTo<BPP_SpecialistsReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Active Pediatrician Report")
                    {
                        var rpt = bppEntities.BPP_PedsReport();
                        dt = ExcelExport.ConvertTo<BPP_PedsReport_Result>(rpt.ToList());
                    }
                    else if (reportname == "Active Pediatric Specialists Report")
                    {
                        var rpt = bppEntities.BPP_PedsSpecialistsReport();
                        dt = ExcelExport.ConvertTo<BPP_PedsSpecialistsReport_Result>(rpt.ToList());
                    }

                    else if (reportname == "Custom Report")
                    {
                        return Redirect("~/Home/Admin/Report");
                    }
                }
                catch (Exception e)
                {
                    // really not doing anything. I just need it to error out gracefully
                    var exception = e.Message + " " + e.InnerException;
                }

                //This gives you the byte array.

                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(ExcelExport.ToCSV(dt));
                System.IO.MemoryStream stream = new System.IO.MemoryStream(bytes);

                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(bytes)
                };

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = reportname + ".csv";

                result.Content.Headers.ContentType =
                    new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

                return ResponseMessage(result);

                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
