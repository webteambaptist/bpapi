﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Mail;
namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for messaging
    /// </summary>
    [RoutePrefix("api/msg")]
    public class MessageController : ApiController
    {
        [Route("sendmsg")]
        [HttpPost]
        /// <summary>
        /// Method to send messages
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public IHttpActionResult sendMessage([FromBody] Message message)
        {
            var bppEntities = new BPPEntities();
            msg m = new msg();

          //  if (string.IsNullOrEmpty(message.Sender))
          //  {
                m.from = ConfigurationManager.AppSettings["sender"];
            //}
            //else
            //{
            //    m.from = message.Sender;
            //}

            m.sendto = message.Recipient;
            m.body = message.Body;
            m.subject = message.Subject;

            string result = Mail.sendMail(m);

            if (result == "")
            {

                bppEntities.Messages.Add(new Message
                {
                    CreatedDT = DateTime.Now,
                    Sender = m.from,
                    Recipient = m.sendto,
                    Body = m.body,
                    Subject = m.subject
                });

                bppEntities.SaveChanges();
            }
            else
            {

                bppEntities.Messages.Add(new Message
                {
                    CreatedDT = DateTime.Now,
                    Sender = m.from,
                    Recipient = m.sendto,
                    Body = m.body,
                    Subject = "ERROR " + result + " " +  m.subject
                });

                bppEntities.SaveChanges();
            }


            return Ok();
        }
        [Route("sendmsg")]
        [HttpPost]
        /// <summary>
        /// Method to send messages
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void sendMessage(MailMessage message)
        {
            var bppEntities = new BPPEntities();
            
            string result = Mail.sendMail(message);

            if (result == "")
            {

                bppEntities.Messages.Add(new Message
                {
                    CreatedDT = DateTime.Now,
                    Sender = message.From.ToString(),
                    Recipient = message.To.ToString(),
                    Body = message.Body,
                    Subject = message.Subject
                });

                bppEntities.SaveChanges();
            }
            else
            {

                bppEntities.Messages.Add(new Message
                {
                    CreatedDT = DateTime.Now,
                    Sender = message.From.ToString(),
                    Recipient = message.To.ToString(),
                    Body = message.Body,
                    Subject = "ERROR " + result + " " + message.Subject
                });

                bppEntities.SaveChanges();
            }
        }
        [Route("contactus")]
        [HttpPost]
        /// <summary>
        /// Method to send messages
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public void contactUs([FromBody] Message message)
        {
            var bppEntities = new BPPEntities();
            msg m = new msg();
            m.body = message.Body;
            m.from = message.Sender;
            m.sendto = message.Recipient;
            m.subject = message.Subject;
            
            string result = Mail.sendMail(m);

            if (result == "")
            {

                bppEntities.Messages.Add(new Message
                {
                    CreatedDT = DateTime.Now,
                    Sender = message.Sender.ToString(),
                    Recipient = message.Recipient.ToString(),
                    Body = message.Body,
                    Subject = message.Subject
                });

                bppEntities.SaveChanges();
            }
            else
            {

                bppEntities.Messages.Add(new Message
                {
                    CreatedDT = DateTime.Now,
                    Sender = message.Sender.ToString(),
                    Recipient = message.Recipient.ToString(),
                    Body = message.Body,
                    Subject = "ERROR " + result + " " + message.Subject
                });

                bppEntities.SaveChanges();
            }
        }
    }
}
