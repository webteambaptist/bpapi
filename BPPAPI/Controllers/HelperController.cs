﻿using BPPAPI.Helpers;
using System;
using System.Linq;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// API with utility methods
    /// </summary>

    [RoutePrefix("api/helper")]
    public class HelperController : ApiController
    {
        /// <summary>
        /// function encrypt string and return encrypted/encoded link
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("EC")]
        public IHttpActionResult EncryptLink()
        {
            var headers = Request.Headers;
            string link = "";
            try
            {
                if (headers.Contains("ActivationKey"))
                {
                    link = headers.GetValues("ActivationKey").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }

            string outlink = Crypto.Encrypt(link);

            return Ok(outlink);
        }

        /// <summary>
        /// function to decrypt link and return plain string
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("DC")]
        public IHttpActionResult DecryptLink()
        {
            var headers = Request.Headers;
            string link = "";
            try
            {
                if (headers.Contains("ActivationKey"))
                {
                    link = headers.GetValues("ActivationKey").First();
                }
            }
            catch (Exception ee)
            {
                return BadRequest();
            }

            string outlink = Crypto.Decrypt(link);

            return Ok(outlink);

        }
    }
}
