﻿using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for Provider Application Workflow
    /// </summary>
    /// 
    [RoutePrefix("api/wf")]
    public class ProviderWorkflowController : ApiController
    {
        [Route("GETTAXID")]
        [HttpGet]
        public IHttpActionResult selTaxId()
        {
            try
            {
                var headers = Request.Headers;
                string practiceName=null;
                if (headers.Contains("PracticeName"))
                {
                    practiceName = headers.GetValues("PracticeName").First();
                }
                var bppEntities = new BPPEntities();
                var le = bppEntities.Practices.Where(x => x.PracticeName == practiceName).Select(u=>u.LegalEntityID).Single();
                var taxId = bppEntities.LegalEntities.Where(x => x.LegalEntityID == le).Select(u => u.TaxID).Single();

                return Json(taxId);
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }
        }
        [Route("GETPRACTICES")]
        [HttpGet]
        public IHttpActionResult selPractices()
        {
            try
            {
                var bppEntities = new BPPEntities();
               // var practices = bppEntities.Practices.Select(x => new { x.ID, x.PracticeName }).OrderBy(i=>i.PracticeName).ToList();
                var practices = bppEntities.Practices.Where(x => x.PracticeApprovalStatus == 4).Select(x => new { x.ID, x.PracticeName }).OrderBy(o => o.PracticeName).ToList();
                return Json(practices);
            } catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }
        }
        /// <summary>
        /// Get specialty list
        /// </summary>
        /// <returns></returns>
        [Route("GETSPECIALS")]
        [HttpGet]
        public IHttpActionResult selSpecials()
        {
            try
            {
                var bppEntities = new BPPEntities();
                return Json(bppEntities.SpecialtyMatrices);
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }

        }
        [Route("GETDEGREES")]
        [HttpGet]
        public IHttpActionResult selDegrees()
        {
            try
            {
                var bppEntities = new BPPEntities();
                return Json(bppEntities.DegreeTypes);
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }
        }
        [Route("GetStatusChangeReasons")]
        [HttpGet]
        public IHttpActionResult selStatusChangeReasons()
        {
            try
            {
                var bppEntities = new BPPEntities();
                return Json(bppEntities.StatusChangeReasons);
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }

        }
    }
}
