﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;


namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for Practice Application 
    /// </summary>
    [RoutePrefix("api/applicationlist")]
    public class ApplicationListController : ApiController
    {
        // <summary>
        // Get Application Data by Provider
        //</summary>        
        // <returns></returns>        
        [HttpGet]
        [Route("LISTBYPROVIDER")]
        public IHttpActionResult ProviderAppData()
        {
            var bppEntities = new BPPEntities();
            // we need all providers that are active
            var allProviders = bppEntities.Providers;
            
            var providerList = new Providers._ProvidersListShort();
            var providers = new List<Providers._ProvidersShort>();
            try
            {
                foreach (var item in allProviders)
                {
                    var provPractice = bppEntities.ProviderPracticeMatrices.Where(x=>x.NPINumber==item.NPINumber).ToList();
                    foreach (var matrix in provPractice)
                    {

                        
                        // exclude deleted or those who have left the group (end date)
                        if (item.isDeleted == false)
                        {
                            var pMemStatus = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == item.NPINumber && x.PracticeGUID == matrix.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).FirstOrDefault();

                            // only display ones that are not deleted and a member
                            if (pMemStatus != null) //getting null reference error 2/25/19 MDO
                            {
                                if (pMemStatus.ProviderStatusID >= 1)
                                {
                                    List<ProviderSpecialtyMatrix> specialties = new List<ProviderSpecialtyMatrix>();
                                    ProviderFull prov = new ProviderFull();
                                    prov.matrix = matrix;
                                    var provider = new Providers._ProvidersShort();
                                    try
                                    {
                                        if (pMemStatus.DateJoinderReceived == null && pMemStatus.BPPEffectiveDate == null && pMemStatus.DatePresentedMQProcess == null && pMemStatus.DatePresentedReady == null && pMemStatus.DateRecommendationSent == null && pMemStatus.DateSignedJoinder == null && pMemStatus.DateSignedMemberApp != null && pMemStatus.NotificationDate == null)
                                        {
                                            prov.isNew = true;
                                        }
                                        else
                                        {
                                            prov.isNew = false;
                                        }
                                        specialties = bppEntities.ProviderSpecialtyMatrices.Where(x => x.NPINumber == item.NPINumber && x.PracticeGUID == matrix.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).ToList();
                                        foreach (var specs in specialties)
                                        {
                                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == specs.SpecialtyID).FirstOrDefault();
                                            if (specs.IsPrmary == true)
                                            {
                                                prov.PrimarySpecialty = specLookup.Specialty;
                                            }
                                            else
                                            {
                                                prov.SecondarySpecialty = specLookup.Specialty;
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        specialties = null;
                                    }
                                    try
                                    {
                                        if (pMemStatus == null)
                                        {
                                            prov.ProviderMemberStatus = 0;
                                        }
                                        else
                                        {
                                            prov.ProviderMemberStatus = pMemStatus.ProviderStatusID;//pMemStatus.ProviderMemberStatus;
                                        }
                                        var ancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == item.NPINumber && x.PracticeGuid == matrix.PracticeGUID && x.ProviderPracticeMatrixID == matrix.ID).FirstOrDefault();
                                        
                                        prov.PrimaryPracticeLocation = "0";
                                        if (ancillary != null)
                                        {
                                            ProviderAncillary a = new ProviderAncillary();
                                            a.AKA = ancillary.AKA;
                                            a.EmploymentStatus = ancillary.EmploymentStatus;
                                            a.SpecialtyType = ancillary.SpecialtyType;
                                            a.Maiden_Name = ancillary.Maiden_Name;
                                            a.ServicePopulation = ancillary.ServicePopulation;
                                            prov.ProviderAncillary = a;
                                        }
                                        prov.CMS = item.CMS;
                                        prov.CreatedDT = item.CreatedDT;
                                        prov.DateOfBirth = item.DateOfBirth;
                                        prov.Email = item.Email;
                                        prov.FirstName = item.FirstName;
                                        prov.GUID = item.GUID;
                                        prov.ID = item.ID;
                                        prov.isBHPrivileged = Convert.ToBoolean(item.isBHPrivileged);
                                        prov.LastName = item.LastName;
                                        prov.MiddleName = item.MiddleName;
                                        prov.ModifiedDT = item.ModifiedDT;
                                        prov.NPINumber = item.NPINumber;
                                        prov.PhoneNumber = item.PhoneNumber;
                                        prov.Suffix = item.Suffix;
                                        //prov.TaxID = item.TaxID;

                                        prov.MemberStatus = bppEntities.ProviderMemberStatus.Where(e => e.StatusID == prov.ProviderMemberStatus).FirstOrDefault();
                                        provider._Provider = prov; // item
                                        var practicedata = bppEntities.selPracticesShortDataWithGuid4().ToList();

                                        var pdata = practicedata.Where(x => x.PracticeGUID == matrix.PracticeGUID).FirstOrDefault();

                                        if (pdata != null)
                                        {
                                            PracticeShort p = new PracticeShort();
                                            p.ID = pdata.id;
                                            if (pdata.LegalEntityID != null)
                                            {
                                                p.LegalEntityID = pdata.LegalEntityID.ToString();
                                            }

                                            p.LegalEntityID = pdata.LegalEntityID.ToString();
                                            p.PracticeName = pdata.PracticeName;
                                            p.PracticeGUID = pdata.PracticeGUID.ToString();
                                            var entity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == pdata.LegalEntityID).FirstOrDefault();
                                            provider._LegalEntity = entity;

                                            provider._Practice = p;
                                            provider.PracticeLocations = null;

                                            providers.Add(provider);
                                        }


                                    }
                                    catch (Exception e)
                                    {
                                        var message = e.StackTrace + " " + e.Message + " " + e.InnerException.Message;
                                        return BadRequest(message);
                                    }
                                }
                                else
                                {
                                    var provider = new Providers._ProvidersShort();
                                    //ProviderPracticeWorkflow pPracticeWorkflow = new ProviderPracticeWorkflow();

                                    ProviderFull prov = new ProviderFull();
                                    prov.matrix = matrix;
                                    //pPracticeWorkflow = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == item.NPINumber && x.PracticeGUID == matrix.PracticeGUID).First();

                                    if (pMemStatus.DateJoinderReceived == null && pMemStatus.BPPEffectiveDate == null && pMemStatus.DatePresentedMQProcess == null && pMemStatus.DatePresentedReady == null && pMemStatus.DateRecommendationSent == null && pMemStatus.DateSignedJoinder == null && pMemStatus.DateSignedMemberApp != null && pMemStatus.NotificationDate == null)
                                    {
                                        prov.isNew = true;
                                    }
                                    else
                                    {
                                        prov.isNew = false;
                                    }
                                    List<ProviderSpecialtyMatrix> specialties = new List<ProviderSpecialtyMatrix>();

                                    try
                                    {
                                        specialties = bppEntities.ProviderSpecialtyMatrices.Where(x => x.NPINumber == item.NPINumber && x.PracticeGUID == matrix.PracticeGUID).ToList();
                                        foreach (var specs in specialties)
                                        {
                                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == specs.SpecialtyID).FirstOrDefault();

                                            if (specs.IsPrmary == true)
                                            {
                                                prov.PrimarySpecialty = specLookup.Specialty;
                                            }
                                            else
                                            {
                                                prov.SecondarySpecialty = specLookup.Specialty;
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        specialties = null;
                                    }

                                    List<ProviderLocation> locList = new List<ProviderLocation>();
                                    try
                                    {
                                        locList = bppEntities.ProviderLocations.Where(x => x.NPINumber == item.NPINumber && x.PracticeGUID == matrix.PracticeGUID).ToList();
                                    }
                                    catch (Exception e)
                                    {
                                        locList = null;
                                    }


                                    try
                                    {
                                        var facList = bppEntities.Facilities.ToList();
                                        prov.Facilties = facList;
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                    List<int> providerFacilities = new List<int>();
                                    try
                                    {
                                        var ProvFacilities = bppEntities.ProviderFacilities.Where(e => e.NPINumber == item.NPINumber && e.PracticeGUID == matrix.PracticeGUID).ToList();
                                        if (ProvFacilities != null)
                                        {

                                            foreach (var facility in ProvFacilities)
                                            {
                                                providerFacilities.Add(facility.FacilityID);
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                    try
                                    {
                                        prov.ProviderPracticeWorkFlow = pMemStatus;
                                        prov.ProviderSpecialties = specialties;
                                        if (pMemStatus == null)
                                        {
                                            prov.ProviderMemberStatus = 0;
                                        }
                                        else
                                        {
                                            prov.ProviderMemberStatus = pMemStatus.ProviderStatusID;// pMemStatus.ProviderMemberStatus;
                                        }
                                        prov.PrimaryPracticeLocation = "0";
                                        var ancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == item.NPINumber && x.PracticeGuid == matrix.PracticeGUID).FirstOrDefault();
                                        if (ancillary != null)
                                        {
                                            ProviderAncillary a = new ProviderAncillary();
                                            a.Maiden_Name = ancillary.Maiden_Name;
                                            a.SpecialtyType = ancillary.SpecialtyType;
                                            a.ServicePopulation = ancillary.ServicePopulation;
                                            a.AKA = ancillary.AKA;
                                            a.EmploymentStatus = ancillary.EmploymentStatus;
                                            prov.ProviderAncillary = a;
                                        }
                                        prov.Locations = locList;
                                        prov.ProviderFacilities = providerFacilities;
                                        // prov.Based = item.Based;
                                        // prov.CareType = item.CareType;
                                        prov.CMS = item.CMS;
                                        prov.CreatedDT = item.CreatedDT;
                                        prov.DateOfBirth = item.DateOfBirth;
                                      // prov.DateOfBoardExam = item.DateOfBoardExam;
                                        prov.Email = item.Email;
                                        if (pMemStatus != null)
                                        {
                                            prov.PrimaryPracticeLocation = pMemStatus.PrimaryPracticeLocation;
                                        }
                                        else
                                        {
                                            prov.PrimaryPracticeLocation = "0";
                                        }
                                        prov.FirstName = item.FirstName;
                                        prov.GUID = item.GUID;
                                        prov.ID = item.ID;
                                        prov.isBHPrivileged = Convert.ToBoolean(item.isBHPrivileged);
                                      //  prov.isBoardCert = Convert.ToBoolean(item.isBoardCert);
                                        prov.LastName = item.LastName;
                                        prov.MiddleName = item.MiddleName;
                                        prov.ModifiedDT = item.ModifiedDT;
                                        prov.NPINumber = item.NPINumber;
                                        prov.PhoneNumber = item.PhoneNumber;
                                        // prov.ProviderType = item.ProviderType;
                                       // prov.ResidencyCompletionDate = item.ResidencyCompletionDate;
                                        prov.Suffix = item.Suffix;
                                        //prov.TaxID = item.TaxID;
                                        prov.MemberStatus = bppEntities.ProviderMemberStatus.Where(e => e.StatusID == prov.ProviderMemberStatus).FirstOrDefault();
                                        
                                        provider._Provider = prov; // item
                                        var practicedata = bppEntities.selPracticesShortDataWithGuid4().ToList();

                                        var pdata = practicedata.Where(x => x.PracticeGUID == matrix.PracticeGUID).FirstOrDefault();

                                        if (pdata != null)
                                        {
                                            PracticeShort p = new PracticeShort();
                                            p.ID = pdata.id;
                                            if (pdata.LegalEntityID != null)
                                            {
                                                p.LegalEntityID = pdata.LegalEntityID.ToString();
                                            }

                                            p.PracticeName = pdata.PracticeName;
                                            p.PracticeGUID = pdata.PracticeGUID.ToString();
                                           
                                            try
                                            {
                                                p.PracticeApprovalStatus = int.Parse(pdata.practiceapprovalstatus.ToString());
                                            }
                                            catch (Exception ee)
                                            {
                                                p.PracticeApprovalStatus = 1;
                                            }
                                            var entity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == pdata.LegalEntityID).FirstOrDefault();
                                            provider._LegalEntity = entity;

                                            provider._Practice = p;
                                            provider.PracticeLocations = null;
                                            providers.Add(provider);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        var message = e.StackTrace + " " + e.Message + " " + e.InnerException.Message;
                                        return BadRequest(message);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ee)
            {
                return BadRequest(ee.StackTrace);
            }


            providerList._ProvidersShort = providers.OrderBy(x => x._Provider.LastName).ToList();
            return Json(providerList);
        }


        /// <summary>
        /// Update Practice Profile
        /// </summary>
        /// <param name="prf"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UPDATEAPP")]
        public IHttpActionResult updatePAD([FromBody] PracticeFull prf)
        {
            var bppEntities = new BPPEntities();
            var practices = bppEntities.Practices;
            var providers = bppEntities.Providers;
            string UpdateUser = "";
            var pf = new PracticeExt();
           // var locations = new List<PracticeLocations>();
            //var contracts = new List<PracticeContracts>();
            string hostName = ConfigurationManager.AppSettings["ALC"];
            var id = Guid.Parse(prf._Practices.FirstOrDefault().LegalEntityID);
            var headers = Request.Headers;

            if (headers.Contains("UserName"))
            {
                UpdateUser = headers.GetValues("UserName").First();
            }
            try
            {

                pf = prf._Practices.FirstOrDefault();
            }
            catch (Exception ee)
            {
                pf = null;
            }
 
            //update the locations first
            try
            {
                if (pf.PracticeLocations != null)
                {
                    foreach (var loc in pf.PracticeLocations)
                    {
                        var Location = bppEntities.Locations.Where(x=>x.LocationGUID == loc.LocationGUID).FirstOrDefault();
                        
                        Location.Address1 = loc.Address1;
                        Location.Address2 = loc.Address2;
                        Location.BillingManagerEmail = loc.BillingManagerEmail;
                        Location.BillingManagerExtension = loc.BillingManagerExtension;
                        Location.BillingManagerName = loc.BillingManagerName;
                        Location.BillingManagerPhoneNumber = loc.BillingManagerPhoneNumber;
                        Location.City = loc.City;
                        Location.EMRVendor = loc.EMRVendor;
                        Location.EMRVersion = loc.EMRVersion;
                        Location.Extension = loc.Extension;
                        Location.Fax = loc.Fax;
                        Location.LocationName = loc.LocationName;
                        Location.ModifiedDT = DateTime.Now;
                        Location.OfficeManagerEmail = loc.OfficeManagerEmail;
                        Location.OfficeManagerExtension = loc.OfficeManagerExtension;
                        Location.OfficeManagerName = loc.OfficeManagerName;
                        Location.OfficeManagerPhoneNumber = loc.OfficeManagerPhoneNumber;
                        Location.PhoneNumber = loc.PhoneNumber;
                        Location.State = loc.State;
                        Location.Zip = loc.Zip;
                        bppEntities.SaveChanges();
                        Audit.LocationAudit(Location, UpdateUser);

                    }
                }

                if (pf != null)
                {
                    var practiceGuid = Guid.Parse(pf.PracticeGUID);
                    var Practice = bppEntities.Practices.Where(x => x.PracticeGUID == practiceGuid).FirstOrDefault();
                    
                    Practice.ModifiedDT = DateTime.Now;
                    Practice.PracticeApprovalStatus = pf.PracticeApprovalStatus;
                    Practice.PracticeName = pf.PracticeName;
                    
                    bppEntities.SaveChanges();
                    Audit.PracticeAudit(Practice, UpdateUser);
                }
            }
            catch (Exception ee)
            {
                return BadRequest(ee.InnerException.Message);
            }

            return Ok();
        }

        public void updateFacMatrix(ProviderFull p, string UserName)
        {
            try
            {
                var bppEntities = new BPPEntities();
                var facs = bppEntities.ProviderFacilities.Where(e => e.NPINumber == p.NPINumber && e.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID).ToList();
                
                foreach (var fac in facs)
                {
                    bppEntities.ProviderFacilities.Remove(fac);
                }
                bppEntities.SaveChanges(); //clear out the entries marked for deletion

                foreach (var pl in p.ProviderFacilities)
                {
                    ProviderFacility pf = new ProviderFacility();
                    pf.FacilityID = pl;
                    pf.NPINumber = p.NPINumber;
                    pf.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                    pf.ProviderPracticeMatrixID = p.matrix.ID;
                    bppEntities.ProviderFacilities.Add(pf);
                    Audit.ProviderFacilityAudit(pf, UserName);
                }

                bppEntities.SaveChanges();

                var paps = bppEntities.ProviderAcceptingPatients.Where(e => e.NPINumber == p.NPINumber && e.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID).ToList();

                foreach (var pap in paps)
                {
                    bppEntities.ProviderAcceptingPatients.Remove(pap);
                }
                bppEntities.SaveChanges();

                foreach (var item in p.ProviderAcceptingPatients)
                {
                    ProviderAcceptingPatient pa = new ProviderAcceptingPatient();
                    pa.ContractID = item;
                    pa.NPINumber = p.NPINumber;
                    pa.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                    pa.ProviderPracticeMatrixID = p.matrix.ID;
                    //pa.AcceptingNewPatients = true;
                    bppEntities.ProviderAcceptingPatients.Add(pa);
                    Audit.ProviderAcceptingPatients(pa, UserName);
                }
                bppEntities.SaveChanges();
            }
            catch (Exception ee)
            {

            }
        }
    }
}
