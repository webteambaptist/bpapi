﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    [RoutePrefix("api/returningmember")]
    public class ReturningMemberController : ApiController
    {

        [HttpGet]
        [Route("CHECKTINEMAIL")]
        //[AllowCrosssiteJson]
        public IHttpActionResult checkTINEmail()
        {
            var headers = Request.Headers;
            string TIN = null;
            string email = null;
            string selection = null;
            string newguid = null;
            if (headers.Contains("TaxID"))
            {
                TIN = headers.GetValues("TaxID").First();
            }
            if (headers.Contains("Email"))
            {
                email = headers.GetValues("Email").First();
            }
            if (headers.Contains("Selection"))
            {
                selection = headers.GetValues("Selection").First();
            }

            var bppEntities = new BPPEntities();
            var practices = bppEntities.Practices;
            var legalEntities = bppEntities.LegalEntities;
            bool isValid = false;

            if (String.IsNullOrEmpty(TIN))
            {
                return BadRequest("Must Supply TIN");
            }

            if (String.IsNullOrEmpty(email))
            {
                return BadRequest("Must Supply Email");
            }

            //var le = legalEntities.Where(x => x.TaxID == TIN && x.POCEmail.ToUpper() == email.ToUpper()).FirstOrDefault();
            var le = legalEntities.Where(x => x.TaxID == TIN).FirstOrDefault();
            if (le != null)
            {
                if (practices.Any(o => o.LegalEntityID == le.LegalEntityID))
                {
                    string selectionOption = selection.ToString();
                    var selection_id = 0;
                    if (selectionOption == "Add Practice")
                    {
                        selection_id = 3;
                    }
                    else if (selectionOption == "Add Location")
                    {
                        selection_id = 4;
                    }
                    else if (selectionOption == "Add Provider")
                    {
                        selection_id = 5;
                    }
                    try
                    {
                        var _returningMember = new ReturningMemberApplication
                        {
                            GUID = Guid.NewGuid(),
                            LE_GUID = le.LegalEntityID,
                            TIN = TIN,
                            Email = email,
                            AppStep = selection_id,
                            CreateDt = DateTime.Now
                        };
                        bppEntities.ReturningMemberApplications.Add(_returningMember);
                        bppEntities.SaveChanges();
                        var GUID_key = bppEntities.ReturningMemberApplications.Where(x => x.GUID == _returningMember.GUID).Select(x => x.GUID).SingleOrDefault();
                        newguid = GUID_key.ToString();
                    }
                    catch (Exception e)
                    {

                    }
                    isValid = true;
                    //Send Email
                    //var name = le.POCName.ToString();
                    //var recipient = le.POCEmail.ToString();
                    var recipient = email.ToString();
                    //var guid = le.LegalEntityID.ToString();
                    string encrypted = Crypto.Encrypt(newguid.ToString());

                    var hostname = ConfigurationManager.AppSettings["ALC"];
                    Message m = new Message();
                    m.Sender = ConfigurationManager.AppSettings["sender"].ToString();
                    m.Recipient = recipient;
                    var body = bppEntities.Emails.Where(x => x.ID == 11).First();
                    m.Body = body.HTML + "<p><h2><a href=" + hostname + "/Application/GetAppStep?ActivationKey=" + encrypted + "&SelectionOption=" + selection_id + "> Apply Online!</a></h2></p>";
                    m.Subject = body.Title;
                    new MessageController().sendMessage(m);   
                }
            }
            return Ok(isValid);
        }
    }
}