﻿using BPPAPI.Helpers;
using BPPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BPPAPI.Controllers
{
    /// <summary>
    /// Controller for Provider Data
    /// </summary>
    [RoutePrefix("api/provider")]
    public class ProviderController : ApiController
    {
        [Route("RemoveProviderEndDate")]
        [HttpGet]
        public IHttpActionResult RemoveProviderEndDate()
        {
            string ID = "";
            string PracticeID = "";
            string MatrixID = "";
            var headers = Request.Headers;
            if (headers.Contains("ID"))
            {
                ID = headers.GetValues("ID").First();
            }
            if (headers.Contains("PracticeID"))
            {
                PracticeID = headers.GetValues("PracticeID").First();
            }
            if (headers.Contains("MatrixID"))
            {
                MatrixID = headers.GetValues("MatrixID").First();
            }
            int _ID = Convert.ToInt32(ID);
            int _PracticeID = Convert.ToInt32(PracticeID);
            int matrixID = int.Parse(MatrixID);
            var bppEntities = new BPPEntities();
            try
            {
                var NPINumber = bppEntities.Providers.Where(x => x.ID == _ID).Select(x => x.NPINumber).FirstOrDefault();
                var PracticeGUID = bppEntities.Practices.Where(x => x.ID == _PracticeID).Select(x => x.PracticeGUID).FirstOrDefault();
                var providerPracticeMatrix = bppEntities.ProviderPracticeMatrices.Where(p=> p.ID == matrixID).FirstOrDefault();

                //Set End-Date to NULL
                providerPracticeMatrix.EndDate = null;

                //Update Provider Member Status to be Active
                var providerpracticeworkflow = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == NPINumber && x.PracticeGUID == PracticeGUID && x.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                providerpracticeworkflow.ProviderStatusID = bppEntities.ProviderMemberStatus.Where(i => i.MemberStatus == "Active").Select(i => i.StatusID).FirstOrDefault();
                bppEntities.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok("Success");
        }
  
        /// <summary>
        /// Get all provider data by ID
        /// </summary>
        /// <returns></returns>
        [Route("SELECTPROVIDER")]
        [HttpGet]
        public IHttpActionResult selProviderByID()
        {
            string ID = "";
            string PracticeID = "";
            string MatrixID = "";
            // string link = "";
            // string GUID = "";

            var headers = Request.Headers;

            //if (headers.Contains("ActivationKey"))
            //{
            //    link = headers.GetValues("ActivationKey").First();
            //}

            if (headers.Contains("ID"))
            {
                ID = headers.GetValues("ID").First();
            }
            if (headers.Contains("PracticeID"))
            {
                PracticeID = headers.GetValues("PracticeID").First();
            }
            if (headers.Contains("MatrixID"))
            {
                MatrixID = headers.GetValues("MatrixID").First();
            }
            var bppEntities = new BPPEntities();
            var results = new PracticeDetail();
            var otherlocations = bppEntities.ProviderLocations;
            var practices = bppEntities.Practices;
            var locations = bppEntities.Locations;
            var providers = bppEntities.Providers;
           // var providermemberstatusmatrix = bppEntities.ProviderMemberStatusMatrices;
            var provider = new Provider();
            var practice = new Practice();
            results._Providers = new List<ProviderFull>();
            results._Facilities = new List<Facility>();
            results._AcceptingNewPatientContracts = new List<Contract>();

            try
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    int _ID = 0;
                    int _PracticeID = 0;
                    try
                    {
                        _ID = int.Parse(ID);
                        _PracticeID = int.Parse(PracticeID);
                    }
                    catch (Exception ee)
                    {
                        _ID = 0;
                        _PracticeID = 0;
                    }

                    provider = bppEntities.Providers.Where(x => x.ID == _ID).FirstOrDefault();
                    int matrixID = int.Parse(MatrixID);
                    _PracticeID = int.Parse(PracticeID);
                    practice = bppEntities.Practices.Where(x => x.ID == _PracticeID).FirstOrDefault();
                    var matrix = bppEntities.ProviderPracticeMatrices.Where(x => x.NPINumber == provider.NPINumber && x.PracticeGUID == practice.PracticeGUID && x.ID == matrixID).FirstOrDefault();
                    
                    var npi = provider.NPINumber;
                    //from p in providers where p.ID == _ID select p).FirstOrDefault();
                    ProviderFull pf = new ProviderFull();
                    
                    var ancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == provider.NPINumber && x.PracticeGuid == practice.PracticeGUID && x.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                    //pf.Based = provider.Based;
                    if (ancillary != null)
                    {
                        ProviderAncillary a = new ProviderAncillary();
                        a.EmploymentStatus = ancillary.EmploymentStatus;
                        a.SpecialtyType = ancillary.SpecialtyType;
                        a.FacilitySetting = ancillary.FacilitySetting;
                        a.ServicePopulation = ancillary.ServicePopulation;
                        a.Maiden_Name = ancillary.Maiden_Name;
                        a.AKA = ancillary.AKA;
                        a.ProviderPracticeMatrixID = matrixID;
                        pf.ProviderAncillary = a;
                    }
                    //pf.CareType = provider.CareType;
                    pf.CMS = provider.CMS;
                    pf.CreatedDT = provider.CreatedDT;
                    pf.DateOfBirth = provider.DateOfBirth;
                   // pf.DateOfBoardExam = provider.DateOfBoardExam;
                    var degree = bppEntities.DegreeTypes.Where(x => x.DegreeID == provider.DegreeID).FirstOrDefault();
                    pf.Degree = degree.Degree;
                    pf.Email = provider.Email;
                    pf.Facilties = bppEntities.Facilities.ToList();
                    pf.Contracts = bppEntities.Contracts.ToList();
                    pf.FirstName = provider.FirstName; 
                    
                    pf.GUID = provider.GUID;
                    //pf.ProviderType = provider.ProviderType;
                    pf.ID = provider.ID;
                    pf.isBHPrivileged = provider.isBHPrivileged;
                  //  pf.isBoardCert = provider.isBoardCert;
                    pf.LastName = provider.LastName;
                    pf.Suffix = provider.Suffix;
                    var pLocations = bppEntities.ProviderLocations.Where(e => e.NPINumber == npi && e.PracticeGUID == practice.PracticeGUID && e.ProviderPracticeMatrixID == matrixID).ToList();
                    pf.Locations = pLocations;
                    pf.MiddleName = provider.MiddleName;
                    pf.ModifiedDT = provider.ModifiedDT;
                    pf.NPINumber = provider.NPINumber;
                    pf.PhoneNumber = provider.PhoneNumber;
                    pf.EndDate = matrix.EndDate;
// pf.EndDate = bppEntities.ProviderPracticeMatrices.Where(i => i.NPINumber == npi && i.PracticeGUID == practice.PracticeGUID).Select(i => i.EndDate).FirstOrDefault();



                    var specialties = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == provider.NPINumber && e.PracticeGUID == practice.PracticeGUID && e.ProviderPracticeMatrixID == matrixID).ToList();
                    foreach (var spec in specialties)
                    {
                        var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.SpecialtyID == spec.SpecialtyID).FirstOrDefault();
                        if (spec.IsPrmary == true)
                        {
                            pf.PrimarySpecialty = specLookup.Specialty;
                        }
                        else
                        {
                            pf.SecondarySpecialty = specLookup.Specialty;
                        }
                    }
                    pf.SpecialtyNotes = provider.SpecialtyNotes;
                    pf.Specialties = bppEntities.SpecialtyMatrices.ToList();
                    pf.Suffix = provider.Suffix;

                    var legalEntity = bppEntities.LegalEntities.Where(x => x.LegalEntityID == practice.LegalEntityID).First();

                    pf.TaxID = legalEntity.TaxID;
                    results._Entity = legalEntity;
                    var prPracticeWF = bppEntities.ProviderPracticeWorkflows.Where(e => e.NPINumber == provider.NPINumber && e.PracticeGUID == practice.PracticeGUID && e.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                    //pf.ProvidersMemberStatus = bppEntities.ProviderMemberStatus.Where(x=>x.StatusID == prPracticeWF.ProviderStatusID)
                    //    .Select(x=>x.MemberStatus).FirstOrDefault();

                    if (practice != null)
                    {
                        PracticeExt ex = new PracticeExt();
                        ex.CreatedDT = practice.CreatedDT;
                        ex.PracticeGUID = practice.PracticeGUID.ToString();
                        ex.ID = practice.ID;
                        ex.LegalEntityID = practice.LegalEntityID.ToString();
                        ex.ModifiedDT = practice.ModifiedDT;
                        ex.PracticeApprovalStatus = practice.PracticeApprovalStatus;
                        var Location = bppEntities.Locations.Where(p => p.PracticeGUID == practice.PracticeGUID && p.isRemoved == false).OrderBy(x=> x.LocationName).ToList();
                        ex.PracticeLocations = new List<PracticeLocations>();
                        foreach (var _locations in Location)
                        {
                            PracticeLocations p = new PracticeLocations();
                            p.Address1 = _locations.Address1;
                            p.Address2 = _locations.Address2;
                            p.BillingManagerEmail = _locations.BillingManagerEmail;
                            p.BillingManagerExtension = _locations.BillingManagerExtension;
                            p.BillingManagerName = _locations.BillingManagerName;
                            p.BillingManagerPhoneNumber = _locations.BillingManagerPhoneNumber;
                            p.City = _locations.City;
                            p.CreatedDT = _locations.CreatedDT;
                            p.EMRVendor = _locations.EMRVendor;
                            p.EMRVersion = _locations.EMRVersion;
                            p.Extension = _locations.Extension;
                            p.Fax = _locations.Fax;
                            p.ID = _locations.ID;
                            p.IsPrimary = _locations.IsPrimary;
                           // p.Lat = _locations.Lat;
                          //  p.Lng = _locations.Lng;
                            p.LocationGUID = _locations.LocationGUID;
                            p.LocationName = _locations.LocationName;
                            p.ModifiedDT = _locations.ModifiedDT;
                            p.OfficeManagerEmail = _locations.OfficeManagerEmail;
                            p.OfficeManagerExtension = _locations.OfficeManagerExtension;
                            p.OfficeManagerName = _locations.OfficeManagerName;
                            p.OfficeManagerPhoneNumber = _locations.OfficeManagerPhoneNumber;
                            p.PhoneNumber = _locations.PhoneNumber;
                            p.PracticeGUID = _locations.PracticeGUID;
                            p.State = _locations.State;
                            p.Zip = _locations.Zip;
                            p.isRemoved = _locations.isRemoved;
                            ex.PracticeLocations.Add(p);
                        }
                       // ex.PracticeLogoReceived = practice.PracticeLogoReceived;
                        ex.PracticeName = practice.PracticeName;
                        results._Practices = new List<PracticeExt>();
                        results._Practices.Add(ex);
                        if (prPracticeWF != null)
                        {
                            pf.PrimaryPracticeLocation = prPracticeWF.PrimaryPracticeLocation;
                            pf.ProviderPracticeWorkFlow = prPracticeWF;
                        }
                    }

                    // var memStatusMatrix = bppEntities.ProviderMemberStatusMatrices
                    //     .Where(e => e.NPINumber == provider.NPINumber && e.PracticeGUID == practice.PracticeGUID).FirstOrDefault();
                    
                    pf.MemberStatus = bppEntities.ProviderMemberStatus
                        .Where(e => e.StatusID == prPracticeWF.ProviderStatusID).FirstOrDefault();
                        //memStatusMatrix.ProviderMemberStatus).FirstOrDefault();

                    pf.ProviderMemberStatus = pf.MemberStatus.StatusID;

                    //pf.StatusChangeReasonID = bppEntities.ProviderMemberStatusMatrices
                    //    .Where(e => e.NPINumber == provider.NPINumber && e.PracticeGUID == practice.PracticeGUID)
                    //    .Select(e => e.InactiveReasonStatus).FirstOrDefault();

                    pf.StatusChangeReason = bppEntities.InactiveReasonStatus
                        .Where(x => x.ID == pf.StatusChangeReasonID).Select(x => x.StatusName).FirstOrDefault();

                    //pf.OtherReason = bppEntities.ProviderMemberStatusMatrices
                    //    .Where(e => e.NPINumber == provider.NPINumber && e.PracticeGUID == practice.PracticeGUID).Select(e => e.OtherReason).FirstOrDefault();


                    var locs = otherlocations.Where(x => x.NPINumber == provider.NPINumber && x.PracticeGUID == practice.PracticeGUID && x.ProviderPracticeMatrixID == matrixID);
                    pf.OtherPracticeLocations = new List<int>();
                    pf.ProviderFacilities = new List<int>();
                    pf.ProviderAcceptingPatients = new List<int>();

                    var facs = bppEntities.ProviderFacilities.Where(x => x.NPINumber == provider.NPINumber && x.PracticeGUID == practice.PracticeGUID && x.ProviderPracticeMatrixID == matrixID);

                    if (facs.Count<ProviderFacility>() > 0)
                    {
                        foreach (var f in facs)
                        {
                            pf.ProviderFacilities.Add(f.FacilityID);
                        }
                    }

                    if (locs.Count<ProviderLocation>() > 0)
                    {
                        foreach (var l in locs)
                        {
                            pf.OtherPracticeLocations.Add(l.LocationID);
                        }
                    }
                    else
                    {
                        pf.OtherPracticeLocations = null;
                    }

                    var paps = bppEntities.ProviderAcceptingPatients.Where(x => x.NPINumber == provider.NPINumber && x.PracticeGUID == practice.PracticeGUID && x.ProviderPracticeMatrixID == matrixID);

                    if (paps.Count<ProviderAcceptingPatient>() > 0)
                    {
                        foreach (var p in paps)
                        {
                            pf.ProviderAcceptingPatients.Add(p.ContractID);
                        }
                    }

                    results._MsoDD = bppEntities.MSOVerificationStatus.OrderBy(e => e.StatusName).ToList();
                    results._MqRecommendDD = bppEntities.MQRecommendationStatus.OrderBy(e => e.StatusName).ToList();
                    results.BoardOutcomeDD = bppEntities.BoardOutcomeStatus.OrderBy(e => e.StatusName).ToList();
                    results.DenialReasonDD = bppEntities.DenialReasonStatus.OrderBy(e => e.StatusName).ToList();
                    results.ProviderMemberStatusDD = bppEntities.ProviderMemberStatus.OrderBy(e => e.StatusID).ThenBy(e => e.MemberStatus).ToList();
                    results.InactiveReasonStatusDD = bppEntities.InactiveReasonStatus.OrderBy(e => e.StatusName).ToList();
                    results._Providers.Add(pf);

                    results._Facilities = bppEntities.Facilities.ToList();
                    results._AcceptingNewPatientContracts = bppEntities.Contracts.ToList();

                    //var ppw = bppEntities.ProviderPracticeWorkflows.Where(p => p.NPINumber == npi && p.PracticeGUID == practice.PracticeGUID && p.ProviderPracticeMatrixID == matrixID).FirstOrDefault();
                    var providerstatus = prPracticeWF.ProviderStatusID;
                    pf.ProvidersMemberStatus = bppEntities.ProviderMemberStatus.Where(i => i.StatusID == providerstatus).Select(i => i.MemberStatus).FirstOrDefault();

                    var msoverification = prPracticeWF.MSOVerificationStatusID;
                    pf.MSOVerification = bppEntities.MSOVerificationStatus.Where(i => i.ID == msoverification).Select(i => i.StatusName).FirstOrDefault();

                    var mqrecommendation = prPracticeWF.MQRecommendationStatusID;
                    pf.MQRecommendation = bppEntities.MQRecommendationStatus.Where(i => i.ID == mqrecommendation).Select(i => i.StatusName).FirstOrDefault();

                    var boardoutcome = prPracticeWF.BoardOutcomeStatusID;
                    pf.BoardOutcomeStatus = bppEntities.BoardOutcomeStatus.Where(i => i.ID == boardoutcome).Select(i => i.StatusName).FirstOrDefault();

                    var denialreason = prPracticeWF.DenialReasonStatusID;
                    pf.DenialReason = bppEntities.DenialReasonStatus.Where(i => i.ID == denialreason).Select(i => i.StatusName).FirstOrDefault();

                    var inactivereason = prPracticeWF.InactiveReasonStatusID;
                    pf.InactiveReason = bppEntities.InactiveReasonStatus.Where(i => i.ID == inactivereason).Select(i => i.StatusName).FirstOrDefault();


                    bppEntities.SaveChanges();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ee)
            {
                return BadRequest(ee.Message);
            }
            return Json(results);
        }

        /// <summary>
        /// Update Provider
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [Route("UPDATEPROVIDER")]
        [HttpPost]
        //public IHttpActionResult updProvider(Provider p)
        public IHttpActionResult updProvider(ProviderFull p)
        {
            var bppEntities = new BPPEntities();
            try
            {
                string UpdateUser = "";
                var headers = Request.Headers;
                if (headers.Contains("UserName"))
                {
                    UpdateUser = headers.GetValues("UserName").First();
                }
                var degreeID = bppEntities.DegreeTypes.Where(x => x.Degree == p.Degree).FirstOrDefault();
                var provider = bppEntities.Providers.Where(x => x.NPINumber == p.NPINumber).FirstOrDefault();
                Audit.ProvidersAudit(provider, UpdateUser);
                provider.FirstName = p.FirstName;
                provider.MiddleName = p.MiddleName;
                provider.LastName = p.LastName;
                provider.Suffix = p.Suffix;
               // provider.AlsoKnownAs = p.AlsoKnownAs;
               // provider.Based = p.Based;
                provider.DateOfBirth = p.DateOfBirth;
                provider.PhoneNumber = p.PhoneNumber;
                provider.Email = p.Email;
              //  provider.ResidencyCompletionDate = p.ResidencyCompletionDate;
              //  provider.DateOfBoardExam = p.DateOfBoardExam;
                provider.DegreeID = degreeID.DegreeID;
                provider.isBHPrivileged = p.isBHPrivileged;
                provider.SpecialtyNotes = p.SpecialtyNotes;
                provider.Suffix = p.Suffix;
               // provider.CareType = p.CareType;
               // provider.Based = p.Based;
               // provider.ProviderType = p.ProviderType;
                provider.CMS = p.CMS;

                // see if you get Workflow data
                bppEntities.SaveChanges();

                var ancillary = bppEntities.ProviderAncillaries.Where(x => x.NPINumber == p.NPINumber && x.PracticeGuid == p.ProviderPracticeWorkFlow.PracticeGUID && x.ProviderPracticeMatrixID == p.matrix.ID).FirstOrDefault();
                
                if (ancillary != null)
                {
                    ancillary.Maiden_Name = p.ProviderAncillary.Maiden_Name;
                    ancillary.AKA = p.ProviderAncillary.AKA;
                    ancillary.EmploymentStatus = p.ProviderAncillary.EmploymentStatus;
                    ancillary.ServicePopulation = p.ProviderAncillary.ServicePopulation;
                    ancillary.SpecialtyType = p.ProviderAncillary.SpecialtyType;
                    ancillary.FacilitySetting = p.ProviderAncillary.FacilitySetting;
                    ancillary.ProviderPracticeMatrixID = p.matrix.ID;
                    bppEntities.SaveChanges();
                    Audit.ProviderAncillaryAudit(ancillary, UpdateUser);
                }
                else
                {
                    ProviderAncillary pa = new ProviderAncillary();
                    pa.AKA = p.ProviderAncillary.AKA;
                    pa.CreatedDT = DateTime.Now;
                    pa.EmploymentStatus = p.ProviderAncillary.EmploymentStatus;
                    pa.Maiden_Name = p.ProviderAncillary.Maiden_Name;
                    pa.NPINumber = p.NPINumber;
                    pa.PracticeGuid = p.ProviderPracticeWorkFlow.PracticeGUID;
                    pa.ServicePopulation = p.ProviderAncillary.ServicePopulation;
                    pa.SpecialtyType = p.ProviderAncillary.SpecialtyType;
                    pa.FacilitySetting = p.ProviderAncillary.FacilitySetting;
                    pa.ProviderPracticeMatrixID = p.matrix.ID;
                    bppEntities.ProviderAncillaries.Add(pa);
                    bppEntities.SaveChanges();
                    Audit.ProviderAncillaryAudit(pa, UpdateUser);
                }

                var wf = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == p.NPINumber && x.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID && x.ProviderPracticeMatrixID == p.matrix.ID).FirstOrDefault();
                
                wf.PrimaryPracticeLocation = p.PrimaryPracticeLocation;
                wf.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                wf.Outcome = p.ProviderPracticeWorkFlow.Outcome;
                wf.EligibilityCertDate = p.ProviderPracticeWorkFlow.EligibilityCertDate;
                //wf.DateSignedMemberApp = p.ProviderPracticeWorkFlow.DateSignedMemberApp;
                wf.DateSignedJoinder = p.ProviderPracticeWorkFlow.DateSignedJoinder;
                wf.DateJoinderReceived = p.ProviderPracticeWorkFlow.DateJoinderReceived;
                wf.DatePresentedMQProcess = p.ProviderPracticeWorkFlow.DatePresentedMQProcess;
                wf.DatePresentedReady = p.ProviderPracticeWorkFlow.DatePresentedReady;
                wf.BPPEffectiveDate = p.ProviderPracticeWorkFlow.BPPEffectiveDate;
                wf.DateNotifcationOutcome = p.ProviderPracticeWorkFlow.DateNotifcationOutcome;
                wf.DateRecommendationSent = p.ProviderPracticeWorkFlow.DateRecommendationSent;
                wf.BoardConsiderationDate = p.ProviderPracticeWorkFlow.BoardConsiderationDate;
                wf.OutcomeNotes = p.ProviderPracticeWorkFlow.OutcomeNotes;
                wf.NotificationDate = p.ProviderPracticeWorkFlow.NotificationDate;

                var msoverification = bppEntities.MSOVerificationStatus.Where(i => i.StatusName == p.MSOVerification).Select(i => i.ID).FirstOrDefault();

                var mqrecommendation = bppEntities.MQRecommendationStatus.Where(i => i.StatusName == p.MQRecommendation).Select(i => i.ID).FirstOrDefault();

                var boardoutcome = bppEntities.BoardOutcomeStatus.Where(i => i.StatusName == p.BoardOutcomeStatus).Select(i => i.ID).FirstOrDefault();

                var denialreason = bppEntities.DenialReasonStatus.Where(i => i.StatusName == p.DenialReason).Select(i => i.ID).FirstOrDefault();

                var providerstatus = bppEntities.ProviderMemberStatus.Where(i => i.MemberStatus == p.ProvidersMemberStatus).Select(i => i.StatusID).FirstOrDefault();

                var inactivereason = bppEntities.InactiveReasonStatus.Where(i => i.StatusName == p.InactiveReason).Select(i => i.ID).FirstOrDefault();

                wf.MSOVerificationStatusID = msoverification;
                wf.MQRecommendationStatusID = mqrecommendation;
                wf.BoardOutcomeStatusID = boardoutcome;
                wf.DenialReasonStatusID = denialreason;
                wf.ProviderStatusID = providerstatus;
                wf.InactiveReasonStatusID = inactivereason;
                wf.ProviderInactiveDate = p.ProviderPracticeWorkFlow.ProviderInactiveDate;
                bppEntities.SaveChanges();
                Audit.ProviderPracticeWorkflowAudit(wf, UpdateUser);
                //Update ProviderMemberStatusMatrix table
                // var pmsm = bppEntities.ProviderMemberStatusMatrices.Where(x => x.NPINumber == p.NPINumber && x.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID).FirstOrDefault();
                // pmsm.ProviderMemberStatus = providerstatus;
                //  pmsm.InactiveReasonStatus = inactivereason;

                //  bppEntities.SaveChanges();

                var ppm = bppEntities.ProviderPracticeMatrices.Where(x => x.ID == p.matrix.ID).FirstOrDefault();
                
                if (ppm != null)
                {
                    ppm.StartDate = p.ProviderPracticeWorkFlow.BPPEffectiveDate;
                    ppm.EndDate = p.ProviderPracticeWorkFlow.ProviderInactiveDate;
                    bppEntities.SaveChanges();
                    Audit.ProviderPracticeMatrixAudit(ppm, UpdateUser);

                    //Get Legal Entity ID from Practices table based on the Practice Guid 
                    var _legalEntityID = bppEntities.Practices.Where(pract => pract.PracticeGUID == ppm.PracticeGUID).Select(pract => pract.LegalEntityID).FirstOrDefault();

                    //Get All Practices associated with returned Legal Entity ID

                    var query = from practices in bppEntities.Practices 
                                join matrix in bppEntities.ProviderPracticeMatrices on practices.PracticeGUID equals matrix.PracticeGUID
                                join workflow in bppEntities.ProviderPracticeWorkflows on matrix.NPINumber equals workflow.NPINumber
                                join memberstatus in bppEntities.ProviderMemberStatus on workflow.ProviderStatusID equals memberstatus.StatusID
                                where practices.LegalEntityID == _legalEntityID && matrix.EndDate == null && workflow.PracticeGUID == practices.PracticeGUID
                                select new { Status = memberstatus.MemberStatus };

                    bool isActive = query.Any(x => x.Status == "Active");

                    if (!isActive)
                    {
                        //Update Legal Entity Status to Inactive
                        var legalEntity = bppEntities.LegalEntities.Where(le => le.LegalEntityID == _legalEntityID).FirstOrDefault();
                        legalEntity.Status = "Inactive";
                        bppEntities.SaveChanges();
                    }
                    else
                    {
                        var legalEntity = bppEntities.LegalEntities.Where(le => le.LegalEntityID == _legalEntityID).FirstOrDefault();
                        legalEntity.Status = "Active";
                        bppEntities.SaveChanges();
                    }
                }

                try
                {
                    // delete all first
                    var primary = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == p.NPINumber && e.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID && e.IsPrmary == true && e.ProviderPracticeMatrixID == p.matrix.ID).FirstOrDefault();
                    
                    var secondary = bppEntities.ProviderSpecialtyMatrices.Where(e => e.NPINumber == p.NPINumber && e.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID && e.IsPrmary == false && e.ProviderPracticeMatrixID == p.matrix.ID).FirstOrDefault();
                    
                    if (primary != null)
                    {
                        bppEntities.ProviderSpecialtyMatrices.Remove(primary);
                    }
                    if (secondary != null)
                    {
                        bppEntities.ProviderSpecialtyMatrices.Remove(secondary);
                    }
                    bppEntities.SaveChanges();
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
                try
                {
                    // Insert Primary
                    try
                    {
                        if (p.SecondarySpecialty != null)
                        {
                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.Specialty.ToUpper() == p.SecondarySpecialty.ToUpper()).FirstOrDefault();

                            ProviderSpecialtyMatrix psm = new ProviderSpecialtyMatrix();
                            psm.IsPrmary = false;
                            psm.NPINumber = p.NPINumber;
                            psm.SpecialtyID = (int)specLookup.SpecialtyID;
                            psm.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                            psm.ProviderPracticeMatrixID = p.matrix.ID;
                            bppEntities.ProviderSpecialtyMatrices.Add(psm);
                            bppEntities.SaveChanges();
                            Audit.ProviderSpecialtyMatrixAudit(psm, UpdateUser);
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                    // Insert secondary
                    try
                    {
                        if (p.PrimarySpecialty != null)
                        {
                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.Specialty.ToUpper() == p.PrimarySpecialty.ToUpper()).FirstOrDefault();

                            ProviderSpecialtyMatrix psm = new ProviderSpecialtyMatrix();
                            psm.IsPrmary = true;
                            psm.NPINumber = p.NPINumber;
                            psm.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                            psm.SpecialtyID = (int)specLookup.SpecialtyID;
                            psm.ProviderPracticeMatrixID = p.matrix.ID;
                            bppEntities.ProviderSpecialtyMatrices.Add(psm);
                            bppEntities.SaveChanges();
                            Audit.ProviderSpecialtyMatrixAudit(psm, UpdateUser);
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }


                try
                {
                    // get Practice
                    var primaryProvLocation = bppEntities.ProviderLocations
                        .Where(e => e.NPINumber == p.NPINumber && e.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID && e.IsPrimary == true && e.ProviderPracticeMatrixID == p.matrix.ID).FirstOrDefault();
                    
                    var otherProvLocations = bppEntities.ProviderLocations
                        .Where(e => e.NPINumber == p.NPINumber && e.PracticeGUID == p.ProviderPracticeWorkFlow.PracticeGUID && e.IsPrimary == false && e.ProviderPracticeMatrixID == p.matrix.ID).ToList();
                    
                    if (primaryProvLocation != null)
                    {
                        bppEntities.ProviderLocations.Remove(primaryProvLocation);
                    }
                    if (otherProvLocations.Count > 0)
                    {
                        foreach (ProviderLocation item in otherProvLocations)
                        {
                            if (otherProvLocations != null)
                            {
                                Audit.ProviderLocationsAudit(item, UpdateUser);
                                bppEntities.ProviderLocations.Remove(item);
                            }
                        }
                    }
                    bppEntities.SaveChanges();
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
                try
                {
                    try
                    {
                        if (p.PrimaryPracticeLocation != null)
                        {
                            int locationID = int.Parse(p.PrimaryPracticeLocation);
                            ProviderLocation plp = new ProviderLocation()
                            {
                                LocationID = locationID,
                                NPINumber = p.NPINumber,
                                IsPrimary = true,
                                ProviderPracticeMatrixID = p.matrix.ID,
                                PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID
                            };
                            bppEntities.ProviderLocations.Add(plp);
                            bppEntities.SaveChanges();
                            Audit.ProviderLocationsAudit(plp, UpdateUser);
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                    try
                    {
                        if (p.OtherPracticeLocations!=null && p.OtherPracticeLocations.Count > 0)
                        {
                            foreach (int other in p.OtherPracticeLocations)
                            {
                                if (p.OtherPracticeLocations != null)
                                {
                                    ProviderLocation plp = new ProviderLocation()
                                    {
                                        LocationID = other,
                                        NPINumber = p.NPINumber,
                                        IsPrimary = false,
                                        ProviderPracticeMatrixID = p.matrix.ID,
                                        PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID
                                    };
                                    bppEntities.ProviderLocations.Add(plp);
                                    bppEntities.SaveChanges();
                                    Audit.ProviderLocationsAudit(plp, UpdateUser);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }

               // bool isupdate = new ApplicationListController().updateMatrix(p);

                new ApplicationListController().updateFacMatrix(p, UpdateUser);

                //if (!isupdate)
               // {
               //     return BadRequest();
               // }

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok("");
        }

        /// <summary>
        /// This just marks a provider as isDeleted = true
        /// </summary>
        /// <param name="providerAppData"></param>
        [HttpPost]
        [Route("DELETEPROVIDER")]
        public IHttpActionResult deleteProvider([FromBody]ProviderAppData providerAppData)
        {
            try
            {
                if (providerAppData.Providers != null)
                {
                    foreach (var provs in providerAppData.Providers)
                    {
                        using (var db = new BPPEntities())
                        {
                            Provider p = new Provider();
                            var provider = db.Providers.Where(e => e.ID == provs.ID).FirstOrDefault();

                            if (provider != null)
                            {
                                provider.isDeleted = true;
                            }
                            db.SaveChanges();

                            //ProviderMemberStatusMatrix mx = new ProviderMemberStatusMatrix();
                            //var memberStatus = db.ProviderMemberStatusMatrices.Where(e => e.NPINumber == provs.NPINumber).FirstOrDefault();

                           // if (memberStatus != null)
                           // {
                          //      memberStatus.ProviderMemberStatus = db.ProviderMemberStatus.Where(e => e.MemberStatus == "Deleted").Select(e => e.StatusID).FirstOrDefault();
                          //  }
                          //  db.SaveChanges();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

            return Ok("Success");
        }

        /// <summary>
        /// Clone Provider Profile
        /// </summary>
        /// <param name="p"></param>
        [Route("CloneProvider")]
        [HttpPost]
        public IHttpActionResult CloneProvider(ProviderFull p)
        {
            var bppEntities = new BPPEntities();
            try
            {
                string UpdateUser = "";
                var headers = Request.Headers;
                if (headers.Contains("UserName"))
                {
                    UpdateUser = headers.GetValues("UserName").First();
                }
                //Insert into Provider Practice Matrix Table
                ProviderPracticeMatrix ppm = new ProviderPracticeMatrix();
                ppm.NPINumber = p.NPINumber;
                ppm.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                ppm.StartDate = p.ProviderPracticeWorkFlow.BPPEffectiveDate;
                ppm.EndDate = p.ProviderPracticeWorkFlow.ProviderInactiveDate;
                bppEntities.ProviderPracticeMatrices.Add(ppm);
                bppEntities.SaveChanges();
                var matrixID = ppm.ID;

                //Insert into Provider Facilities
                foreach (var pl in p.ProviderFacilities)
                {
                    ProviderFacility pf = new ProviderFacility();
                    pf.NPINumber = p.NPINumber;
                    pf.FacilityID = pl;
                    pf.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                    pf.ProviderPracticeMatrixID = matrixID;
                    bppEntities.ProviderFacilities.Add(pf);
                }
                bppEntities.SaveChanges();

                //Insert into Provider Ancillary Table
                ProviderAncillary pa = new ProviderAncillary();
                pa.ProviderPracticeMatrixID = matrixID;
                    pa.AKA = p.ProviderAncillary.AKA;
                    pa.CreatedDT = DateTime.Now;
                    pa.EmploymentStatus = p.ProviderAncillary.EmploymentStatus;
                    pa.Maiden_Name = p.ProviderAncillary.Maiden_Name;
                    pa.NPINumber = p.NPINumber;
                    pa.PracticeGuid = p.ProviderPracticeWorkFlow.PracticeGUID;
                    pa.ServicePopulation = p.ProviderAncillary.ServicePopulation;
                    pa.SpecialtyType = p.ProviderAncillary.SpecialtyType;                   
                    bppEntities.ProviderAncillaries.Add(pa);
                    bppEntities.SaveChanges();
             
                //Insert into Provider Practice Workflow Table
                ProviderPracticeWorkflow ppw = new ProviderPracticeWorkflow();
                ppw.ProviderPracticeMatrixID = matrixID;
                ppw.NPINumber = p.NPINumber;
                ppw.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                ppw.PrimaryPracticeLocation = p.PrimaryPracticeLocation;
                ppw.Outcome = p.ProviderPracticeWorkFlow.Outcome;
                ppw.EligibilityCertDate = p.ProviderPracticeWorkFlow.EligibilityCertDate;
                ppw.DateSignedMemberApp = p.ProviderPracticeWorkFlow.DateSignedMemberApp;
                ppw.DateSignedJoinder = p.ProviderPracticeWorkFlow.DateSignedJoinder;
                ppw.DateJoinderReceived = p.ProviderPracticeWorkFlow.DateJoinderReceived;
                ppw.DatePresentedMQProcess = p.ProviderPracticeWorkFlow.DatePresentedMQProcess;
                ppw.DatePresentedReady = p.ProviderPracticeWorkFlow.DatePresentedReady;
                ppw.MedStaffPrivileges = p.ProviderPracticeWorkFlow.MedStaffPrivileges;
                ppw.BPPEffectiveDate = p.ProviderPracticeWorkFlow.BPPEffectiveDate;
                ppw.DateNotifcationOutcome = p.ProviderPracticeWorkFlow.DateNotifcationOutcome;
                ppw.DateRecommendationSent = p.ProviderPracticeWorkFlow.DateRecommendationSent;
                ppw.BoardConsiderationDate = p.ProviderPracticeWorkFlow.BoardConsiderationDate;
                ppw.OutcomeNotes = "Provider Profile has been cloned.";
                ppw.Witness = p.ProviderPracticeWorkFlow.Witness;

                ppw.NotificationDate = p.ProviderPracticeWorkFlow.NotificationDate;

                var msoverification = bppEntities.MSOVerificationStatus.Where(i => i.StatusName == p.MSOVerification).Select(i => i.ID).FirstOrDefault();

                var mqrecommendation = bppEntities.MQRecommendationStatus.Where(i => i.StatusName == p.MQRecommendation).Select(i => i.ID).FirstOrDefault();

                var boardoutcome = bppEntities.BoardOutcomeStatus.Where(i => i.StatusName == p.BoardOutcomeStatus).Select(i => i.ID).FirstOrDefault();

                var denialreason = bppEntities.DenialReasonStatus.Where(i => i.StatusName == p.DenialReason).Select(i => i.ID).FirstOrDefault();

                var providerstatus = bppEntities.ProviderMemberStatus.Where(i => i.MemberStatus == "Cloned").Select(i => i.StatusID).FirstOrDefault();

                var inactivereason = bppEntities.InactiveReasonStatus.Where(i => i.StatusName == p.InactiveReason).Select(i => i.ID).FirstOrDefault();

                ppw.MSOVerificationStatusID = msoverification;
                ppw.MQRecommendationStatusID = mqrecommendation;
                ppw.BoardOutcomeStatusID = boardoutcome;
                ppw.DenialReasonStatusID = denialreason;
                ppw.ProviderStatusID = providerstatus;
                ppw.InactiveReasonStatusID = inactivereason;
                ppw.ProviderInactiveDate = p.ProviderPracticeWorkFlow.ProviderInactiveDate;
                bppEntities.ProviderPracticeWorkflows.Add(ppw);
               bppEntities.SaveChanges();
              
                //Insert into Provider Specialty Table
                try
                {
                    // Insert Primary
                    try
                    {
                        if (p.SecondarySpecialty != null)
                        {
                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.Specialty.ToUpper() == p.SecondarySpecialty.ToUpper()).FirstOrDefault();

                           ProviderSpecialtyMatrix psm = new ProviderSpecialtyMatrix();
                            psm.IsPrmary = false;
                            psm.NPINumber = p.NPINumber;
                            psm.SpecialtyID = (int)specLookup.SpecialtyID;
                            psm.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                            psm.ProviderPracticeMatrixID = matrixID;
                            bppEntities.ProviderSpecialtyMatrices.Add(psm);
                            bppEntities.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                    // Insert secondary
                    try
                    {
                        if (p.PrimarySpecialty != null)
                        {
                            var specLookup = bppEntities.SpecialtyMatrices.Where(s => s.Specialty.ToUpper() == p.PrimarySpecialty.ToUpper()).FirstOrDefault();

                            ProviderSpecialtyMatrix psm = new ProviderSpecialtyMatrix();
                            psm.IsPrmary = true;
                            psm.NPINumber = p.NPINumber;
                            psm.PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID;
                            psm.SpecialtyID = (int)specLookup.SpecialtyID;
                            psm.ProviderPracticeMatrixID = matrixID;
                            bppEntities.ProviderSpecialtyMatrices.Add(psm);
                            bppEntities.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }

                //Insert into Provider Locations

                try
                {
                    try
                    {
                        if (p.PrimaryPracticeLocation != null)
                        {
                            int locationID = int.Parse(p.PrimaryPracticeLocation);
                            ProviderLocation plp = new ProviderLocation()
                            {
                                LocationID = locationID,
                                NPINumber = p.NPINumber,
                                IsPrimary = true,
                                ProviderPracticeMatrixID = matrixID,
                                PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID
                                
                            };
                            bppEntities.ProviderLocations.Add(plp);
                            bppEntities.SaveChanges();
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                    try
                    {
                        if (p.OtherPracticeLocations != null && p.OtherPracticeLocations.Count > 0)
                        {
                            foreach (int other in p.OtherPracticeLocations)
                            {
                                if (p.OtherPracticeLocations != null)
                                {
                                    ProviderLocation plp = new ProviderLocation()
                                    {
                                        LocationID = other,
                                        NPINumber = p.NPINumber,
                                        IsPrimary = false,
                                        ProviderPracticeMatrixID = matrixID,
                                        PracticeGUID = p.ProviderPracticeWorkFlow.PracticeGUID
                                    };
                                    bppEntities.ProviderLocations.Add(plp);
                                    bppEntities.SaveChanges();
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok("");
        }

        //public void MoveProviderToHistory(string npi, Guid practiceGUID)
        //{
        //    var bppEntities = new BPPEntities();
        //    try
        //    {
        //        // Move all Provider Tables to history
        //        var appworkflow = bppEntities.ProviderApplicationWorkflows.Where(x => x.NPINumber == npi && x.PracticeGUID == practiceGUID).FirstOrDefault();
        //        ProviderApplicationWorkflowHistory wfHistory = new ProviderApplicationWorkflowHistory();
        //        wfHistory.HistoryDt = DateTime.Now;
        //        wfHistory.ID = appworkflow.ID;
        //        wfHistory.CreatedDT = appworkflow.CreatedDT;
        //        wfHistory.LastUpdateDT = appworkflow.LastUpdateDT;
        //        wfHistory.NPINumber = appworkflow.NPINumber;
        //        wfHistory.PracticeGUID = appworkflow.PracticeGUID;
        //        bppEntities.ProviderApplicationWorkflowHistories.Add(wfHistory);
        //        bppEntities.SaveChanges();

        //        // delete from live table
        //        bppEntities.ProviderApplicationWorkflows.Remove(appworkflow);
        //        bppEntities.SaveChanges();
        //    }
        //    catch(Exception e)
        //    {
        //        var whyamihere = e.Message;
        //        var innerwhy = e.InnerException;
        //    }

        //    var facilities = bppEntities.ProviderFacilities.Where(x => x.NPINumber == npi && x.PracticeGUID == practiceGUID).ToList();
        //    foreach (var facility in facilities)
        //    {
        //        ProviderFacilityHistory history = new ProviderFacilityHistory();
        //        history.FacilityID = facility.FacilityID;
        //        history.HistoryDt = DateTime.Now;
        //        history.ID = facility.ID;
        //        history.NPINumber = facility.NPINumber;
        //        history.PracticeGUID = facility.PracticeGUID;
        //        bppEntities.ProviderFacilityHistories.Add(history);
        //        bppEntities.SaveChanges();

        //        bppEntities.ProviderFacilities.Remove(facility);
        //        bppEntities.SaveChanges();
        //    }

        //    var providerLocations = bppEntities.ProviderLocations.Where(x => x.NPINumber == npi && x.PracticeGUID == practiceGUID).ToList();
        //    foreach (var location in providerLocations)
        //    {
        //        ProviderLocationsHistory history = new ProviderLocationsHistory();
        //        history.HistoryDt = DateTime.Now;
        //        history.ID = location.ID;
        //        history.IsPrimary = location.IsPrimary;
        //        history.LocationID = location.LocationID;
        //        history.NPINumber = location.NPINumber;
        //        history.PracticeGUID = location.PracticeGUID;

        //        bppEntities.ProviderLocationsHistories.Add(history);
        //        bppEntities.SaveChanges();

        //        bppEntities.ProviderLocations.Remove(location);
        //    }

        //    var providerMemberMatrix = bppEntities.ProviderMemberStatusMatrices.Where(x => x.NPINumber == npi && x.PracticeGUID == practiceGUID).FirstOrDefault();
        //    var matrixHistory = new ProviderMemberStatusMatrixHistory();
        //    matrixHistory.HistoryDt = DateTime.Now;
        //    matrixHistory.ID = providerMemberMatrix.ID;
        //    matrixHistory.InactiveReasonStatus = providerMemberMatrix.InactiveReasonStatus;
        //    matrixHistory.NPINumber = providerMemberMatrix.NPINumber;
        //    matrixHistory.OtherReason = providerMemberMatrix.OtherReason;
        //    matrixHistory.PracticeGUID = providerMemberMatrix.PracticeGUID;
        //    matrixHistory.ProviderMemberStatus = providerMemberMatrix.ProviderMemberStatus;

        //    bppEntities.ProviderMemberStatusMatrixHistories.Add(matrixHistory);
        //    bppEntities.SaveChanges();

        //    bppEntities.ProviderMemberStatusMatrices.Remove(providerMemberMatrix);

        //    var providerPractWorkflow = bppEntities.ProviderPracticeWorkflows.Where(x => x.NPINumber == npi && x.PracticeGUID == practiceGUID).FirstOrDefault();
        //    ProviderPracticeWorkflowHistory provPractWorkflowHistory = new ProviderPracticeWorkflowHistory();
        //    provPractWorkflowHistory.HistoryDt = DateTime.Now;
        //    provPractWorkflowHistory.BoardConsiderationDate = providerPractWorkflow.BoardConsiderationDate;
        //    provPractWorkflowHistory.BPPEffectiveDate = providerPractWorkflow.BPPEffectiveDate;
        //    provPractWorkflowHistory.DateNotifcationOutcome = providerPractWorkflow.DateNotifcationOutcome;
        //    provPractWorkflowHistory.DatePresentedMQProcess = providerPractWorkflow.DatePresentedMQProcess;
        //    provPractWorkflowHistory.DatePresentedReady = providerPractWorkflow.DatePresentedReady;
        //    provPractWorkflowHistory.DateRecommendationSent = providerPractWorkflow.DateRecommendationSent;
        //    provPractWorkflowHistory.DateSignedJoinder = providerPractWorkflow.DateSignedJoinder;
        //    provPractWorkflowHistory.DateSignedMemberApp = providerPractWorkflow.DateSignedMemberApp;
        //    provPractWorkflowHistory.EligibilityCertDate = providerPractWorkflow.EligibilityCertDate;
        //    provPractWorkflowHistory.ID = providerPractWorkflow.ID;
        //    provPractWorkflowHistory.MedStaffPrivileges = providerPractWorkflow.MedStaffPrivileges;
        //    provPractWorkflowHistory.NPINumber = providerPractWorkflow.NPINumber;
        //    provPractWorkflowHistory.Outcome = providerPractWorkflow.Outcome;
        //    provPractWorkflowHistory.OutcomeNotes = providerPractWorkflow.OutcomeNotes;
        //    provPractWorkflowHistory.PracticeGUID = providerPractWorkflow.PracticeGUID;
        //    provPractWorkflowHistory.PrimaryPracticeLocation = providerPractWorkflow.PrimaryPracticeLocation;
        //    provPractWorkflowHistory.ProviderApprovalStatus = providerPractWorkflow.ProviderApprovalStatus;
        //    provPractWorkflowHistory.Witness = providerPractWorkflow.Witness;

        //    bppEntities.ProviderPracticeWorkflowHistories.Add(provPractWorkflowHistory);
        //    bppEntities.SaveChanges();

        //    bppEntities.ProviderPracticeWorkflows.Remove(providerPractWorkflow);

        //    var specialities = bppEntities.ProviderSpecialtyMatrices.Where(x => x.NPINumber == npi).ToList();
        //    foreach (var specialty in specialities)
        //    {
        //        ProviderSpecialtyMatrixHistory history = new ProviderSpecialtyMatrixHistory();
        //        history.ID = specialty.ID;
        //        history.HistoryDt = DateTime.Now;
        //        history.IsPrmary = specialty.IsPrmary;
        //        history.NPINumber = specialty.NPINumber;
        //        history.SpecialtyID = specialty.SpecialtyID;
        //        history.PracticeGUID = specialty.PracticeGUID;
        //        bppEntities.ProviderSpecialtyMatrixHistories.Add(history);
        //        bppEntities.SaveChanges();

        //        bppEntities.ProviderSpecialtyMatrices.Remove(specialty);
        //        bppEntities.SaveChanges();
        //    }

        //    return;
        //}
    }
}
